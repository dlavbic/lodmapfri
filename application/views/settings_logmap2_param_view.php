<?php $this->load->helper('url'); ?>
<?php $this->load->helper('html'); ?>
<?php $this->load->helper('form'); ?>


<div class="container">

    <!--<div class="row well well-large">-->            
    <!--<div class="col-md-9">-->
    <!-- General Questions -->

    <div class="row">

        <div class="well well-lg">

            <?php
            $attributes = array('class' => 'logmap2params', 'class' => 'form-horizontal');
            echo form_open('logMap2params/saveProps', $attributes);
            ?>
            <fieldset>
                <legend><h3>LogMapFRI parameters</h3></legend>

                <legend><small>String metric for ontology alignment</small></legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Good SMOA anchors</label>  
                    <div class="col-md-4">
                        <input id="textinput" name="good_isub_anchors" type="text" placeholder="Good isub anchors" value="<?php echo $parameters['good_isub_anchors']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="good_isub_candidates">Good SMOA candidates</label>  
                    <div class="col-md-4">
                        <input id="good_isub_candidates" name="good_isub_candidates" type="text" placeholder="" value="<?php echo $parameters['good_isub_candidates']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="max_ambiguity">Max ambiguity</label>  
                    <div class="col-md-4">
                        <input id="max_ambiguity" name="max_ambiguity" type="text" placeholder="" value="<?php echo $parameters['max_ambiguity']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="good_ambiguity">Good ambiguity</label>  
                    <div class="col-md-4">
                        <input id="good_ambiguity" name="good_ambiguity" type="text" placeholder="" value="<?php echo $parameters['good_ambiguity']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="reverse_labels"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="reverse_labels">
                            <?php
                            $data = array(
                                'name' => 'reverse_labels',
                                'id' => 'reverse_labels',
                                'value' => '1',
                                'checked' => $parameters['reverse_labels'] == "true" ? "true" : "",
                            );

                            echo form_checkbox($data);
                            ?>

                            Reverse labels
                        </label>
                    </div>
                </div>



                <legend><small>Overlapping</small></legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="use_overlapping"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="use_overlapping">

                            <?php
                            $data = array(
                                'name' => 'use_overlapping',
                                'id' => 'use_overlapping',
                                'value' => '1',
                                'checked' => $parameters['use_overlapping'] == "true" ? "true" : "",
                            );

                            echo form_checkbox($data);
                            ?>

                            Use overlapping
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="min_size_overlapping">Min size overlapping</label>  
                    <div class="col-md-4">
                        <input id="min_size_overlapping" name="min_size_overlapping" value="<?php echo $parameters['min_size_overlapping']; ?>" type="text" placeholder="" class="form-control input-md" required="">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="second_chance_conflicts"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="second_chance_conflicts">
                            <?php
                            $data = array(
                                'name' => 'second_chance_conflicts',
                                'id' => 'second_chance_conflicts',
                                'value' => '1',
                                'checked' => $parameters['second_chance_conflicts'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Second chance conflicts
                        </label>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="ratio_second_chance_discarded">Ratio second chance discarded</label>  
                    <div class="col-md-4">
                        <input id="ratio_second_chance_discarded" name="ratio_second_chance_discarded" type="text" placeholder="" value="<?php echo $parameters['ratio_second_chance_discarded']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <legend><small>Instance confidence factor</small></legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="instance_matching"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="instance_matching">
                            <?php
                            $data = array(
                                'name' => 'instance_matching',
                                'id' => 'instance_matching',
                                'value' => '1',
                                'checked' => $parameters['instance_matching'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Instance matching
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="good_instance_confidence_map">instance confidence factor (ICF)</label>  
                    <div class="col-md-4">
                        <input id="min_conf_pro_map" name="good_instance_confidence_map" type="text" placeholder="" value="<?php echo $parameters['good_instance_confidence_map']; ?>" class="form-control input-md">
                    </div>
                </div>



                <legend><small>Class scope parameters</small></legend>


                <div class="form-group">
                    <label class="col-md-4 control-label" for="sub_levels">Sub levels</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="sub_levels" type="text" placeholder="sub_levels" value = "<?php echo $parameters['sub_levels']; ?>" class="form-control input-md" required="">
                        <!--<span class="help-block">Help</span>-->  
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="super_levels">Super levels</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="super_levels" type="text" placeholder="super_levels" value = "<?php echo $parameters['super_levels']; ?>" class="form-control input-md" required="">
                        <!--<span class="help-block">Help</span>-->  
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="max_size_subclasses">Max size of subclasses</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="max_size_subclasses" type="text" placeholder="max_size_subclasses" value = "<?php echo $parameters['max_size_subclasses']; ?>" class="form-control input-md" required="">
                        <!--<span class="help-block">Help</span>-->  
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="good_confidence">Good confidence</label>  
                    <div class="col-md-4">
                        <input id="good_confidence" name="good_confidence" type="text" placeholder="" value="<?php echo $parameters['good_confidence']; ?>" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="good_sim_coocurrence">Good sim coocurrence</label>  
                    <div class="col-md-4">
                        <input id="good_sim_coocurrence" name="good_sim_coocurrence" type="text" placeholder="" value="<?php echo $parameters['good_sim_coocurrence']; ?>" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="min_conf_pro_map">Min conf. pro. map</label>  
                    <div class="col-md-4">
                        <input id="min_conf_pro_map" name="min_conf_pro_map" type="text" placeholder="" value="<?php echo $parameters['min_conf_pro_map']; ?>" class="form-control input-md">

                    </div>
                </div>




                <div class="form-group">
                    <label class="col-md-4 control-label" for="bad_score_scope">Bad score scope</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="bad_score_scope" type="text" placeholder="bad_score_scope" value = "<?php echo $parameters['bad_score_scope']; ?>" class="form-control input-md" required="">
                        <!--<span class="help-block">Help</span>-->  
                    </div>
                </div>


                <legend><small>Alternative strings (synonims)</small></legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="syn_items_size">Synonyms items size</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="syn_items_size" type="text" placeholder="syn_items_size" value = "<?php echo $parameters['syn_items_size']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="syn_items_size_for_allowed_addition">Items size for allowed addition</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="syn_items_size_for_allowed_addition" type="text" placeholder="syn_items_size_for_allowed_addition" value = "<?php echo $parameters['syn_items_size_for_allowed_addition']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="syn_items_max_addition_size">Maximum items size for allowed addition</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="syn_items_max_addition_size" type="text" placeholder="syn_items_max_addition_size" value = "<?php echo $parameters['syn_items_max_addition_size']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="use_umls_4_individuals"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="use_umls_4_individuals">
                            <?php
                            $data = array(
                                'name' => 'use_umls_4_individuals',
                                'id' => 'use_umls_4_individuals',
                                'value' => '1',
                                'checked' => $parameters['use_umls_4_individuals'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Use WordNet for individuals
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="use_umls_4_context_individuals"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="use_umls_4_context_individuals">
                            <?php
                            $data = array(
                                'name' => 'use_umls_4_context_individuals',
                                'id' => 'use_umls_4_context_individuals',
                                'value' => '1',
                                'checked' => $parameters['use_umls_4_context_individuals'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Use WordNet for context individuals
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="use_umls_lexicon"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="use_umls_lexicon">
                            <?php
                            $data = array(
                                'name' => 'use_umls_lexicon',
                                'id' => 'use_umls_lexicon',
                                'value' => '1',
                                'checked' => $parameters['use_umls_lexicon'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Use UMLS lexicon
                        </label>
                    </div>
                </div>



                <legend><small>String morphological normalization</small></legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="onto_languages">Ontology language</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="onto_languages" type="text" placeholder="onto_languages" value = "<?php echo $parameters['onto_languages']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="use_lammagen"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="use_lammagen">
                            <?php
                            $data = array(
                                'name' => 'use_lammagen',
                                'id' => 'use_lammagen',
                                'value' => '1',
                                'checked' => $parameters['use_lammagen'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Use Lemmatization (Lammagen)
                        </label>
                    </div>
                </div>

                <legend><small>Entities scope (context)</small></legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="combine_individuals"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="combine_individuals">
                            <?php
                            $data = array(
                                'name' => 'combine_individuals',
                                'id' => 'combine_individuals',
                                'value' => '1',
                                'checked' => $parameters['combine_individuals'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Combine individuals
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="use_neighbourhood_similarity"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="use_neighbourhood_similarity">
                            <?php
                            $data = array(
                                'name' => 'use_neighbourhood_similarity',
                                'id' => 'use_neighbourhood_similarity',
                                'value' => '1',
                                'checked' => $parameters['use_neighbourhood_similarity'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Use neighborhood similarity
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="extract_scope_mapping"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="extract_scope_mapping">
                            <?php
                            $data = array(
                                'name' => 'extract_scope_mapping',
                                'id' => 'extract_scope_mapping',
                                'value' => '1',
                                'checked' => $parameters['extract_scope_mapping'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Extract scope mapping
                        </label>
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-md-4 control-label" for="syn_items_max_addition_size">Maximum items size for allowed addition</label>  
                    <div class="col-md-4">
                        <input id="bad_score_scope" name="syn_items_max_addition_size" type="text" placeholder="syn_items_max_addition_size" value = "<?php echo $parameters['syn_items_max_addition_size']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <legend><small>Output</small></legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="output_equivalences_only"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="output_equivalences_only">
                            <input type="checkbox" name="output_equivalences_only" id="output_equivalences_only" value="1">
                            Output equivalences only
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="output_class_mappings"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="output_class_mappings">
                            <?php
                            $data = array(
                                'name' => 'output_class_mappings',
                                'id' => 'output_class_mappings',
                                'value' => '1',
                                'checked' => $parameters['output_class_mappings'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Output class mappings
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="output_prop_mappings"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="output_prop_mappings">
                            <?php
                            $data = array(
                                'name' => 'output_prop_mappings',
                                'id' => 'output_prop_mappings',
                                'value' => '1',
                                'checked' => $parameters['output_prop_mappings'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Output prop mappings
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="output_instance_mappings"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="output_instance_mappings">
                            <?php
                            $data = array(
                                'name' => 'output_instance_mappings',
                                'id' => 'output_instance_mappings',
                                'value' => '1',
                                'checked' => $parameters['output_instance_mappings'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Output instance mappings
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="output_instance_mapping_files"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="output_instance_mapping_files">
                            <?php
                            $data = array(
                                'name' => 'output_instance_mapping_files',
                                'id' => 'output_instance_mapping_files',
                                'value' => '1',
                                'checked' => $parameters['output_instance_mapping_files'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Output instance mapping files
                        </label>
                    </div>
                </div>

                <legend><small>Reasoning</small></legend>



                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="reasoner">Reasoner</label>
                    <div class="col-md-4">



                        <?php
                        $options = array(
                            'HermiT' => 'HermiT',
                            'MORe' => 'MORe',
                            'Pellet' => 'Pellet',
                            'trowl' => 'trowl',
                        );

                        $data = array(
                            'class' => 'form-control',
                            'id' => 'reasoner',
                        );

//                            $shirts_on_sale = array('small', 'large');

                        echo form_dropdown('reasoner', $options, $parameters['reasoner'], 'class="form-control" id="reasoner"');
                        ?>



                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 control-label" for="reason_datatypes"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="reason_datatypes">
                            <?php
                            $data = array(
                                'name' => 'reason_datatypes',
                                'id' => 'reason_datatypes',
                                'value' => '1',
                                'checked' => $parameters['reason_datatypes'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Reason datatypes
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="timeout">Reasoner Timeout</label>  
                    <div class="col-md-4">
                        <input id="timeout" name="timeout" type="text" placeholder="" value="<?php echo $parameters['timeout']; ?>" class="form-control input-md" required="">
                    </div>
                </div>

                <legend><small>Interactivity</small></legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="allow_interactivity"></label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="allow_interactivity">
                            <?php
                            $data = array(
                                'name' => 'allow_interactivity',
                                'id' => 'allow_interactivity',
                                'value' => '1',
                                'checked' => $parameters['allow_interactivity'] == "true" ? "true" : "",
                            );
                            echo form_checkbox($data);
                            ?>
                            Allow interactivity
                        </label>
                    </div>
                </div>

                <div class="form-actions pull-right">
                    <!-- <button type="button" class="btn btn-warning" disabled="disabled">Reset values</button>-->
                    <button name="sbm" type="submit" value="reset_button" class="btn btn-warning">Reset values</button>
                    <button name="sbm" type="submit" value="save_button" class="btn btn-success">Save</button>
                    <button name="sbm" type="submit" class="btn">Cancel</button>
                </div>

            </fieldset>

            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<script>

    $(document).ready(function () {




//        var data2 = '<?php //print $lexUtilsSynonyms;                               ?>';
//        var data2 = "adfafasd";

//    console.log(data2);

//        $('input[name="ttlContent4RDF"]').val(editor.getValue());
//        var editor = $('.CodeMirror')[0].CodeMirror;
//        editor.setValue(data2);
    });

</script>

