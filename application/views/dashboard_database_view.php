<?php $this->load->helper('url'); ?>
<?php $this->load->helper('html'); ?>
<?php $this->load->helper('form'); ?>


<!-- Loading Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Please Wait</h4>

            </div>
            <div class="modal-body center-block">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.modal -->

<div class="row">
    <!-- Form Name -->
    <legend>Database</legend>
    <div class="col-md-8">
        <!--<form class="form-horizontal">-->
        <?php echo form_open('dashboard/getSchema', "class=form-horizontal"); ?>
        <fieldset>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-2 control-label" for="selectDatabase">DB Type</label>
                <div class="col-md-4">
                    <select placeholder="Select database" name="dbType" class="form-control">
                        <option value="mysql">MySQL</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" for="dbPort">DB Host</label>
                <div class="col-sm-4">
                    <input name="dbHost" type="text" placeholder="Insert host" value="localhost" class="form-control">
                </div>

                <label class="col-md-2 control-label" for="textinput">DB Port</label>
                <div class="col-sm-2">
                    <input name="dbPort" type="text" placeholder="Port" class="form-control">
                </div>
            </div>



            <div class="form-group">
                <label class="col-md-2 control-label" for="selectDatabase">Select Database</label>
                <div class="col-md-4">
                    <select id="selectDatabase" placeholder="Select database" name="selectDatabase" class="form-control">
                        <?php
                        if (count($dbs)) {
                            echo "<option value='-1'>None</option>";
                            foreach ($dbs as $db_index => $db_value) {
                                echo "<option value='" . $db_value . "'>" . $db_value . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" for="textinput">DB User</label>  
                <div class="col-md-4">
                    <input id="db_user" name="dbUser" type="text" placeholder="Username" class="form-control input-md">
                </div>

                <label class="col-md-2 control-label" for="textinput">Password</label>  
                <div class="col-md-4">
                    <input id="db_pass" name="dbPassword" type="password" placeholder="Password" class="form-control input-md">
                    <span class="help-block">If there is no password, leave the field empty.</span> 
                </div>
            </div>

            <div class="form-group well">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <!--<li id="showDbFields" class="btn btn-primary ">Show DB Schema</li>-->
                        <button href="#myModal" data-toggle="modal" type="submit" id="showDbFields" class="btn btn-primary ">Show DB Schema</button>




                        <!--<button href="#myModal" role="button" type="submit" class="btn btn-primary" data-toggle="modal">Submit</button>-->





                    </div>
                </div>
            </div>


        </fieldset>
        <?php echo form_close(); ?>
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script>


    $(function() {




    });


</script>
