m<?php $this->load->helper('url'); ?>
<?php $this->load->helper('html'); ?>
<?php $this->load->helper('form'); ?>

<!-- Modals -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Please Wait...</h4>
            </div>
            <div class="modal-body center-block">
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="schemaLogModal" tabindex="-1" role="dialog" aria-labelledby="schemaLogModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="schemaLogModalLabel">ShemaSpy log</h4>
            </div>
            <div class="modal-body">
                <pre id="schemaLogContent"></pre>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modals -->

<div class="container">

    <div class="row">

        <div id="rootwizard">

            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid" style="padding-left: 0px;">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-left: 0px;">
                        <ul class="nav navbar-nav">
                            <li><a href="#tab1" data-toggle="tab">1. DB Parameters</a></li>
                            <li><a href="#tab2" data-toggle="tab">2. DB Schema & Table Paths</a></li>
                            <li><a href="#tab3" data-toggle="tab">3. Custom class/table attributes</a></li>
                            <li><a href="#tab4" data-toggle="tab">4. Edit R2RML</a></li>
                            <li><a href="#tab5" data-toggle="tab">5. DB2Triples & Matching process</a></li>
                            <li><a href="#tab6" data-toggle="tab">6. Results</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="progress">
                <div class="progress-bar progress-bar-info bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                    <span class="sr-only"></span>
                </div>
            </div>

            <div class="well well-lg">
                <div class="tab-content">

                    <!-- tab1 -->
                    <div class="tab-pane" id="tab1">
                        <div class="row">
                            <!-- Form Name -->
                            <div class="col-md-12">
                                <form class="form-horizontal" id="databaseDataForm">
                                    <?php // echo form_open('', "class=form-horizontal"); ?>
                                    <legend>Database Parameters </legend>

                                    <h3><small>First step: Choose your database and set all parameters in order to generate R2RML mapping document</small></h3>
                                    <br />
                                    <fieldset>

                                        <?php /*
                                          <!-- Select Basic -->
                                          <div class="form-group">
                                          <label class="col-md-2 control-label" for="selectDatabase">DB Type</label>
                                          <div class="col-md-4">
                                          <select id="dbType" placeholder="Select database" name="dbType" class="form-control">
                                          <option value="mysql">MySQL</option>
                                          </select>
                                          </div>
                                          </div>

                                          <div class="form-group">
                                          <label class="col-md-2 control-label" for="dbPort">DB Host</label>
                                          <div class="col-sm-4">
                                          <input id="dbHost" name="dbHost" type="text" placeholder="Insert host" value="localhost" class="form-control">
                                          </div>

                                          <label class="col-md-2 control-label" for="textinput">DB Port</label>
                                          <div class="col-sm-2">
                                          <input id="dbPort" name="dbPort" type="number" placeholder="Port" class="form-control">
                                          </div>
                                          </div>
                                          <div class="form-group">
                                          <label class="col-md-2 control-label" for="textinput">DB User</label>
                                          <div class="col-md-4">
                                          <input id="db_user" name="db_user" type="text" placeholder="Username" value="root" class="form-control input-md">
                                          </div>

                                          <label class="col-md-2 control-label" for="textinput">Password</label>
                                          <div class="col-md-4">
                                          <input id="db_pass" name="db_pass" type="password" placeholder="Password" class="form-control input-md">
                                          <span class="help-block">If there is no password, leave the field empty.</span>
                                          </div>
                                          </div>

                                          <!--                                        <div class="form-group">
                                          <div class="col-sm-offset-2 col-md-4">
                                          <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-flash" aria-hidden="true"></span> db Connect</button>
                                          </div>
                                          </div>-->

                                          <br />
                                         */
                                        ?>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="selectDatabase">Select Database</label>
                                            <div class="col-md-4">
                                                <select id="selectDatabase" placeholder="Select database" name="selectDatabase" class="form-control">
                                                    <?php
                                                    if (count($dbs)) {
                                                        echo "<option value=''>None</option>";
                                                        foreach ($dbs as $db_index => $db_value) {
                                                            echo "<option value='" . $db_value . "'>" . $db_value . "</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-4 col-md-offset-2 checkbox">
                                                <label><input checked type="checkbox" name="includeViewsCheckbox" id="includeViewsCheckbox"> Include views</label>
                                            </div>

                                            <label class="col-md-2 control-label" for="textinput">Schema image type</label>  

                                            <div class="col-md-4 radio">
                                                <label>
                                                    <input type="radio" name="schemaImageType" id="schemaImageAsDirectedGraph" value="1" checked>
                                                    View database schema as directed graph
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-4 col-md-offset-2 checkbox">
                                                <label><input type="checkbox" name="includeHTMLCheckbox" id="includeHTMLCheckbox"> Generate shema image</label>
                                            </div>

                                            <div class="col-md-4 col-md-offset-2 radio">
                                                <label>
                                                    <input type="radio" name="schemaImageType" id="schemaImageAsPNG" value="2">
                                                    View database schema as image (PNG)
                                                </label>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-4 col-md-offset-2 checkbox">
                                                <label><input type="checkbox" name="inverseGraphCheckbox" id="inverseGraphCheckbox"> Inverse Schema DG</label>
                                            </div>
                                        </div>


                                    </fieldset>
                                </form>
                                <?php // echo form_close(); ?>
                            </div>
                        </div>
                    </div>

                    <!-- tab2 -->
                    <div class="tab-pane" id="tab2">
                        <div class="row">

                            <div class="form-horizontal">
                                <legend>Select tables to include into mapping file </legend>
                                <h3>
                                    <small>Second step: Select tables as start and end point in order to calculate all possible paths between them. In directed schema graph below select one of available paths (found with <a href="http://en.wikipedia.org/wiki/Breadth-first_search">Breadth First Search Algorithm</a>)</small>
                                </h3>

                                <br />
                                <fieldset>
                                    <div id="tableFieldSelection">

                                        <h4>
                                            <small></small>
                                        </h4>

                                        <div class="form-group">

                                            <label class="col-md-2 control-label" for="table_list">Select first table</label>

                                            <div class="col-md-4">
                                                <select id="table_selection1" name="table_list" placeholder="Select table" class="form-control lstTables" >
                                                    <option value='0'>None</option>
                                                </select>
                                            </div>

                                            <label class="col-md-2 control-label" for="table_list2">Select last table</label>

                                            <div class="col-md-4">
                                                <select id="table_selection2" name="table_list2" placeholder="Select table" class="form-control lstTables" >
                                                    <option value='0'>None</option>
                                                </select>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="form-group">

                                        <div id="schemaSpyArea">
                                            <div class="col-md-2 right text-right">
                                                <button class="btn btn-default btn-xs" data-toggle="modal" data-target="#schemaLogModal">View Schema Log</button>
                                            </div>
                                            <div class="col-md-10">
                                                <a id="databaseSchemaLink" href="#" target="_blank" >
                                                    <img id="databaseSchemaImage" src="#" alt="testna shema db" class="img-responsive img-thumbnail" style="max-height: 600px;">
                                                </a>
                                            </div>
                                        </div>

                                        <div id="directedGraphArea" class="col-md-12">
                                            <div id="mynetwork" class="img-responsive img-thumbnail" style="width: 100%; height: 400px;"></div>
                                        </div>
                                    </div>


                                    <br />
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <div class="pull-right">
                                                <button id="getAllPossiblePathsButton" class="btn btn-success ">
                                                    <span class="glyphicon glyphicon-road"></span> Show me all possible paths
                                                </button>
                                                <!--<button id="addTableID" class="btn btn-default "><span class="glyphicon glyphicon-plus"></span> Additional table</button>-->
                                            </div>
                                        </div>
                                    </div>

                                    <br />

                                    <div id="tablePathsDiv" class="panel panel-primary">
                                        <div class="panel-heading">
                                            <span class="glyphicon glyphicon-road"></span> Select Database Tables Path
                                        </div>
                                        <div class="panel-footer">

                                            <small><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Select one of available paths listed below (found with <a href="http://en.wikipedia.org/wiki/Breadth-first_search">Breadth First Search Algorithm</a>) and click next to go next step.</small>
                                        </div>
                                        <div class="panel-body" id="pathList">
                                        </div>
                                    </div>

                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <!-- tab3 -->
                    <div class="tab-pane" id="tab3">

                        <div class="row">
                            <legend>Custom class/table attributes </legend>
                            <h3><small>Third step: Choose items from tables of chosen tables path.</small></h3>
                            <br />
                            <fieldset>
                                <div class="col-md-5">
                                    <div class="col-md-10" id="tablesFieldsComboBox">
                                    </div>
                                </div>

                                <div class="col-md-6 parrent-affix">
                                    <div data-spy="affix" data-offset-bottom="160" data-offset-top="60" class="affixed-element col-md-6 " >

                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon"><#TriplesMap</span>

                                            <input type="text" id="tripleMapNameID" class="form-control" placeholder="Class name">
                                            <span class="input-group-addon">></span>
                                        </div>
                                        <ul class="target connected">
                                            <li class="placeholder">Drop your favourites here</li>
                                        </ul>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <!-- tab4 -->
                    <div class="tab-pane" id="tab4">
                        <div class="row">
                            <?php
                            $attributes = array('class' => 'R2RMLSourceForm', 'class' => 'form-horizontal');
                            echo form_open('mapping/getTTLFile', $attributes);
                            ?>
                            <input type="hidden" name="database" value="" />

                            <legend>Edit R2RML </legend>
                            <h3>
                                <small>Fourth step: Edit content of "auto-generated" R2RML mapping document.</small>
                            </h3>
                            <br />
                            <fieldset>
                                <div class="form-group">
                                    <div class="container">
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="15" id="TTLSource" name="TTLSource"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-download-alt"></span> Download <span id="ttlFileName"></span>.ttl</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <?php echo form_close(); ?>
                        </div>
                    </div>

                    <!-- tab5 -->
                    <div class="tab-pane" id="tab5">
                        <div class="row">

                            <!-- Form Name -->
                            <div class="col-md-12">

                                <?php
                                $attributes = array('class' => 'getRDFTriplesForm', 'class' => 'form-horizontal upload-files-form', 'id' => 'getRDFTriplesForm');
                                echo form_open('mapping/getRDFFile', $attributes);
                                ?>
                                <input type="hidden" name="database" value="" />
                                <input type="hidden" name="ttlContent4RDF" value="" />
                                <input type="hidden" name="gsFileID" value="" />
                                <input type="hidden" name="ontoFileID" value="" />
                                <!--<form class="form-horizontal">-->

                                <legend>R2RML Parser Properties</legend>
                                <h3>
                                    <small>Fifth step: Set R2RML Processor (<a target="_blank" href="https://github.com/antidot/db2triples/">DB2Triples</a>) parameters and start matching process with LogMapFRI.</small>
                                </h3>

                                <br />

                                <fieldset>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="defaultNamespace">Default namespace</label>

                                        <div class="col-md-4">
                                            <input id="defaultNamespaceID" name="defaultNamespace" class="form-control" placeholder="" value="http://www.lavbic.net/#" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="fileSyntaxRadioOptions">Export triples format</label>

                                        <div class="col-md-4">
                                            <div class="radio">
                                                <label class="checkbox-inline">
                                                    <input checked type="radio" name="RDFDumpTypeRadioButton" id="ntriple" value="RDFDumpTypeFile"> RDF file
                                                </label>
                                            </div>
                                            <div class="radio ">
                                                <label class="checkbox-inline">
                                                    <input type="radio" name="RDFDumpTypeRadioButton" id="xml" value="RDFDumpTypeTDB"> Fuseki Triplestore server (Jena TDB backend)
                                                </label>
                                            </div>

                                            <!--                                            <div class="radio disabled">
                                                                                            <label class="checkbox-inline">
                                                                                                <input type="radio" name="RDFDumpTypeRadioButton" id="isql" value="RDFDumpTypeVOS" disabled> Virtuoso
                                                                                            </label>
                                                                                        </div>-->
                                        </div>
                                    </div>

                                    <div id="RDFDumpTypeFile" class="desc">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="fileSyntaxRadioOptions">RDF file syntax</label>

                                            <div class="col-md-4">
                                                <div class="radio">
                                                    <label>
                                                        <input checked type="radio" name="fileSyntaxRadioOptions" id="ntriple" value="NTRIPLES"> N-TRIPLES
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="fileSyntaxRadioOptions" id="xml" value="RDFXML"> RDF/XML
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="fileSyntaxRadioOptions" id="owl" value="RDFXML"> OWL/XML
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="fileSyntaxRadioOptions" id="turtle" value="TURTLE"> TURTLE
                                                    </label>
                                                </div>
                                                <!-- <div class="radio"><label><input type="radio" name="fileSyntaxRadioOptions" id="ttl" value="TTL"> TTL</label></div>-->
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="fileSyntaxRadioOptions" id="n3" value="N3"> N3
                                                    </label>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="destFileName">Destination File Name</label>

                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input id="destFileNameID" name="destFileName" class="form-control" placeholder="Destination File Name" type="text">
                                                    <span class="input-group-addon">.RDF</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="pull-right">
                                                    <button id="getTriplesRDF" type="submit" class="btn btn-success" name="RDFDumpTypeButton" value="1"><span class="glyphicon glyphicon-download-alt"></span> Get triples in <span id="ttlFileName"></span>.RDF </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="RDFDumpTypeTDB" class="desc">
                                        <div  class="form-group">
                                            <label class="col-md-2 control-label" for="JenaTDBDirectoryPath">Jena TDB Directory Path</label>

                                            <div class="col-md-4">
                                                <input type="folder" id="JenaTDBDirectoryPathID" name="JenaTDBDirectoryPath" class="form-control" placeholder="Jena TDB Directory Path" value="C:\apache-jena-fuseki-2.0.0\run\databases">
                                                <p class="help-block">This property defines the directory where the target model will be serialized</p>
                                            </div>
                                        </div>

                                        <!--                                        <div class="form-group">
                                                                                    <div class="col-sm-offset-2 col-sm-10">
                                                                                        <div class="pull-right">
                                                                                            <button id="storeTDBButton" type="submit" class="btn btn-info" name="storeTDBButton" value="2"><span class="glyphicon glyphicon glyphicon-upload"></span> Store triples using Jena TDB </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>-->
                                    </div>

                                </fieldset>




                                <legend>LogMapFRI: Mapping Settings</legend>
                                <h3>
                                    <small>You can edit all parameters for LogMapFRI application by clicking on settings button <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> and choosing "Edit LogMapFRI Parameters".</small>
                                </h3>

                                <fieldset>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="selectonto">Select Ontology</label>
                                        <div class="col-md-4">
                                            <select id="selectonto" name="selectonto" class="form-control">
                                                <option value="dbpedia_small">DBpedia</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- File Button --> 
                                    <div class="form-group">

                                        <div class="form-inline"> 
                                            <label class="col-md-3 control-label" for="fileontobutton">Upload ontology</label>
                                            <div class="col-md-4">
                                                <input name="ontoUserFile" id="ontoUserFile" class="input-file" type="file">
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" name = "submit-ontology" class="btn btn-sm btn-primary" id="submitOntology">Upload ontology</button>
                                            </div>
                                            <div class="col-md-3">
                                                <span id="ontoFileName"></span>
                                            </div>
                                        </div>
                                        <!--</div>-->
                                    </div>
                                </fieldset>

                                <style>
                                    .popover{
                                        max-width: none;
                                    }
                                </style>

                                <fieldset>
                                    <!-- File Button --> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="fileGSbutton">Upload Gold Standard Mappings</label>
                                        <div class="col-md-4">
                                            <input id="GSMappingsUserFile" name="GSMappingsUserFile" class="input-file" type="file">
                                            <p class="help-block">Gold Standard Mappings consists of correct mappings. <a tabindex="0" class="glyphicon glyphicon-question-sign" role="button" data-toggle="popover" data-trigger="focus" title="Gold Standard Mappings example" data-content="http://www.lavbic.net/onto/sakila/City/Paris|http://dbpedia.org/resource/Paris"></a>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-sm btn-primary" id="submitGSMappings">Upload GS Mappings</button>
                                        </div>
                                        <div class="col-md-3">
                                            <span id="gsFileName"></span>
                                        </div>

                                    </div>

                                </fieldset>

                                <?php echo form_close(); ?>
                                <!--</form>-->
                            </div>
                        </div>
                    </div>

                    <!-- tab6 -->
                    <div class="tab-pane" id="tab6">

                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                $attributes = array('class' => 'R2RMLSourceForm', 'class' => 'form-horizontal');
                                echo form_open('#', $attributes);
                                ?>
                                <input type="hidden" name="database" value="" />

                                <!--<div class="headline"><h2>Lexical Utilities: Creating and Editing Synonyms</h2></div>-->
                                <legend>LogMapFRI Matching Results</legend>

                                <br />

                                <fieldset>

                                    <div class="well" style="background-color: white;">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Precision*</th>
                                                    <td id="precision_result"></td>
                                                    <td>
                                                        <img class="mwe-math-fallback-image-inline tex" alt="\text{Precision}=\frac{tp}{tp+fp} \, " src="//upload.wikimedia.org/math/a/8/7/a87a5d89797001aa6c8d9a7031caf1ad.png">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Recall*</th>
                                                    <td id="recall_result"></td>
                                                    <td>
                                                        <img class="mwe-math-fallback-image-inline tex" alt="\text{Recall}=\frac{tp}{tp+fn} \, " src="//upload.wikimedia.org/math/9/1/b/91b88600b433b3059101d0295735daf5.png">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">F-measure*</th>
                                                    <td id="fmeasure_result"></td>
                                                    <td>
                                                        <span class="text-muted">* Values are available only if GS mappings are provided.</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Threshold</th>
                                                    <td id="threshold_result"></td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"># Mappings</th>
                                                    <td id="mappings_result"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Matching time</th>
                                                    <td id="matching_time_result"></td>
                                                    <td></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>


                                    <div class="form-group">
                                        <div class="container">
                                            <div class="col-md-12">
                                                <label for="logmap2_mappings">Valid mappings: </label>
                                                <textarea class="form-control" rows="10" id="logmap2_mappings" name="logmap2_mappings"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <br />

                                    <div class="form-group">
                                        <div class="container">
                                            <div class="col-md-12">
                                                <label for="incompatible_instance_mappings">Incompatible instance mappings: </label>
                                                <textarea class="form-control" rows="15" id="incompatible_instance_mappings" name="incompatible_instance_mappings"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <br />

                                    <div class="form-group">
                                        <div class="container">
                                            <div class="col-md-12">
                                                <label for="logmap2_mappings_rdf">Mapping Result in RDF: </label>
                                                <textarea class="form-control" rows="15" id="logmap2_mappings_rdf" name="logmap2_mappings_rdf"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <br />

                                    <div class="form-group">
                                        <div class="container">
                                            <div class="col-md-12">
                                                <label for="logmap2_mappings_owl">Mapping Result in OWL: </label>
                                                <textarea class="form-control" rows="15" id="logmap2_mappings_owl" name="logmap2_mappings_owl"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <br />

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <div class="pull-right">
                                                <button type="submit" id="storeResultTDB" name="storeResultTDB" class="btn btn-success"><span class="glyphicon glyphicon glyphicon-floppy-disk"></span> Store to TDB </button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                <?php echo form_close(); ?>


                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <small>
                                            <p class="help-block">
                                                <kbd>Ctrl-F</kbd> / <kbd>Cmd-F</kbd>
                                                Start searching
                                            </p>
                                            <p class="help-block">
                                                <kbd>Ctrl-G</kbd> / <kbd>Cmd-G</kbd>
                                                Find next
                                            </p>
                                            <p class="help-block">
                                                <kbd>Shift-Ctrl-G</kbd> / <kbd>Shift-Cmd-G</kbd>
                                                Find previous
                                            </p>
                                            <p class="help-block">
                                                <kbd>Shift-Ctrl-F</kbd> / <kbd>Cmd-Option-F</kbd>
                                                Replace
                                            </p>
                                            <p class="help-block">
                                                <kbd>Shift-Ctrl-R</kbd> / <kbd>Shift-Cmd-Option-F</kbd>
                                                Replace all
                                            </p>
                                        </small>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>

                </div>	
            </div>
            <ul class="pager wizard">
                <li class="previous first" ><a href="#">First</a></li>
                <li class="previous"><a href="#">Previous</a></li>
                <li class="next"><a href="#">Next</a></li>
                <!--<li class="next finish" style="display:none;"><a href="javascript:;">Finish</a></li>-->
            </ul>
        </div>

        <!--</div>-->
    </div>
</div>

<!--<script src="<?php echo base_url(); ?>assets/bwizard/js/bwizard.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/select2/select2.js" type="text/javascript"></script>


<script>

    //GLOBALS
    var defaultImage = "http://placehold.it/800x400/99CCFF/FFF&text=Schema%20image%20";
    var hostname;
    var db_port;
    var username;
    var password;
    var database;
    var inverse;
    var dbdriver;
    var r2rmlContent;
    var includeViews;
    var includeHTML;
    var schemaType;
    var tableData;
    var graphPaths = new Array();
    var selectedGraphIndex;
    var selectedGraphPath;
    var tripleMapNameID;
    var numOfSelectionFields = 2;
    var GS_filename;
    var GS_file_id;
    var ontology_filename;
    var ontology_file_id;
    var GS_user_file = false;
    var ontology_user_file = false;
    var allNodes = [];
    var fieldSelectDiv = "<div class='form-group' id='selectionFieldForm###'>" +
            "<label class='col-md-2 control-label' for='table_list'>Select table</label>" +
            "<div class='col-md-3'>" +
            "<select id='table_selection###' name='table_list###' placeholder='Select table' class='form-control lstTables' >" +
            "<option value='0'>None</option>" +
            "</select>" +
            "</div>" +
            "<label class='col-md-2 control-label' for='textinput'>Select fields</label>" +
            "<div class='col-md-4'>" +
            "<select id='tableFieldsSelect###' name='fields_list###' placeholder='Select field' class='form-control lstTablesField' >" +
            "</select>" +
            "<span class='help-block'><a class='btn btn-info btn-xs'>More info.</a></span>" +
            "</div>" +
            "<div class='col-md-1'>" +
            "<button id='removeSelectField###' class='btn btn-default removeSelectionButton'><span class='glyphicon glyphicon-trash'></span></button>" +
            "</div>" +
            "</div>";

    var graphSeparator = "<span class='glyphicon glyphicon glyphicon-arrow-right btn-lg'></span>";

    $(window).resize(function () {
        $('.affixed-element').width($('.parrent-affix').width());
    });

    $(document).ready(function () {

        $('#submitGSMappings').click(function (e) {

            e.preventDefault();

            $.ajaxFileUpload({
                url: 'mapping/upload_GSMappings',
                secureuri: false,
                fileElementId: 'GSMappingsUserFile',
//                fileElementId: $('GSMappingsUserFile').val(),
                dataType: 'json',
                success: function (data, status)
                {
                    GS_filename = data.filename;
                    GS_file_id = data.file_id;

                    if (GS_file_id.length > 0) {
                        GS_user_file = true;
                    }

                    $('#gsFileName').html(GS_filename);
                    $('input[name="gsFileID"]').val(GS_file_id);

                    alert(data.msg);
                }
            });
            return false;
        });

        $('#submitOntology').click(function (e) {
//        $('#getRDFTriplesForm').submit(function (e) {

            e.preventDefault();
//            $('#myModal').modal('toggle');

            $.ajaxFileUpload({
                url: 'mapping/upload_ontology',
                secureuri: false,
                fileElementId: 'ontoUserFile',
//                fileElementId: $('GSMappingsUserFile').val(),
                dataType: 'json',
                success: function (data, status)
                {
                    ontology_filename = data.filename;
                    ontology_file_id = data.file_id;

                    if (ontology_file_id.length > 0) {
                        ontology_user_file = true;
                    }

                    $('#ontoFileName').html(ontology_filename);
                    $('input[name="ontoFileID"]').val(ontology_file_id);

                    alert(data.msg);

                }
            });
            return false;
        });

        $('#storeResultTDB').click(function (e) {
//        $('#getRDFTriplesForm').submit(function (e) {

            e.preventDefault();
//            $('#myModal').modal('toggle');

            storeFinalResult2TDB();
        });

        $('[data-toggle="popover"]').popover();

        $('#getAllPossiblePathsButton').on('click', function () {

            $("#tablePathsDiv").show();

            $('input[name=selectedGraphPath]:radio').on('click', function () {
                $('#rootwizard').find('.pager .next').show();
            });

        });

        $("#tablePathsDiv").hide();

        // last tab hide and show divs based on RDF export type
        $("div.desc").hide();

        $("#RDFDumpTypeFile").show();

        $("input[name$='RDFDumpTypeRadioButton']").click(function () {
            var activeRadioButton = $(this).val();
            $("div.desc").hide();
            $("#" + activeRadioButton).show();
        });

        var editor = CodeMirror.fromTextArea(document.getElementById("TTLSource"), {
            mode: "text/turtle",
            lineNumbers: true
                    //            styleActiveLine: true,
                    //            matchBrackets: true
        });

        var editor_mappings = CodeMirror.fromTextArea(document.getElementById("logmap2_mappings"), {
            mode: "text/turtle",
            height: "150px",
            lineNumbers: true
        });

        var editor_incom_map = CodeMirror.fromTextArea(document.getElementById("incompatible_instance_mappings"), {
            mode: "text/turtle",
            lineNumbers: true
        });

        var editor_map_rdf = CodeMirror.fromTextArea(document.getElementById("logmap2_mappings_rdf"), {
            mode: "text/turtle",
            lineNumbers: true
        });

        var editor_map_owl = CodeMirror.fromTextArea(document.getElementById("logmap2_mappings_owl"), {
            mode: "text/turtle",
            lineNumbers: true
        });

        $('#rootwizard').bootstrapWizard({
            onTabShow: function (tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $('#rootwizard').find('.bar').css({width: $percent + '%'}).attr('aria-valuenow', $percent);

                if ($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    //                    $('#rootwizard').find('.pager .finish').show();
                    //                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            },
            onNext: function (tab, navigation, index) {
                if (index === 1) {  // first next button

                    hostname = $("#dbHost").val();
                    db_port = $("#dbPort").val();
                    username = $("#db_user").val();
                    password = $("#db_pass").val();
                    database = $("#selectDatabase option:selected").val();
                    includeViews = $("#includeViewsCheckbox:checked").val();
                    inverse = $("#inverseGraphCheckbox:checked").val();
                    includeHTML = $("#includeHTMLCheckbox:checked").length;
                    schemaType = $('input:radio[name=schemaImageType]:checked').val();
                    dbdriver = $("#dbType option:selected").val();

                    getDatabaseData();

                    return false;
                } else if (index === 2) {   // second next button
                    selectedGraphIndex = $('input[name=selectedGraphPath]:checked').val();

                    if (!selectedGraphIndex) {

                        selectedGraphPath = paths[chosenPath];

                    } else {
                        selectedGraphPath = graphPaths[selectedGraphIndex];
                    }

                    getGraphTablesFields(selectedGraphPath);

                    return false;
                } else if (index === 3) {   // third next button
                    //                    var selectedGraphIndex = $('input[name=selectedGraphPath]:checked').val();
                    //                    var selectedGraphPath = graphPaths[selectedGraphIndex];
                    //
                    var logicalTableDefinitionMode = $('input[name=logicalTableRadioOptions]:checked').val();
                    tripleMapNameID = $('#tripleMapNameID').val();
                    $('input[name="database"]').val(database);

                    var items = [];
                    $("ul.target").children().each(function () {

                        var itemAttr = $(this).attr("data-attribute");
                        var itemTable = $(this).attr("data-table");
                        var itemPK = $(this).attr("data-primary-key");
                        var item = {table: itemTable, attribute: itemAttr, pk: itemPK};
                        items.push(item);
                    });
                    var selecedAttributes = JSON.stringify(items);
                    //                    getMappingContent(selectedGraphIndex, selectedGraphPath, logicalTableDefinitionMode);
                    getMappingContent(selecedAttributes, selectedGraphIndex, selectedGraphPath, logicalTableDefinitionMode, tripleMapNameID);

                    return false;
                } else if (index === 4) {   // fourth next button

                    $('input[name="database"]').val(database);
                    $('input[name="defaultNamespace"]').val("http://www.lavbic.net/onto/" + database + "/" + tripleMapNameID + "/");
                    //                    $('input[name="r2rmlMappingContent"]').val(database);

                    var editor = $('.CodeMirror')[0].CodeMirror;
                    $('input[name="ttlContent4RDF"]').val(editor.getValue());
                    r2rmlContent = editor.getValue();
//                    generateRDFTriples(editor.getValue());

                } else if (index === 5) {   // fifth next button
                    //                    getMappingFile();
                    $('input[name="database"]').val(database);

                    getLogMap2MappingResult();
//                    $('input[name="r2rmlMappingContent"]').val(database);

//                    var editor = $('.CodeMirror')[0].CodeMirror;
//                    $('input[name="ttlContent4RDF"]').val(editor.getValue());

                }
            },
            onPrevious: function (tab, navigation, index) {
                if (index === 0) {
                    resetFieldImput();
                }
            },
            onTabClick: function (tab, navigation, index) {
                database = $("#selectDatabase option:selected").val();
                //            TODO: za testne namene je zakomentirano -> ODKOMENTIRAJ! in kar je pred tem zbrisi
                return false;
            }
        });

        $('#getRDFTriplesForm').submit(function () {
            $('#myModal').modal('toggle');
        });



        $('#addTableID').on('click', function () {

            numOfSelectionFields = numOfSelectionFields + 1;
            var str = fieldSelectDiv.replace(/###/g, numOfSelectionFields);

            $('#tableFieldSelection').append(str);

            setTableSelectionData(numOfSelectionFields);

        });

        $(".removeSelectionButton").on("click", function () {
            var temp_id = 'selectionFieldForm' + this.id.substr(this.id.length - 1);
            temp_id = temp_id.toString();
//            console.log(temp_id);

            $(temp_id).remove();
        });
    });

    function getAllPossiblePaths(tablesArray) {
        $('#myModal').modal('toggle');

        $.ajax({url: "<?php echo site_url('mapping/getPathsFromDatabase'); ?>",
            data: {
                //tukaj ko naredis array vseh izbranih table in field poslji kar cel array
                tables: tablesArray,
                inverse: inverse,
                database: database // potreben samo database ker bos bral iz xml file                 
            },
            dataType: "json",
            type: "POST",
            success: function (data) {

                if (data.length === 0) {
                    alert("Data error!");
                } else {
                    graphPaths = data['validPaths'];

                    $('#myModal').modal('hide');
                    $("#tablePathsDiv").show();
                    $("#pathList").empty();

                    paths = [];
                    var allNodesTmp = [];
                    $.each(data['validPaths'], function (graphIndex, graph) {

                        var curGraphPathCont = "<div class='radio'>"
                                + "<label style='margin: 15px;'>"
                                + "<input name='selectedGraphPath' value='" + graphIndex + "' type='radio' />"
                                + "<p class='inline' style='margin: 0px 20px 10px;'>";

                        color = "0C85CE";
                        if (graphIndex == data['shortestPathIndex'] || (graph.length) == data['shortestPathLength']) {
                            color = "FFAA00";
                        }
                        var tmpPath = [];
                        $.each(graph, function (nodeIndex, node) {

                            tmpPath.push(node);
                            curGraphPathCont += "<img style='width: 70px; height: 70px;' src='http://placehold.it/85x80/" + color + "/fff&text=" + node + "' class='img-circle'>";

                            if (nodeIndex != graph.length - 1) {
                                curGraphPathCont += graphSeparator;
                            }
                            allNodesTmp.push(node);
                        });
                        paths.push(tmpPath);

                        curGraphPathCont += "</p>"
                                + "</label>"
                                + "</div>";

                        $("#pathList").append(curGraphPathCont);
                        if (schemaType == 2) {
                            $('#rootwizard').find('.pager .next').show();
                        } else {
                            $("#tablePathsDiv").hide();
                        }

                    });

                    if (schemaType == 1) {


                        allNodes = $.unique(allNodesTmp);

                        $.each(network.nodes, function (index, node) {
                            if (node.id === tablesArray[0] || node.id === tablesArray[1]) {
                                node.options.color.background = '#ABFFAB';
                            } else {
                                node.options.color.background = '#97C2FC';
                            }

                            if ($.inArray(node.id, allNodes) === -1) {
                                node.options.color.background = '#F3F8FF';
                            }
                        });
                        network.redraw();
                    }
                }
            }
        });
    }

    function getPaths(tablesArray, fieldsArray) {

        $('#myModal').modal('toggle');

        $.ajax({url: "<?php echo site_url('mapping/getPathsFromDatabase'); ?>",
            data: {
                //tukaj ko naredis array vseh izbranih table in field poslji kar cel array
                database: database, // potreben samo database ker bos bral iz xml file
                tables: tablesArray,
                fields: fieldsArray
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
                if (data.length === 0) {
                    alert("Data error!");
                } else {

                    graphPaths = data['validPaths'];

                    $('#myModal').modal('hide');
                    $('#rootwizard').bootstrapWizard('show', 2);

                    $("#pathList").empty();

                    $.each(data['validPaths'], function (graphIndex, graph) {

                        var curGraphPathCont = "<div class='radio'>"
                                + "<label style='margin: 15px;'>"
                                + "<input name='selectedGraphPath' value='" + graphIndex + "' type='radio' />"
                                + "<p class='inline' style='margin: 0px 20px 10px;'>";

                        color = "0C85CE";
                        if (graphIndex == data['shortestPathIndex'] || (graph.length) == data['shortestPathLength']) {
                            color = "FFAA00";
                        }

                        $.each(graph, function (nodeIndex, node) {

                            curGraphPathCont += "<img style='width: 70px; height: 70px;' src='http://placehold.it/85x80/" + color + "/fff&text=" + node + "' class='img-circle'>";

                            if (nodeIndex != graph.length - 1) {
                                curGraphPathCont += graphSeparator;
                            }
                        });
                        curGraphPathCont += "</p>"
                                + "</label>"
                                + "</div>";
                        $("#pathList").append(curGraphPathCont);

                    });
                }
            }
        });
    }

    function getLogMap2MappingResult() {

        $('#myModal').modal('toggle');

        $.ajax({url: "<?php echo site_url('mapping/getLogMap2MappingResult'); ?>",
            data: {
                defaultNamespace: $('input[name="defaultNamespace"]').val(),
                RDFDumpTypeRadioButton: $('input:radio[name=RDFDumpTypeRadioButton]:checked').val(),
                fileSyntaxRadioOptions: $('input[name="fileSyntaxRadioOptions"]').val(),
                destFileName: $('input[name="destFileName"]').val(),
                JenaTDBDirectoryPath: $('input[name="JenaTDBDirectoryPath"]').val(),
                RDFDumpTypeButton: $('input[name="RDFDumpTypeButton"]').val(),
                storeTDBButton: $('input[name="storeTDBButton"]').val(),
                ttlContent4RDF: r2rmlContent,
                selectonto: $('#selectonto option:selected').val(),
                fileGSbutton: GS_filename,
                fileontobutton: ontology_filename,
                gsFileID: GS_file_id,
                ontoFileID: ontology_file_id,
                ontoFileUser: ontology_user_file,
                gsFileUser: GS_user_file,
                database: database
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
                if (data.length === 0) {
                    $('#myModal').modal('hide');
                    alert("Data error!");
                } else {
                    // console.log(data);
                    $('#myModal').modal('hide');
                    $('#rootwizard').bootstrapWizard('show', 5);
                    // $('#TTLSource').val(data['r2rml_content']);
                    var editor_mappings = $('.CodeMirror')[1].CodeMirror;
                    var editor_incom_map = $('.CodeMirror')[2].CodeMirror;
                    var editor_map_rdf = $('.CodeMirror')[3].CodeMirror;
                    var editor_map_owl = $('.CodeMirror')[4].CodeMirror;

                    editor_mappings.setValue(data['logmap2_mappings']);
                    editor_incom_map.setValue(data['incompatible_instance_mappings']);
                    editor_map_rdf.setValue(data['logmap2_mappings_rdf']);
                    editor_map_owl.setValue(data['logmap2_mappings_owl']);

                    $('#precision_result').html(data['precision']);
                    $('#recall_result').html(data['recall']);
                    $('#fmeasure_result').html(data['fmeasure']);
                    $('#threshold_result').html(data['threshold']);
                    $('#mappings_result').html(data['mappings']);
                    $('#matching_time_result').html(data['time']);

                }
            },
            error: function (data) {

                $('#myModal').modal('hide');
                alert('LogMapFRI error!');
            }

        });
    }

    function storeFinalResult2TDB() {

        $('#myModal').modal('toggle');

        var editor = $('.CodeMirror')[4].CodeMirror;
        var final_result_OWL = editor.getValue();


        $.ajax({url: "<?php echo site_url('mapping/saveFinalResult2TDB'); ?>",
            data: {
                owlMappings: final_result_OWL,
                JenaTDBDirectoryPath: $('input[name="JenaTDBDirectoryPath"]').val(),
                database: database
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
                $('#myModal').modal('hide');

                alert("Upload Succesful!");
            },
            error: function (data) {
                $('#myModal').modal('hide');
                alert('LogMapFRI error!');
            }
        });
    }

    function getMappingContent(selecedAttributes, selectedGraphIndex, selectedGraphPath, logicalTableDefinitionMode, tripleMapName) {

        $('#myModal').modal('toggle');

        $.ajax({url: "<?php echo site_url('mapping/getMappingContent'); ?>",
            data: {
                pathIndex: selectedGraphIndex,
                selecedAttributes: selecedAttributes,
                graphPath: selectedGraphPath,
                logTableDefMode: logicalTableDefinitionMode,
                tripleMapName: tripleMapName,
                implied: includeViews,
                includeHTML: includeHTML,
                database: database
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
                if (data.length === 0) {
                    alert("Data error!");
                } else {
                    $('#myModal').modal('hide');
                    $('#rootwizard').bootstrapWizard('show', 3);

                    //                    $('#TTLSource').val(data['r2rml_content']);

                    var editor = $('.CodeMirror')[0].CodeMirror;

                    editor.setValue(data['r2rml_content']);
                }
            }
        });
    }

    $.arrayIntersect = function (a, b) {
        return $.grep(a, function (i)
        {
            return $.inArray(i, b) > -1;
        });
    };

    var network;
    var paths = [];
    var chosenPath = -1;

    function getDatabaseData() {

        $('#myModal').modal('toggle');
        $.ajax({url: "<?php echo site_url('mapping/getSchema'); ?>",
            data: {
                hostname: hostname,
                db_port: db_port,
                username: username,
                password: password,
                selectDatabase: database,
                dbType: dbdriver,
                includeHTML: includeHTML,
                schemaType: schemaType,
                inverse: inverse,
                views: includeViews},
            dataType: "json",
            type: "POST",
            success: function (data) {
                $('#myModal').modal('hide');
                if (data.length == 0) {
                    alert("Data error!");
                } else {
                    $("#schemaSpyArea").hide();

                    $('#rootwizard').bootstrapWizard('show', 1);

                    var nodes = [];
                    var edges = [];

                    //                    alert(schemaType);

                    if (schemaType == 1) {

                        var i = 0;
                        $.each(data['schema_graph'], function (table, children) {
                            $('#schemaLogContent').append(children + '<br />');


                            var tempNode = {id: table, label: table, shape: 'box'};
                            //                            tempNode["id"] = index;
                            //                            tempNode["label"] = value;
                            //                            tempNode["shape"] = "box";

                            $.each(children, function (index, value) {
                                //                            $('#schemaLogContent').append(value + '<br />');
                                var tmpEdge = {id: table + value, from: table, to: value, style: 'arrow', width: 1, length: 200};

                                edges.push(tmpEdge);
                            });

                            nodes.push(tempNode);

                        });


                        var container = document.getElementById('mynetwork');

                        var graphData = {
                            nodes: nodes,
                            edges: edges
                        };
                        var options = {
                            hover: true,
                            edges: {
                                width: 2,
                                color: {
                                    color: 'black',
                                    highlight: '#848484',
                                    hover: '#848484'
//                                    highlight: {
                                            //                                        background: 'pink',
                                            //                                        border: 'red'
                                            //                                    }
                                },
                            },
                            nodes: {
                                radius: 24,
                                color: {
                                    background: '#97C2FC',
                                    border: '#2B7CE9',
                                    highlight: {
                                        background: 'pink',
                                        border: 'red'
                                    }},
                                shape: 'box',
                            }
                        };
                        network = new vis.Network(container, graphData, options);
                        chosenPath = -1;


                        //                        paths.push(["country", "city", "address", "store", "staff"]);
//                        paths.push(["country", "city", "address", "store"]);
                        //                        paths["2a"] = ["1a", "2a"]; 
                        network.on('hoverNode', function (properties) {

                            //                        network._unselectAll(true);
//                    network.highlisghEdges = false;
//                            var selectedPath = paths[properties.node];
                            var pathIndex = -1;
//

                            if (properties.node.length === 0) {
                                network._unselectAll(true);
                                pathIndex = -1;
                            }

                            var minLength = -1;
                            $.each(paths, function (index, value) {
                                if ($.inArray(properties.node, value) !== -1) {

                                    if (minLength < 0 || minLength >= value.length) {
                                        pathIndex = index;
                                        minLength = value.length;
                                    }

                                }
                            });

                            if (pathIndex >= 0) {

                                network._unselectAll(true);
                                network.selectNodes(paths[pathIndex], false);
                                selectPathEdges(paths[pathIndex]);

                                //                                focusNode(properties.node);
                            }

                            //                            $.each(paths, function(index, value) {
                            //                                if ($.inArray(properties.node, value) !== -1 && value.length <= minPathLength) {
                            ////                                    alert();
                            //                                    minPathIndex = index;
                            //                                    minPathLength = value.length;
                            ////                                    network.selectNodes(value, false);
                            ////                                    network.selectEdgesAdditive(["countrycity", "cityaddress"]);
                            ////                                    network._addToSelection("countrycity");
                            //                                }
//
                            //                                if (minPathIndex !== -1) {
                            //                                    network.selectNodes(paths[minPathIndex], false);
                            //                                    selectPathEdges(paths[minPathIndex]);
//                                }
                            //                            });


                        }).on('blurNode', function (properties) {
                            if (chosenPath < 0) {
                                network._unselectAll(true);
                            } else {
                                network._unselectAll(true);
                                var selectedPath = paths[chosenPath];
                                network.selectNodes(selectedPath, false);
                                selectPathEdges(selectedPath);
                            }
                        }).on('select', function (properties) {
                            if (properties.nodes.length === 0) {
                                network._unselectAll(true);
                                chosenPath = -1;
                            }

                            var minLength = -1;
                            $.each(paths, function (index, value) {
                                if ($.inArray(properties.nodes[0], value) !== -1) {

                                    if (minLength < 0 || minLength >= value.length) {
                                        chosenPath = index;
                                        minLength = value.length;
                                    }

                                }
                            });

                            if (chosenPath >= 0) {

                                network._unselectAll(true);
                                network.selectNodes(paths[chosenPath], false);
                                selectPathEdges(paths[chosenPath]);
                                $('input[name=selectedGraphPath]')[chosenPath].checked = true;

                                $('#rootwizard').find('.pager .next').show();
                                focusNode(properties.node);
                            }

                        }).on('doubleClick', function (properties) {
                            //                            network._unselectAll(true);
                            //                            network.highlisghEdges = false;
                            //                            var selectedPath = paths[properties.node];
                            //                            alert('selected items: ' + properties.nodes);
                            //                            if(properties.nodes.length > 1){
                            //                                network.selectNodes(properties.nodes[0], false);
                            //                            } else{
                            //                                network.selectNodes(properties.nodes, false);
                            //                            }
                            //                            $.each(network.nodes, function(index, node) {
                            ////                                node.options.color.background = 'red';
                            //                                if (node.id === "country" || node.id === "store") {
                            //                                    node.options.color.background = 'white';
                            ////                                    node.options.fontColor = 'white';
////                                    node.color = 'black';
////                                    node.options.radius = 50;
////                                    node.baseRadiusValue = 50;
                            //                                }
////                                $('#schemaLogContent').append(value + '<br />');
//                            });
                            //                            network.redraw();
//                            alert('selected items: ' + properties.nodes);
                            //                            var selectedPath = paths[properties.node];
                            //                            pathChoosed = properties.node;
                        });


                    } else {

                        $('#schemaLogContent').text("");
                        $('#directedGraphArea').hide();
                        $('#getAllPossiblePathsButton').hide();
                        $('#schemaSpyArea').show();


                        $.each(data['schema_log'], function (index, value) {
                            $('#schemaLogContent').append(value + '<br />');
                        });
                        $("#databaseSchemaLink").attr("href", "<?php echo base_url(); ?>dump/diagrams/summary/relationships.real.large.png");

                        $("#databaseSchemaImage").attr("src", "<?php echo base_url(); ?>dump/diagrams/summary/relationships.real.compact.png?v=" + (new Date()).getTime()).error(function () {
                            $("#databaseSchemaImage").attr("src", "http://placehold.it/800x400/99CCFF/FFF&text=Schema%20image%20");
                            $("#databaseSchemaLink").attr("href", "#");
                        });
                    }

                    $('#rootwizard').bootstrapWizard('show', 1);
                    $('#rootwizard').find('.pager .next').hide();
                    $('#selectedDatabase').text(data['selected_db']);

                    tableData = data['tables'];

                    setTableSelectionData(1);
                    setTableSelectionData(2);

                }
            },
            //            error: function(XMLHttpRequest, textStatus, errorThrown) {
//                alert("Status: " + textStatus);
            //                alert("Error: " + errorThrown);
//            }
        });
    }

    $('.lstTables').on('change', function () {
        var selectedStartTable = $("#table_selection1 option:selected").val();
        var selectedEndTable = $("#table_selection2 option:selected").val();
        var inverted = true;

        if (selectedStartTable != 0 && selectedEndTable != 0) {

            selectedTablesArray = new Array();
            selectedTablesArray.push(selectedStartTable);
            selectedTablesArray.push(selectedEndTable);

            //        getGraphTablesFields(selectedGraphPath);
            getAllPossiblePaths(selectedTablesArray);

        }

    });

    function selectPathEdges(path) {

        var pathEdges = [];
        $.each(path, function (index, value) {

            if (index < path.length - 1) {
                pathEdges.push(path[index] + path[index + 1]);

            }
        });

        network.selectEdgesAdditive(pathEdges);
    }

    function focusNode(nodeId) {
        var offsetx = 0;
        var offsety = 0;
        var duration = 2000;
        var scale = 0.85;
        var easingFunction = "easeInQuad";

        var options = {
            // position: {x:positionx,y:positiony}, // this is not relevant when focusing on nodes
            scale: scale,
            offset: {x: offsetx, y: offsety},
            animation: {
                duration: duration,
                easingFunction: easingFunction
            }
        }
//        statusUpdateSpan.innerHTML = "Focusing on node: " + nodeId;
        //        finishMessage = "Node: " + nodeId + " in focus.";
        network.focusOnNode(nodeId, options);
    }

    function setTableSelectionData(tableSelectionID) {
        $.each(tableData, function (index, value) {
            var selector = '#table_selection' + tableSelectionID;
            $(selector).append('<option value="' + value + '">' + value + '</option>');
        });



        $(".removeSelectionButton").on("click", function () {
            var temp_id = 'selectionFieldForm' + this.id.substr(this.id.length - 1);
            temp_id = temp_id.toString();
//            console.log(temp_id);

            $(temp_id).remove();
        });
    }

    function capitaliseFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function getGraphTablesFields(selectedGraph) {

        //        var selectFieldId = id.substr(id.length - 1);
        $('#myModal').modal('toggle');
        $.ajax({url: "<?php echo site_url('mapping/getGraphTablesFields'); ?>",
            data: {
//                hostname: "localhost",
                //                db_port: db_port,
//                username: "root",
                //                password: "",
                //                database: "sakila",
                //                db_table: selectedGraph,
//                dbdriver: "mysql"
                hostname: hostname,
                db_port: db_port,
                username: username,
                password: password,
                database: database,
                db_table: selectedGraph,
                dbdriver: dbdriver
            },
            dataType: "json",
            type: "POST",
            success: function (data) {

                if (data.length == 0) {
                    alert("Data error!");
                } else {
                    $('#rootwizard').bootstrapWizard('show', 2);
                    $('#myModal').modal('hide');

                    var graphTablesDiv = $('#tablesFieldsComboBox');
                    graphTablesDiv.empty();

                    defaultTripleMapName = "";
                    $.each(data, function (index, node) {

                        $(graphTablesDiv).append('<label>' + index + '</label>');
                        $(graphTablesDiv).append('<ul id="table_' + index + '" class="lstTables source" ></ul>');

                        tableComboBox = $('#table_' + index);
                        $.each(node, function (attr_index, value) {

                            $(tableComboBox).append('<li data-table="' + index + '" data-primary-key="' + value['primary_key'] + '" data-attribute="' + value['name'] + '" id="' + index + '_' + value['name'] + '">' + value['name'] + '<a id="test_' + index + '" class="btn btn-link btn-table-attr" style="float: right; padding: 0 5px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></li>');
                        });

                        defaultTripleMapName += capitaliseFirstLetter(index);
                    });

                    $("#tripleMapNameID").val(defaultTripleMapName);

                    $(".source, .target").sortable({
                        connectWith: ".connected"
                    });

                    $(".source li").draggable({
                        addClasses: false,
                        cursor: "pointer",
                        cursorAt: {top: -12, left: -20},
                        start: function () {
//                            var link = $("<li><a href='#' class='dismiss'>asdfasdfasdfasdf</a></li>");
                            $(".target").append("<li class='sortable-placeholder'></li>");

//                            $(list).attr("id", ui.draggable.attr("id") + "_target");
//                            $(list).attr("data-table", ui.draggable.attr("data-table"));
//                            $(list).attr("data-primary-key", ui.draggable.attr("data-primary-key"));
//                            $(list).attr("data-attribute", ui.draggable.attr("data-attribute"));

//                            $(list).append(link);
                        },
                        stop: function () {
                            $(".target").find(".sortable-placeholder").remove();
                        },
                        //                        helper: "clone",
                        helper: function (event) {

                            var list = $("<li></li>").text($(this).text());

                            $(list).attr("id", $(this).attr("id") + "_target");
                            $(list).attr("data-table", $(this).attr("data-table"));
                            $(list).attr("data-primary-key", $(this).attr("data-primary-key"));
                            $(list).attr("data-attribute", $(this).attr("data-attribute"));

                            return list;
                        }

                    }).on("click", ".btn-table-attr", function (event) {
                        event.preventDefault();

                        var target = $(".target");
                        var curSrcElem = $(this).parent();

                        if (target.find("#" + curSrcElem.attr("id") + "_target").length != 0 || target.find("#" + curSrcElem.attr("id")).length != 0) {
                            alert("Element already exists in a table");
                            return false;
                        }
                        $(".target").find(".placeholder").remove();


                        var link = $("<a href='#' class='dismiss'>x</a>");
                        var list = $("<li></li>").text(curSrcElem.text());

                        $(list).attr("id", curSrcElem.attr("id") + "_target");
                        $(list).attr("data-table", curSrcElem.attr("data-table"));
                        $(list).attr("data-primary-key", curSrcElem.attr("data-primary-key"));
                        $(list).attr("data-attribute", curSrcElem.attr("data-attribute"));

                        $(list).append(link);
                        $(list).appendTo($(".target"));


                    });

                    $(".target").droppable({
                        addClasses: false,
                        activeClass: "listActive",
                        //                        accept: ":not(.ui-sortable-helper)",
                        drop: function (event, ui) {

                            $(this).find(".placeholder").remove();
                            var link = $("<a href='#' class='dismiss'>x</a>");
                            var list = $("<li></li>").text(ui.draggable.text());

                            $(list).attr("id", ui.draggable.attr("id") + "_target");
                            $(list).attr("data-table", ui.draggable.attr("data-table"));
                            $(list).attr("data-primary-key", ui.draggable.attr("data-primary-key"));
                            $(list).attr("data-attribute", ui.draggable.attr("data-attribute"));
                            $(list).append(link);
                            $(list).appendTo(this);
                            //                            updateValues();
                        },
                        accept: function (draggable) {
                            if ($(this).find("#" + draggable.attr("id") + "_target").length != 0) {
                                return false;
                            } else if ($(this).find("#" + draggable.attr("id")).length != 0) {
                                return false;
                            }
                            return true;
                        }
                    }).sortable({
                        items: "li:not(.placeholder)",
                        sort: function () {
                            $(this).removeClass("listActive");
                        },
                        update: function () {
                            //                            updateValues();
                        }
                    }).on("click", ".dismiss", function (event) {
                        event.preventDefault();
                        $(this).parent().remove();
                        //                        updateValues();
                    });
                }

                $('.affixed-element').width($('.parrent-affix').width());

            }
        });
    }

    function resetFieldImput() {
        $('#table_selection').empty();
        $('#selectedDatabase').empty();
        $('#schemaLogContent').text("");
        $("#databaseSchemaLink").attr("href", "#");
        $("#databaseSchemaImage").attr("src", defaultImage);
        $('.lstTables').empty();
        $('.lstTablesField').empty();

    }

</script>