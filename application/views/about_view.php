<?php $this->load->helper('url'); ?>
<?php $this->load->helper('html'); ?>
<?php $this->load->helper('form'); ?>

<div class="container">
    <div class="row">            
        <div class="col-md-12 well well-lg">
            <div class="headline">
                <h2>LodMapFRI - RDB Data Enrichment Tool.</h2>
            </div>
            
            <br />
            
            Enrichment of data in relational database with Linked Data.
        </div>
    </div>
</div>