<?php $this->load->helper('url'); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon-32x32.png" sizes="32x32" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon-16x16.png" sizes="16x16" rel="stylesheet">

        <title>LodMapFRI</title>

        <!-- Lib core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <!--<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">-->
        <!--<link href="<?php echo base_url(); ?>assets/bwizard/css/bwizard.min.css" rel="stylesheet" />-->

        <!-- Custom styles -->
        <link href="<?php echo base_url(); ?>assets/css/main_template.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/select2/select2.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/select2/select2-bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/bootstrap-transfer/css/bootstrap-transfer.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/codemirror/lib/codemirror.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/codemirror/addon/dialog/dialog.css" rel="stylesheet" type="text/css"/>


        <link href="<?php echo base_url(); ?>assets/vis/dist/vis.css" rel="stylesheet">

        <style>
            .CodeMirror {
                border: 1px solid #eee;
                height: 500px;
                width: 100%;
                _position: relative; /* IE6 hack */
            }

            :checked + p { 
                /*border: 3px solid #90D0F5;*/ 
                /*padding: 5px;*/
                padding: 5px;
                border-color: rgba(82, 168, 236, 0.8);
                outline: 0px none;
                box-shadow: 0px 0px 8px rgba(82, 168, 236, 0.6);
                /*font-weight: bold;*/ 
            }

            ul.source, ul.target {
                min-height: 50px;
                margin: 10px 0px 10px 0px;
                padding: 2px;
                border-width: 1px;
                border-style: solid;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                list-style-type: none;
                list-style-position: inside;
            }

            ul.source {
                border-color: #f8e0b1;
                /*background-color: #fff;*/
            }

            #tablesFieldsComboBox > label {
                margin-bottom: 5px;
                margin-top: 5px;
            }

            ul.source {
                border-color: #f8e0b1;
                background-color: #fff;
            }

            ul.target {
                border-color: #add38d;
            }

            .source li, .target li {
                margin: 5px;
                padding: 5px;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
                z-index: 1000;
                cursor: pointer;
            }
            .source li {
                background-color: #fcf8e3;
                border: 1px solid #fbeed5;
                color: #c09853;
                z-index: 1000;
            }
            .target li {
                background-color: #ebf5e6;
                border: 1px solid #d6e9c6;
                color: #468847;
                z-index: 1000;
            }
            .sortable-dragging {
                border-color: #ccc !important;
                background-color: #fafafa !important;
                color: #bbb !important;
            }
            .sortable-placeholder {
                height: 40px;
            }
            .source .sortable-placeholder {
                border: 2px dashed #f8e0b1 !important;
                background-color: #fefcf5 !important;
            }
            .target .sortable-placeholder {
                border: 2px dashed #add38d !important;
                background-color: #f6fbf4 !important;
            }

            body > li {
                width: 177px;
                margin: 5px;
                padding: 5px;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
                list-style-type: none;
                list-style-position: inside;
                border-width: 1px;
                border-style: solid;
                border-color: #ccc !important;
                background-color: #fafafa !important;
                color: #bbb !important;
                z-index: 1000;
            }
            .listActive {
                border: 1px solid #ccc;
                background-color: #fcfcfc;
                /*padding: 0.5em 0 3em 0 !important;*/
                z-index: 500;
            }
            .placeholder {
                list-style-type: none;
                text-align: center;
                font-style: italic;
                border: 1px dashed #ddd !important;
                background-color: #fff !important;
                color: #aaa !important;
            }
            .dismiss {
                float: right;
                position: relative;
                top: -8px;
                line-height: 20px;
                font-size: 12px;
                font-weight: bold;
                text-decoration: none !important;
                color: #468847;
            }

            .dropdown-menu>li>a:hover, .dropdown-menu>li>a:focus {
                background-color: #5BC0DE;
            }

            @media(max-width:992px){
                .affix {
                    position: static;
                    width: auto;
                    top: 0;
                }
            }
        </style>

        <script src="<?php echo base_url(); ?>assets/vis/dist/vis.js"></script>

        <!--[if lt IE 9]><script src="<?php echo base_url(); ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<?php echo base_url(); ?>assets/js/ie-emulation-modes-warning.js"></script>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
        <!--<script src="http://code.jquery.com/jquery-latest.js"></script>-->
        <script src="<?php echo base_url(); ?>assets/jquery-ui-1.11.2/external/jquery/jquery.js"></script>

<!--<script src="../../../assets/jqueryajaxupload/ajaxfileupload.js" type="text/javascript"></script>-->
        <script src="<?php echo base_url(); ?>assets/jqueryajaxupload/ajaxfileupload.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/codemirror/lib/codemirror.js"></script>
        <script src="<?php echo base_url(); ?>assets/codemirror/mode/turtle/turtle.js"></script>

        <script src="<?php echo base_url(); ?>assets/codemirror/addon/search/search.js"></script>
        <script src="<?php echo base_url(); ?>assets/codemirror/addon/search/searchcursor.js"></script>
        <script src="<?php echo base_url(); ?>assets/codemirror/addon/search/match-highlighter.js"></script>

        <script src="<?php echo base_url(); ?>assets/codemirror/addon/dialog/dialog.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>assets/jquery-ui-1.11.2/jquery-ui-1.10.3.custom.min.js"></script>
        <!--<script src="<?php echo base_url(); ?>assets/html5sortable/jquery.sortable.js"></script>-->


        <!-- Include the plugin's CSS and JS: -->
<!--        <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" type="text/css"/>-->


    </head>

    <body>
        <!-- Navbar menu -->
        <?php print $headerContent; ?>

        <!-- Page content -->
        <?php print $mainContent; ?>

        <!-- Page footer -->
        <?php print $footerContent; ?>
    </body>
</html>
