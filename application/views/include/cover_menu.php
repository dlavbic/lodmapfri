<?php $this->load->helper('url'); ?>

<div class="masthead clearfix">
    <div class="inner">
        <h3 class="masthead-brand">
            <img src="<?php echo base_url(); ?>assets/img/icon_rdf_white-48.png" alt="App Logo" class="img-responsive inline pull-left" /> <span style="padding-top: 10px; padding-left: 15px;float: left;position: relative;">LodMapFRI</span>
        </h3>
        <ul class="nav masthead-nav">
            <li <?php if ($this->uri->uri_string() == 'home'): ?> class="active"<?php endif; ?>>
                <a href="<?php echo base_url("index.php/home"); ?>">About</a>
            </li>
            <li <?php if ($this->uri->uri_string() == 'contact'): ?> class="active"<?php endif; ?>>
                <a href="<?php echo base_url("index.php/contact"); ?>">Contact</a>
            </li>
        </ul>
    </div>
</div>