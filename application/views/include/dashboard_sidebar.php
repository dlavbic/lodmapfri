<?php $this->load->helper('url'); ?>

<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li <?php if ($this->uri->uri_string() == 'dashboard'): ?> class="active"<?php endif; ?>><a class="disabled" href="">Set Database Parameters</a></li>
        <li <?php if ($this->uri->uri_string() == 'dashboard/getSchema'): ?> class="active"<?php endif; ?>><a class="disabled" href="">DB Schema</a></li>
        <li><a href="">Edit R2RML</a></li>
        <li><a href="">Export</a></li>
    </ul>
</div>