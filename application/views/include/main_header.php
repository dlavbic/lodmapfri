<?php $this->load->helper('url'); ?>

<div class="navbar navbar-inverse navbar-fixed-top " role="navigation">
    <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand" href="#">

            </a>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" style="color: white;" href="<?php echo base_url("index.php/mapping"); ?>"><img style="margin-right: 3px;" alt="Brand" src="<?php echo base_url("/assets/img/icon_rdf_white-24.png"); ?>"> LodMapFRI</a>
            <!--<a class="navbar-brand" href="<?php echo base_url("index.php/mapping"); ?>">RDFmapGen</a>-->

        </div>

        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo base_url("index.php/home"); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                <!--<li class="active"><a href="#">Home</a></li>-->
                <!--<li><a href="<?php echo base_url("index.php/mapping"); ?>">Mapping</a></li>-->

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> 
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo base_url("index.php/LexicalUtilities"); ?>">Lexical Utilities: Synonyms</a></li>
                        <li><a href="<?php echo base_url("index.php/LexicalUtilitiesLexSpelling"); ?>">Lexical Utilities: spelling variants map</a></li>
                        <li><a href="<?php echo base_url("index.php/LexicalUtilitiesLexPlurals"); ?>">Lexical Utilities: Plurals</a></li>
                        <li><a href="<?php echo base_url("index.php/LexicalUtilitiesLexNorm"); ?>">Lexical Utilities: Plurals+normalization</a></li>
                        <li><a href="<?php echo base_url("index.php/LexicalUtilitiesStopwords"); ?>">Lexical Utilities: Stop words</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url("index.php/logmap2params"); ?>">Edit LogMapFRI Parameters</a></li>
                        
                    </ul>
                </li>

                <li><a href="<?php echo base_url("index.php/help"); ?>" alt="Help" ><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></li>
                <li><a href="<?php echo base_url("index.php/about"); ?>" alt="About"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a></li>
                <li><a href="<?php echo base_url("index.php/contact"); ?>"><span alt="Contact" class="glyphicon glyphicon-envelope" aria-hidden="true"></span></a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>