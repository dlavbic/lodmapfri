<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('url', 'date');
        $this->load->dbutil();
        $this->load->database();
    }

    public function index() {
        $dbs = $this->dbutil->list_databases();

        $data = array(
            'headerContent' => $this->load->view('include/dashboard_header', array(), TRUE),
            'sidebarContent' => $this->load->view('include/dashboard_sidebar', array(), TRUE),
            'mainContent' => $this->load->view('dashboard_database_view', array('dbs' => $dbs), TRUE),
            'footerContent' => $this->load->view('include/dashboard_footer', array(), TRUE),
        );
        $this->load->view('templates/dashboard_template', $data);
    }

    public function getDatabaseData() {
        $this->load->model('test_model', '', TRUE);
        $data = $this->test_model->get_tables();

        echo json_encode($data);
    }

    public function getSchema() {

        $user_database = $this->input->post('selectDatabase', TRUE);

        $command = 'java -jar C:\Users\dbalija\Desktop\schemaSpy.jar -t mysql -host localhost -dp "C:\Program Files (x86)\MySQL\MySQL Connector J\mysql-connector-java-5.1.32-bin.jar" -gv "C:\Program Files (x86)\Graphviz2.38" -db ' . $user_database . ' -u root -o "C:\xampp\htdocs\FRI_MAG_RDFizing\TEST"';
        exec($command, $result);

//        echo '<pre>';
//        var_dump($result);
//        echo '</pre>';
//        exit();

        $config['hostname'] = "localhost";      // $this->input->post('dbHost', TRUE);
        $config['username'] = "root";           //$this->input->post('database_username', TRUE); $this->input->post('dbUser', TRUE);
        $config['password'] = "";               //$this->input->post('dbPassword', TRUE);
        $config['database'] = $user_database;   // $this->input->post('selectDatabase', TRUE);
        $config['dbdriver'] = $this->input->post('dbType', TRUE);
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;

        $this->load->model('test_model', '', $config);

        $CI = & get_instance();
        $CI->test_model = $this->load->database($config, TRUE);

        $tables = $this->test_model->list_tables();

        $data = array(
            'headerContent' => $this->load->view('include/dashboard_header', array(), TRUE),
            'sidebarContent' => $this->load->view('include/dashboard_sidebar', array(), TRUE),
            'mainContent' => $this->load->view('dashboard_schema_view', array('tables' => $tables, 'selected_db' => $user_database, 'schema_log' => $result), TRUE),
            'footerContent' => $this->load->view('include/dashboard_footer', array(), TRUE),
        );
        $this->load->view('templates/dashboard_template', $data);
    }

    public function getMappingFile() {

//        $userDatabase = $this->input->post('database_name', TRUE);
//        $this->load->model('test_model');
        $table_list = $this->input->post('table_list', TRUE);


        libxml_use_internal_errors(true);


        $map = array(
            '&nbsp;' => ' ',
            'Â' => ' ',
        );


        foreach ($table_list as $table) {
            echo $table;

            echo '<br />';
            echo '<br />';
            echo '<br />';

            $fileEndEnd = file_get_contents('TEST/tables/' . $table . '.html');
//            mb_convert_encoding($file, $to_encoding)
//            $fileEndEnd = mb_convert_encoding($fileEndEnd, 'UTF-8', mb_detect_encoding($fileEndEnd, 'UTF-8', true));
//            $fileEndEnd = mb_convert_encoding($fileEndEnd, "ISO-8859-1", mb_detect_encoding($fileEndEnd, "UTF-8, ISO-8859-1, ISO-8859-15", true));


            $fileEndEnd = mb_convert_encoding($fileEndEnd, "UTF-8", "ISO-8859-1");

//            $fileEndEnd = mb_convert_encoding($fileEndEnd, 'HTML-ENTITIES', "UTF-8");

            $dom = new DOMDocument();
            $html = $dom->loadHTML($fileEndEnd);

            //discard white space 
            $dom->preserveWhiteSpace = false;

            //the table by its tag name
            $html_tables = $dom->getElementsByTagName('table');

            //get all rows from the table
//            $tbody = $tables->getElementsByTagName('tbody');
//            $tble = $html_tables->getElementById('columns');

            $main_table = $html_tables->item(5);
//            $main_table = $html_tables->getElementById('columns');

            $rows = $main_table->childNodes->item(10)->getElementsByTagName('tr');

            // PRINT TABLE
            echo "<table class='table table-bordered' border='1'>";
            echo "<thead align='left'>";
            echo "<tr>";
            echo "<th>Column</th>";
            echo "<th>Type</th>";
            echo "<th>Size</th>";
            echo "<th title='Are nulls allowed?'>Nulls</th>";
            echo "<th title='Is column automatically updated?'>Auto</th>";
            echo "<th title='Default value'>Default</th>";
            echo "<th title='Columns in tables that reference this column'><span class='notSortedByColumn'>Children</span></th>";
            echo "<th title='Columns in tables that are referenced by this column'><span class='notSortedByColumn'>Parents</span></th>";
            echo "<th title='Comments' class='comment'><span class='notSortedByColumn'>Comments</span></th>";
            echo "</tr>";
            echo "</thead>";

            echo "<tbody valign='top'>";
//            echo "<tr class='odd'>";

            $result = array();

            foreach ($rows as $row_index => $row) {
                echo "<tr>";
                $cols = $row->getElementsByTagName('td');

                foreach ($cols as $value) {

                    $val_class = $value->getAttribute('class');

                    if (strcmp($val_class, 'relatedTable detail') == 0) {
                        
                    }
//                    elseif ($value->childNodes->length > 1) {
////                        print_r($value->childNodes->item(0).childNodes);
//                        echo '<br />';
////                        print_r($value->childNodes->item(1).childNodes);
//                        echo '<br />';
//                    } 
                    elseif (strcmp($val_class, 'constraint detail') == 0) {
                        
                    } elseif (strcmp($value->getAttribute('title'), "Automatically updated by the database") == 0) {
                        echo "<td class='" . $value->getAttribute('class') . "' title='" . $value->getAttribute('title') . "'>" . "Yes" . "</td>";
                    } else {
                        echo "<td class='" . $value->getAttribute('class') . "' title='" . $value->getAttribute('title') . "'>" . iconv("UTF-8", "ISO-8859-1", $value->nodeValue) . "</td>";
                    }
                }
                echo "</tr>";
            }


//            echo '<pre>';
//            echo json_encode($html_tbl);
//            echo '</pre>';

            exit();

            $rows = $tble->getElementsByTagName('tr');


//            exit();
            // loop over the table rows
            foreach ($rows as $row) {
                // get each column by tag name
                $cols = $row->getElementsByTagName('td');
                // echo the values  
                echo $cols->item(0)->nodeValue . '<br />';
                echo $cols->item(1)->nodeValue . '<br />';
                echo $cols->item(2)->nodeValue;
            }
//            exit();
        }




//        echo '<pre>';
//        var_dump($this->input->post());
//        echo '</pre>';
//        echo '<br />';
//        echo '<br />';
//        echo '<br />';
//        echo '<pre>';
////        var_dump($data[$table]);
//        echo '</pre>';
//        exit();
//        $data = array();
//        foreach ($table_list as $table) {
//            $data[$table] = $this->test_model->get_fields($table);
////            $r2rml_txt = sprintf("<TriplesMap%s>\n", $table);
////            $r2rml_txt .= "a rr:TriplesMapClass;
//            \n";
////            $r2rml_txt .= sprintf("rr:logicalTable [rr:tableName '%s'];
//            \n", $table);
////            $r2rml_txt .= sprintf("rr:subjectMap [rr:template 'http://example.com/%s/%s/{%s}';
//            rr:class ex:%s];
//            \n", $table);
//
//            foreach ($data[$table] as $field_index => $field) {
//                echo $field->name;
//                echo '<br />';
//                echo $field->type;
//                echo '<br />';
//                echo $field->max_length;
//                echo '<br />';
//                echo $field->primary_key;
//                echo '<br />';
//                echo '------------------------------------';
//                echo '<br />';
//            }
//
////            $file = fopen("r2rml_files/" . $table . ".r2rml", "w");
////
////            echo fprintf($file, "<TriplesMap%s>
////    a rr:TriplesMapClass;
////    
////    rr:logicalTable [rr:tableName  '%s'] ;    
////
////    rr:subjectMap [ rr:template 'http://example.com/%s/%s/{%s}' ; rr:class ex:%s ] ;", $table, $table, $userDatabase, $table);
//        }
//
        echo json_encode($data);
        exit();
    }

}
