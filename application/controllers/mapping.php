<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mapping extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('url', 'date', 'file');
        $this->load->dbutil();
        $this->load->database();
    }

    public function index() {


        $dbs = $this->dbutil->list_databases();

        $data = array(
            'headerContent' => $this->load->view('include/main_header', array(), TRUE),
            'mainContent' => $this->load->view('mapping_view', array('dbs' => $dbs), TRUE),
            'footerContent' => $this->load->view('include/main_footer', array(), TRUE),
        );
        $this->load->view('templates/main_template', $data);
    }

    public function upload_ontology() {
        $status = "";
        $msg = "";
        $filename = "";
        $file_element_name = 'ontoUserFile';

        $target_path = $_FILES['ontoUserFile']['name'];

        if ($status != "error") {
            $config['upload_path'] = UPLOAD_PATH;
            $config['allowed_types'] = 'owl|rdf|ttl|n3|txt';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                $filename = $data['file_name'];

                if (!empty($data)) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg, 'filename' => $target_path, 'file_id' => $filename));
    }

    public function upload_GSMappings() {
        $status = "";
        $msg = "";
        $filename = "";
        $file_element_name = 'GSMappingsUserFile';

        $target_path = $_FILES['GSMappingsUserFile']['name'];

        if ($status != "error") {
            $config['upload_path'] = UPLOAD_PATH;
            $config['allowed_types'] = 'owl|rdf|ttl|n3|txt';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                $filename = $data['file_name'];

                if (!empty($data)) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg, 'filename' => $target_path, 'file_id' => $filename));
    }

    public function getDatabaseData() {
        $this->load->model('test_model', '', TRUE);
        $data = $this->test_model->get_tables();

        echo json_encode($data);
    }

    private function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir")
                        $this->rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function getSchema() {
        $this->rrmdir("/" . DATABASE_SCHEMA_PATH);

        $includeViews = intval($this->input->post('views', TRUE));
        $includeHTML = intval($this->input->post('includeHTML', TRUE));
        $user_database = $this->input->post('selectDatabase', TRUE);
        $schemaType = $this->input->post('schemaType', TRUE);
        $inverse = $this->input->post('inverse', TRUE);

        $graphviz_path = empty(GRAPHVIZ_PATH) ? "lib/Graphviz2.38" : GRAPHVIZ_PATH;

        $command = 'java -jar lib/schemaSpy/schemaSpy.jar'
                . ' -t mysql'
                . ' -host ' . $this->db->hostname
                . ' -dp lib/mysql-connector-java/mysql-connector-java-5.1.33-bin.jar'
                . ' -gv ' . $graphviz_path
                . ' -db ' . $user_database
                . ' -u ' . $this->db->username
                . ' -o ' . DATABASE_SCHEMA_PATH
                . ' -lq'
                . ' -norows'
                . ' -maxdet "300"'
                . ' -nologo';

        if (!empty($this->db->password)) {
            $command .= ' -p ' . $this->db->password . ' ';
        }

        if ($includeViews == 0) {
            // $command .= '-noimplied ';
            $command .= '-noviews ';
        }

        if ($includeHTML == 0) {
            $command .= '-nohtml ';
        }

        exec($command, $result);

        $config['hostname'] = $this->db->hostname;
        $config['username'] = $this->db->username;
        $config['password'] = $this->db->password;
        $config['database'] = $user_database;
        $config['dbdriver'] = $this->db->dbdriver; //$this->input->post('dbType', TRUE);
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;

        $CI = & get_instance();
        $CI->test_model = $this->load->database($config, TRUE);

        if ($schemaType == 1) {
            $graph = $this->getDatabaseDirectedGraph($user_database, false, $inverse);
//            $g = new Graph($graph);
            $data['schema_graph'] = $graph;
        }

        $data['tables'] = $this->test_model->list_tables();
        $data['selected_db'] = $user_database;
        $data['schema_log'] = $result;

        echo json_encode($data);
    }

    public function getGraphTablesFields() {

        $database = $this->input->post('database', TRUE);
        $graph = $this->input->post('db_table', TRUE);

        $config['hostname'] = $this->db->hostname;
        $config['username'] = $this->db->username;
        $config['password'] = $this->db->password;
        $config['database'] = $database;
        $config['dbdriver'] = $this->db->dbdriver;
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $this->load->model('test_model', '', TRUE);

        $CI = & get_instance();
        $CI->test_model = $this->load->database($config, TRUE);

        $result = array();
        foreach ($graph as $node) {
            $result["$node"] = $this->test_model->field_data($node);
        }

        echo json_encode($result);
    }

    function getTablePK($dbName, $tableName) {

        $xmlFile = DATABASE_SCHEMA_PATH . "/$dbName.xml";

        if (file_exists($xmlFile)) {

            $xml = simplexml_load_file($xmlFile);

            $result = $xml->xpath("/database/tables/table[@name='" . $tableName . "']/index[@name='PRIMARY']/column");

            if (empty($result)) {
                return 0; // nima starsev
            }

            return ($result[0]['name']);
        } else {
            exit("NAPAKA: $xmlFile.");
        }
    }

    function getAllTableChildren($dbName, $tableName) {

        $tablePK = $this->getTablePK($dbName, $tableName);
//            $tablePK = $tableName . "_id";

        $xmlFile = DATABASE_SCHEMA_PATH . "/$dbName.xml";
        $xml = simplexml_load_file($xmlFile);

        if (file_exists($xmlFile)) {

            $parents = $xml->xpath("/database/tables/table[@name='" . $tableName . "']//parent");
            $children = $xml->xpath("//table[@name='" . $tableName . "']/column[@name='" . $tablePK . "']/child");

            $result = array_merge($children, $parents);

            if (empty($result)) {
                return array(); // nima starsev
            }

            $childrenTable = array();
            foreach ($result as $index => $val) {
                $childrenTable[$index] = (string) $val['table'];
                // $childrenTable[(string) $val['table']] = (string) $val['column'];
            }

            return $childrenTable;
        } else {
            exit("NAPAKA: $xmlFile.");
        }
    }

    function getTableFieldsFromSS($dbName, $tableName, $PK = false) {
        $tableChild = $this->getAllTableChildren($dbName, $tableName);

        $xmlFile = DATABASE_SCHEMA_PATH . "/$dbName.xml";
        $xml = simplexml_load_file($xmlFile);

        if (file_exists($xmlFile)) {

            $fields = $xml->xpath("/database/tables/table[@name='" . $tableName . "']/column");

            if (empty($fields)) {
                return array(); // nima starsev
            }

            $childrenTable = array();
            foreach ($fields as $index => $val) {

                if (!$PK && count($val) == 1) {
                    continue;
                }
                $childrenTable[$index] = array('name' => (string) $val['name'], 'type' => (string) $val['type']);
            }

            return $childrenTable;
        } else {
            exit("NAPAKA: $xmlFile.");
        }
    }

    public function getPathsFromDatabase() {

        $database = $this->input->post('database', TRUE);
        $tablesArray = $this->input->post('tables', TRUE);
        $fieldsArray = $this->input->post('fields', TRUE);
        $inverseGraph = $this->input->post('inverse', TRUE);
        $graph = $this->getDatabaseDirectedGraph($database, false, $inverseGraph);
        $g = new Graph($graph);
        $start = $tablesArray[0];
        $goal = $tablesArray[1];
        $validPaths = $g->breadthFirstSearchPaths($start, $goal);

        $data = array();
        if (count($validPaths) > 0) {

            $shortestPathIndex = 0;
            $shortestPathLength = count($validPaths[$shortestPathIndex]);

            foreach ($validPaths as $pathIndex => $path) {
                $tmpPathLength = count($path);
                if ($tmpPathLength < $shortestPathLength) {
                    $shortestPathLength = $tmpPathLength;
                    $shortestPathIndex = $pathIndex;
                }
            }

            $data['validPaths'] = $validPaths;
            $data['shortestPathIndex'] = $shortestPathIndex;
            $data['shortestPathLength'] = $shortestPathLength;
        } else {
            $validPaths[0] = array($start);
            $validPaths[1] = array($goal);

            $data['validPaths'] = $validPaths;
            $data['shortestPathIndex'] = -1;
            $data['shortestPathLength'] = 1;
        }
        echo json_encode($data);
    }

    public function getR2RMLMappingSqlQuery($database, $pathIndex, $path, $selecedAttributes, $userClassTripleMapName, $implied) {

        $defaultURI = "http://www.lavbic.net/";
        $schemaURI = $defaultURI . "onto/";     //http://www.lavbic.net/onto/
        $graphURI = $defaultURI . ucfirst($database);   //http://www.lavbic.net/Sakila
        $supportGraphURI = $defaultURI . ucfirst($database) . "SupportGraph";   //http://www.lavbic.net/Sakila

        $firstNode = $path[0];
        $lastNode = $path[count($path) - 1];
        $firstNodeNeighbours = $this->getNeighbours($firstNode, $database, FALSE);
        $lastNodeNeighbours = $this->getNeighbours($lastNode, $database, FALSE);

        $xmlFile = DATABASE_SCHEMA_PATH . "/" . $database . ".xml";
        if (file_exists($xmlFile)) {
            $xml = simplexml_load_file($xmlFile);
        }

        if (empty($userClassTripleMapName)) {
            $userClassTripleMapName = "TriplesMapPath" . ucfirst((string) array_values($path)[0]) . ucfirst((string) end($path));
        }

        $documentConventions = array(
            "@prefix rr: <http://www.w3.org/ns/r2rml#> .",
            "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .",
            "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .",
            "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .",
            "@prefix void: <http://rdfs.org/ns/void#> .",
            "@prefix lavbic-owl: <$schemaURI" . strtolower($database) . "/" . $userClassTripleMapName . "/> .",
            "@prefix owl: <http://www.w3.org/2002/07/owl#> ."
        );

        $query_dummy = "SELECT 1;";
        $query_select = "SELECT CONCAT(";
        foreach ($path as $index => $attribute) {
            $query_select .= $attribute . "." . $this->getTablePK($database, $attribute);

            if ($index != count($path) - 1) {
                $query_select .= ", '-', ";
            }
        }

        $query_select .= ") AS id, ";
        $pom = "";

        //user selected attributes
        foreach ($selecedAttributes as $index => $attribute) {
            //detect pks and fks and ignore them
            if ($attribute->pk || $this->isForeignKeyAttribute($database, $attribute->table, $attribute->attribute)) {
                continue;
            }

            $query_select .= $attribute->table . "." . $attribute->attribute;
            // $query_select .= " as " . $table->table . "_" . $table->attribute;

            if ($index == count($selecedAttributes) - 1) {
                $query_select .= " ";
            } else {
                $query_select .= ", ";
            }
        }

        $query_from = "FROM ";
        foreach ($path as $index => $attribute) {
            $query_from .= $attribute;

            if ($index != count($path) - 1) {
                $query_from .= ", ";
            }
        }

        $graphWithKeys = $this->getDatabaseDirectedGraphWithKeys($database, false);
        $query_where = "WHERE ";

        for ($index = 0; $index < count($path) - 1; $index++) {
            $query_where .= $path[$index] . "." . $graphWithKeys[$path[$index]]["primaryKey"] . " = " . $path[$index + 1] . "." . $graphWithKeys[$path[$index]]["children"][$path[$index + 1]]['primaryKey'];

            if ($index != count($path) - 2) {
                $query_where .= " AND ";
            }
        }

        $pom .= "\n###########\n"
                . "# classes #\n"
                . "###########\n";

        foreach ($path as $index => $attribute) {
            $pom .= sprintf("\n" . "<#TriplesMap%sClass> a rr:TriplesMapClass;" . "\n", ucfirst($attribute));
            $pom .= sprintf("rr:logicalTable [ rr:sqlQuery \"\"\"%s\"\"\" ] ;\n", $query_dummy);
            $pom .= sprintf("rr:subjectMap [ rr:termtype \"IRI\" ; rr:constant \"%s%s/%s\" ];" . "\n", $schemaURI, $database, ucfirst($attribute), $supportGraphURI);
            $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant rdfs:label ] ; rr:objectMap [ rr:constant \"%s\" ] ; ] ;" . "\n", $attribute);
            // $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant rdf:type ] ; rr:objectMap [ rr:constant owl:Class ] ; ] ;" . "\n");

            if ($index > 0) {
                $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant lavbic-owl:relatedTo ] ; rr:objectMap [ rr:constant \"%s%s/%s\" ] ; ] ;" . "\n", $schemaURI, $database, ucfirst($path[$index - 1]));
            }

            if ($index == count($path) - 1) {
                $lastNodeNeighbours = array_diff($lastNodeNeighbours, $path);
                foreach ($lastNodeNeighbours as $ln_table) {
                    $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant lavbic-owl:relatedTo ] ; rr:objectMap [ rr:constant \"%s%s/%s\" ] ; ] ;" . "\n", $schemaURI, $database, ucfirst($ln_table));
                }
            } else if ($index == 0) {
                $firstNodeNeighbours = array_diff($firstNodeNeighbours, $path);
                foreach ($firstNodeNeighbours as $fn_table) {
                    $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant lavbic-owl:relatedTo ] ; rr:objectMap [ rr:constant \"%s%s/%s\" ] ; ] ;" . "\n", $schemaURI, $database, ucfirst($fn_table));
                }
            }

            $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant owl:Class ] ; rr:objectMap [ rr:constant \"%s%s/%s\" ] ; ] ." . "\n", $schemaURI, $database, ucfirst($attribute));
        }

        $pom .= "\n##############\n"
                . "# neighbours #\n"
                . "##############\n";

        $neighbours = array_unique(array_values(array_diff(array_merge($firstNodeNeighbours, $lastNodeNeighbours), $path)));

        // NEIGHBOURS
        foreach ($neighbours as $index => $attribute) {
            $pom .= sprintf("\n" . "<#TriplesMap%sClass> a rr:TriplesMapClass;" . "\n", ucfirst($attribute));
            $pom .= sprintf("rr:logicalTable [ rr:sqlQuery \"\"\"%s\"\"\" ] ;\n", $query_dummy);
            $pom .= sprintf("rr:subjectMap [ rr:termtype \"IRI\" ; rr:constant \"%s%s/%s\" ];" . "\n", $schemaURI, $database, ucfirst($attribute));
            $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant rdfs:label ] ; rr:objectMap [ rr:constant \"%s\" ] ; ] ;" . "\n", strtolower($attribute));
            $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant lavbic-owl:SupportClass ] ; rr:objectMap [ rr:constant \"TRUE\" ] ; ] ;\n");
            $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant rdf:type ] ; rr:objectMap [ rr:constant owl:Class ] ; ] ;" . "\n");
            $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant owl:Class ] ; rr:objectMap [ rr:constant \"%s%s/%s\" ] ; ] ." . "\n", $schemaURI, $database, ucfirst($attribute));
            // $pom .= sprintf("<rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Class\"/>" . "\n");
        }

        $pom .= "\n#############\n"
                . "# instances #\n"
                . "#############\n";

        //user selected attributes
        foreach ($selecedAttributes as $index => $attribute) {

            //detect pks and fks and ignore them
            if ($attribute->pk || $this->isForeignKeyAttribute($database, $attribute->table, $attribute->attribute)) {
                continue;
            }

            $pom .= sprintf("\n" . "<#TriplesMap%s%s> a rr:TriplesMapClass;" . "\n", ucfirst($attribute->table), ucfirst($attribute->attribute));

            $pom .= sprintf("\t" . "rr:logicalTable [ rr:sqlQuery \"\"\"" . "\n");
            $pom .= sprintf("\t" . "%s" . "\n", $query_select);
            $pom .= sprintf("\t" . "%s" . "\n", $query_from);
            $pom .= sprintf("\t" . "%s" . "\n", $query_where);
            $pom .= sprintf("\t" . "\"\"\" ] ;" . "\n");

            $pom .= sprintf("rr:subjectMap [ rr:termtype \"IRI\" ; "
                    . "rr:template \"%s%s/%s/{%s}\" ; rr:class %s:%s ; "
                    . "rr:graph <%s#>];" . "\n", $schemaURI, strtolower($database), ucfirst($attribute->table), $attribute->attribute, ucfirst($database), ucfirst($attribute->table), $supportGraphURI);
            $pom .= sprintf("rr:predicateObjectMap [ rr:predicate rdf:type ; rr:objectMap [ rr:template \"%s%s/%s#{id}\" ; rr:termType rr:IRI]; ] ; \n", $schemaURI, $database, $userClassTripleMapName);
            $pom .= sprintf("rr:predicateObjectMap [ rr:predicate rdf:type ; rr:objectMap [ rr:constant \"%s%s/%s\" ; rr:termType rr:IRI]; ] ; \n", $schemaURI, $database, ucfirst($attribute->table));
            $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant rdfs:label ] ; rr:objectMap [ rr:column \"%s\" ] ; ] ." . "\n", $attribute->attribute);
        }

        $r2rmlContent = "";   // .ttl => Turtle - Terse RDF Triple Language
        //prefix 
        foreach ($documentConventions as $conIndex => $convention) {
            $r2rmlContent .= sprintf("%s\n", $convention);
        }

        $r2rmlContent .= sprintf("@prefix %s: <%s%s/> .\n", ucfirst($database), $schemaURI, $database);

        $r2rmlContent .= "\n###################\n"
                . "# support classes #\n"
                . "###################\n";

        // preslikava - triple map
        $r2rmlContent .= sprintf("\n" . "<#TriplesMap%s> a rr:TriplesMapClass; " . "\n", $userClassTripleMapName);

        // rr:logicalTable
        $r2rmlContent .= sprintf("\t" . "rr:logicalTable [ rr:sqlQuery \"\"\"" . "\n");
        $r2rmlContent .= sprintf("\t" . "%s" . "\n", $query_select);
        $r2rmlContent .= sprintf("\t" . "%s" . "\n", $query_from);
        $r2rmlContent .= sprintf("\t" . "%s" . "\n", $query_where);
        $r2rmlContent .= sprintf("\t" . "\"\"\" ] ;" . "\n");

        $r2rmlContent .= sprintf("rr:subjectMap [ rr:termtype \"IRI\" ; "
                . "rr:constant \"%s%s/%s\" ; "
                . "rr:graph <%s#>];" . "\n", $schemaURI, strtolower($database), $userClassTripleMapName, $supportGraphURI);
        $r2rmlContent .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant owl:Class ] ; "
                . "rr:objectMap [ rr:constant \"%s%s/%s\" ; rr:termType rr:IRI ] ; ] ; \n", $schemaURI, strtolower($database), $userClassTripleMapName);
        $r2rmlContent .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant lavbic-owl:SupportClass ] ; rr:objectMap [ rr:constant \"TRUE\" ] ; ] ;\n");

        $relTables = array_merge($path, $neighbours);

        foreach ($relTables as $relTable) {
            $r2rmlContent .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant lavbic-owl:relatedTo ] ; rr:objectMap [ rr:constant \"%s%s/%s\" ] ; ] ;" . "\n", $schemaURI, $database, ucfirst($relTable));
        }
        $r2rmlContent .= sprintf("rr:predicateObjectMap [ rr:predicate rdfs:label ; rr:objectMap [ rr:constant \"%s\" ] ; ] ." . "\n", $userClassTripleMapName);

        // preslikava - triple map
        $r2rmlContent .= sprintf("\n" . "<#TriplesMap%sInstanceClass> a rr:TriplesMapClass; " . "\n", $userClassTripleMapName);

        // rr:logicalTable
        $r2rmlContent .= sprintf("\t" . "rr:logicalTable [ rr:sqlQuery \"\"\"" . "\n");
        $r2rmlContent .= sprintf("\t" . "%s" . "\n", $query_select);
        $r2rmlContent .= sprintf("\t" . "%s" . "\n", $query_from);
        $r2rmlContent .= sprintf("\t" . "%s" . "\n", $query_where);
        $r2rmlContent .= sprintf("\t" . "\"\"\" ] ;" . "\n");

        $r2rmlContent .= sprintf("rr:subjectMap [ rr:termtype \"IRI\" ; "
                . "rr:template \"%s%s/%s#{id}\" ; "
                . "rr:graph <%s#>];" . "\n", $schemaURI, strtolower($database), $userClassTripleMapName, $supportGraphURI);

        $r2rmlContent .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant owl:Class ] ; "
                . "rr:objectMap [ rr:template \"%s%s/%s#{id}\" ; rr:termType rr:IRI ] ; ] ; \n", $schemaURI, strtolower($database), $userClassTripleMapName);
        $r2rmlContent .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant lavbic-owl:SupportClass ] ; rr:objectMap [ rr:constant \"TRUE\" ] ; ] ;\n");
        $r2rmlContent .= sprintf("rr:predicateObjectMap [ rr:predicate rdfs:subClassOf ; rr:objectMap [ rr:constant \"%s%s/%s\" ; rr:termType rr:IRI]; ] .\n", $schemaURI, strtolower($database), $userClassTripleMapName);

        $r2rmlContent .= $pom;

        $data['r2rml_content'] = $r2rmlContent;

        return json_encode($data);
    }

    public function getR2RMLMappingBaseTables($database, $pathIndex, $path, $implied) {
        $firstNode = $path[0];
        $lastNode = $path[count($path) - 1];
        $firstNodeNeighbours = $this->getNeighbours($firstNode, $database, FALSE);
        $lastNodeNeighbours = $this->getNeighbours($lastNode, $database, FALSE);
        $path = array_values(array_unique(array_merge($path, $firstNodeNeighbours, $lastNodeNeighbours)));

        $defaultURI = "http://www.lavbic.net/";
        $schemaURI = $defaultURI . "/schemas/";     //http://www.lavbic.net/schemas/
        $graphURI = $defaultURI . ucfirst($database);   //http://www.lavbic.net/Sakila

        $xmlFile = DATABASE_SCHEMA_PATH . "/" . $database . ".xml";
        if (file_exists($xmlFile)) {
            $xml = simplexml_load_file($xmlFile);
        }

        $pom = "";
        foreach ($path as $index => $table) {

            $tripleMapName = "TriplesMap" . ucfirst((string) $table);
            $cols = $this->getTableFieldsFromSS($database, $table, true);
            $tablePK = $this->getTablePK($database, $table);

            //TriplesMap
            $pom .= sprintf("<#%s> a rr:TriplesMap; rr:logicalTable [ rr:tableSchema \"%s\" ; rr:tableOwner \"%s\" ; rr:tableName \"%s\" ]; \n", $tripleMapName, $database, $database, $table);

            // rr:subjectMap
            $pom .= sprintf("rr:subjectMap [ rr:termtype \"IRI\" ; rr:template \"%s/%s/{%s}\" ; rr:class %s:%s ; rr:graph <%s#>];" . "\n", $graphURI, strtolower($table), $tablePK, ucfirst($database), ucfirst($table), $graphURI);

            $i = 0;
            foreach ($cols as $key => $col) {

                $pom .= sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant %s:%s ] ; rr:objectMap [ rr:column \"%s\" ]; ]", ucfirst($database), $col['name'], $col['name']);

                if (++$i != count($cols)) {
                    $pom .= sprintf(" ;" . "\n");
                }
            }

            $fkTables = $xml->tables->xpath("table[@name='" . $table . "']/column/parent");
            $pkTables = $xml->tables->xpath("table[@name='" . $table . "']/column/child");

            $joinRows = array();

            foreach ($fkTables as $fkTable) {
                if (!in_array($fkTable["table"], $path)) {
                    continue;
                }

                $fk = $xml->tables->xpath("table[@name='" . (string) $fkTable["table"] . "']/column/child[@foreignKey='" . (string) $fkTable["foreignKey"] . "']");
                $fkColumn = (string) $fk[0]["column"];

                $row = sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant %s:%s_has_%s ] ; rr:objectMap [ rr:termtype \"IRI\" ; rr:template \"%s%s/%s/{%s}\" ]; ]", ucfirst($database), strtolower($table), strtolower($fkTable["table"]), $defaultURI, ucfirst($database), strtolower($fkTable["table"]), $fkColumn);

                $joinRows[] = $row;
            }

            foreach ($pkTables as $pkTable) {

                if (!in_array($pkTable["table"], $path)) {
                    $j++;
                    continue;
                }

                $row = sprintf("rr:predicateObjectMap [ rr:predicateMap [ rr:constant %s:%s ] ; rr:objectMap [ rr:parentTriplesMap <#TriplesMap%s>; rr:joinCondition [ rr:child \"%s\" ; rr:parent \"%s\" ] ; ] ; ]", ucfirst($database), $table . "_of_" . $pkTable["table"], ucfirst($pkTable["table"]), $tablePK, $pkTable["column"]);

                $joinRows[] = $row;
            }

            if (count($joinRows) > 0) {

                $pom .= sprintf(" ;" . "\n");

                $j = 0;
                $numItems = count($joinRows);
                foreach ($joinRows as $row) {
                    if (++$j === $numItems) {
                        $pom .= $row . " .\n\n";
                    } else {
                        $pom .= $row . " ;\n";
                    }
                }
            } else {
                $pom .= ".\n\n";
            }
        }

        $documentConventions = array(
            "@prefix rr: <http://www.w3.org/ns/r2rml#> .",
            "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .",
        );

        $r2rmlContent = "";   // .ttl => Turtle - Terse RDF Triple Language
        foreach ($documentConventions as $conIndex => $convention) {
            $r2rmlContent .= sprintf("%s\n", $convention);
        }
        $r2rmlContent .= sprintf("@prefix %s: <%s%s/> .\n", ucfirst((string) $database), $schemaURI, $database);
        $r2rmlContent .= sprintf("@prefix %s-stat: <%s%s/stat#> .\n", strtolower((string) $database), $defaultURI, $database);
        $r2rmlContent .= sprintf("@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n", strtolower((string) $database), $database);
        $r2rmlContent .= sprintf("@prefix void: <http://rdfs.org/ns/void#> .\n\n", strtolower((string) $database), $database);

        $r2rmlContent .= $pom;

        $data['r2rml_content'] = $r2rmlContent;

        return json_encode($data);
    }

    public function getMappingContent() {
        $selecedAttributes = json_decode($this->input->post('selecedAttributes', TRUE));
        $database = $this->input->post('database', TRUE);
        $pathIndex = $this->input->post('pathIndex', TRUE);
        $path = $this->input->post('graphPath', TRUE);
//        $logTableDefMode = $this->input->post('logTableDefMode', TRUE);
        $logTableDefMode = 1;
        $tripleMapName = $this->input->post('tripleMapName', TRUE);
        $implied = $this->input->post('implied', TRUE);
        $mappingContent;

        switch ($logTableDefMode) {
            case 0:
                $mappingContent = $this->getR2RMLMappingBaseTables($database, $pathIndex, $path, $implied);

                break;
            case 1:
                $mappingContent = $this->getR2RMLMappingSqlQuery($database, $pathIndex, $path, $selecedAttributes, $tripleMapName, $implied);

                break;

            default:
                return 0;
        }
        echo $mappingContent;
    }

    public function getLogMap2MappingResult() {

        $database = $this->input->post('database', TRUE);
        $ttlContent4RDF = $this->input->post('ttlContent4RDF', TRUE);
        $namespace = $this->input->post('defaultNamespace', TRUE);
        $syntax = $this->input->post('fileSyntaxRadioOptions', TRUE);
        $TDBPath = $this->input->post('JenaTDBDirectoryPath', TRUE);
        $selectonto = $this->input->post('selectonto', TRUE);
        $fileGSID = $this->input->post('gsFileID', TRUE);
        $ontoFileID = $this->input->post('ontoFileID', TRUE);
        $ontoFileUser = $this->input->post('ontoFileUser', TRUE);
        $gsFileUser = $this->input->post('gsFileUser', TRUE);
        $dumpType = $this->input->post('RDFDumpTypeButton', TRUE);
        $RDFDumpTypeRadioButton = $this->input->post('RDFDumpTypeRadioButton', TRUE);
        $destinationFileName = $this->input->post('destFileName', TRUE);
        $dumpTDB = $this->input->post('storeTDBButton', TRUE);
        $fileGSbutton = $this->input->post('fileGSbutton', TRUE);
        $fileontobutton = $this->input->post('fileontobutton', TRUE);
        $ctm = time();
        $basePath = $this->getPath(realpath(dirname(BASEPATH)));

        if (strcmp($ontoFileUser, "true") == 0) {
            $selectonto = UPLOAD_PATH . $ontoFileID;
        } elseif (strcmp($selectonto, "dbpedia_small") == 0) {
            $selectonto = "/onto/dbpedia/dbpedia_small_over.rdf";
        }

        $fileGS = "GS_identity_mappings.rdf";
        if (strcmp($gsFileUser, "true") == 0) {
            $fileGS = UPLOAD_PATH . $fileGSID;
        }

        if (!is_dir(RDF_REPO_PATH)) {
            // dir doesn't exist, make it
            mkdir(RDF_REPO_PATH, 0777, true);
        }

        if (!is_dir(R2RML_REPO_PATH)) {
            mkdir(R2RML_REPO_PATH, 0777, true);
        }

        if (!is_dir(LOGMAPFRI_OUTPUT_PATH)) {
            mkdir(LOGMAPFRI_OUTPUT_PATH, 0777, true);
        }

        if (!empty($database)) {
            $r2rmlFilename = $this->getPath($basePath . "/" . R2RML_REPO_PATH . "/" . $database . $ctm . '.ttl');
        } else {
            $r2rmlFilename = $this->getPath($basePath . "/" . R2RML_REPO_PATH . "/" . 'rdfump' . $ctm . '.ttl');
        }

        if (is_writable($r2rmlFilename)) {
            if (!$handle = fopen($r2rmlFilename, 'w')) {
                echo "Cannot open file ($r2rmlFilename)";
                exit;
            }

            if (fwrite($handle, $ttlContent4RDF) === FALSE) {
                echo "Cannot write to file ($r2rmlFilename)";
                exit;
            }
            fclose($handle);
        } else {
            file_put_contents($r2rmlFilename, $ttlContent4RDF);
        }
        
        if ($dumpTDB == FALSE) {
            $file = $this->getRDFWithDB2Triples($database, $namespace, $r2rmlFilename, $syntax, $ctm, $TDBPath);
        } else {
            $file = $this->getRDF2TDBWithDB2Triples($database, $namespace, $r2rmlFilename, $syntax, $ctm, $TDBPath);
        }

        $logMapPRF = $this->runLogMapFRI($file, $selectonto, "out_logmap2//", $fileGS, false);

        $data = array();

        if (count($logMapPRF) >= 5) {
            $data['threshold'] = $logMapPRF[0];
            $data['precision'] = $logMapPRF[1];
            $data['recall'] = $logMapPRF[2];
            $data['fmeasure'] = $logMapPRF[3];
            $data['mappings'] = $logMapPRF[4];
            $data['time'] = $logMapPRF[5];

            $editor_mappings_file = LOGMAPFRI_OUTPUT_PATH . '/logmap2_mappings.txt';
            $incompatible_instance_mappings_file = LOGMAPFRI_OUTPUT_PATH . '/incompatible_instance_mappings.txt';
            $mappings_rdf_file = LOGMAPFRI_OUTPUT_PATH . '/logmap2_mappings.rdf';
            $mappings_owl_file = LOGMAPFRI_OUTPUT_PATH . '/logmap2_mappings.owl';

            $results_files = array("logmap2_mappings" => $editor_mappings_file,
                "incompatible_instance_mappings" => $incompatible_instance_mappings_file,
                "logmap2_mappings_rdf" => $mappings_rdf_file,
                "logmap2_mappings_owl" => $mappings_owl_file);

            foreach ($results_files as $key => $file) {

                $lexUtilsSynonyms = null;

                if (is_file($file)) {
                    $data[$key] = file_get_contents($file, true);
                } else {
                    echo "The file $file does not exist";
                }
            }
            echo json_encode($data);
        } else {
            return;
        }
    }

    public function getRDFFile() {

        $database = $this->input->post('database', TRUE);
        $ttlContent4RDF = $this->input->post('ttlContent4RDF', TRUE);
        $namespace = $this->input->post('defaultNamespace', TRUE);
        $syntax = $this->input->post('fileSyntaxRadioOptions', TRUE);
        $destinationFileName = $this->input->post('destFileName', TRUE);
        $dumpType = $this->input->post('RDFDumpTypeButton', TRUE);
        $RDFDumpTypeRadioButton = $this->input->post('RDFDumpTypeRadioButton', TRUE);
        $TDBPath = $this->input->post('JenaTDBDirectoryPath', TRUE);
        $dumpTDB = $this->input->post('storeTDBButton', TRUE);
        $ctm = time();
        $r2rmlFilename = "";
        $basePath = $this->getPath(realpath(dirname(BASEPATH)));

        if (!is_dir(R2RML_REPO_PATH)) {
            mkdir(R2RML_REPO_PATH, 0777, true);
        }

        if (!empty($database)) {
            $r2rmlFilename = $this->getPath($basePath . "/" . R2RML_REPO_PATH . "/" . $database . $ctm . '.ttl');
        } else {
            $r2rmlFilename = $this->getPath($basePath . "/" . R2RML_REPO_PATH . "/" . 'rdfump' . $ctm . '.ttl');
        }

        if (is_writable($r2rmlFilename)) {
            if (!$handle = fopen($r2rmlFilename, 'w')) {
                echo "Cannot open file ($r2rmlFilename)";
                exit;
            }

            if (fwrite($handle, $ttlContent4RDF) === FALSE) {
                echo "Cannot write to file ($r2rmlFilename)";
                exit;
            }
            fclose($handle);
        } else {
            file_put_contents($r2rmlFilename, $ttlContent4RDF);
        }

        $this->getRDFFileDB2Triples($database, $namespace, $destinationFileName, $syntax, $dumpType, $r2rmlFilename);
    }

    public function getRDFFileDB2Triples($database, $namespace, $destinationFileName, $syntax, $dumpType, $r2rmlFile) {

        if (empty($destinationFileName)) {
            $destinationFileName = $database;
        }

        if (!is_dir(RDF_REPO_PATH)) {
            mkdir(RDF_REPO_PATH, 0777, true);
        }

        $result;
        $user_pass = empty($this->db->password) ? "none" : $this->db->password;
        $file = $this->getPath(sprintf("%s/%s.rdf", RDF_REPO_PATH, $destinationFileName));
        $jar_dependency = "";

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $jar_dependency .= "lib/db2triples/dependency/*;lib/db2triples/db2triples-1.0.3-SNAPSHOT.jar";
        } else {
            $jar_dependency .= "lib/db2triples/db2triples-1.0.3-SNAPSHOT.jar:lib/db2triples/dependency/*";
        }

        $command = sprintf("java -cp %s "
                . "net.antidot.semantic.rdf.rdb2rdf.main.Db2triples "
                . "-m r2rml "
                . "-u %s "
                . "-pass %s "
                . "-b %s "  // database
                . "-i %s "  //http://www.lavbic.net/onto/sakila/
                . "-r %s "  // r2rml file
                . "-o ./%s "
                . "-t %s "
                . "-l jdbc:mysql://%s/ "
                . "-f", $jar_dependency, $this->db->username, $user_pass, $database, $namespace, $r2rmlFile, $file, $syntax, $this->db->hostname);

        exec($command, $result);

        if (!file_exists($file))
            die("File not found");

        // Force the download
        header("Content-Disposition: attachment; filename=" . basename($file) . "");
        header("Content-Length: " . filesize($file));
        header("Content-Type: application/octet-stream;");
        readfile($file);

        return true;
    }

    public function getRDF2TDBWithDB2Triples($database, $namespace, $r2rmlFile, $syntax, $id, $pathTDB) {

        if (!file_exists($r2rmlFile))
            die("File " . $r2rmlFile . "not found");

        $result;
        $user_pass = empty($this->db->password) ? "none" : $this->db->password;
        $jar_dependency = "";

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $jar_dependency .= "lib/db2triples/dependency/*;lib/db2triples/db2triples-1.0.3-SNAPSHOT.jar";
        } else {
            $jar_dependency .= "lib/db2triples/db2triples-1.0.3-SNAPSHOT.jar:lib/db2triples/dependency/*";
        }

        $command = sprintf("java -cp %s "
                . "net.antidot.semantic.rdf.rdb2rdf.main.Db2triples "
                . "-m r2rml "
                . "-u %s "
                . "-p %s "
                . "-b %s "  // database
                . "-i %s "  // http://www.lavbic.net/onto/sakila/
                . "-r %s "  // r2rml file
                . "-o %s//%s%s.rdf "
                . "-t RDFXML "
                . "-l jdbc:mysql://%s/ "
                . "-f", $jar_dependency, $this->db->username, $user_pass, $database, $namespace, $r2rmlFile, $this->getPath(RDF_REPO_PATH), $database, $id, $this->db->hostname);

        exec($command, $result);

        if (count($result) > 0) {
            echo 'ERROR!!!';
            $return['error'] = true;

            return $return;
        } else {
            $file = sprintf("%s%s%s%s.rdf", RDF_REPO_PATH, DS, $database, $id);
            $this->loadRDF2TDB($file, $pathTDB, $database);

            return $file;
        }
    }
    
    public function getRDFWithDB2Triples($database, $namespace, $r2rmlFile, $syntax, $id, $pathTDB) {

        if (!file_exists($r2rmlFile))
            die("File " . $r2rmlFile . "not found");

        $result;
        $user_pass = empty($this->db->password) ? "none" : $this->db->password;
        
        $file = $this->getPath(sprintf("%s/%s%s.rdf", RDF_REPO_PATH, $database, $id));
        
        $jar_dependency = "";

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $jar_dependency .= "lib/db2triples/dependency/*;lib/db2triples/db2triples-1.0.3-SNAPSHOT.jar";
        } else {
            $jar_dependency .= "lib/db2triples/db2triples-1.0.3-SNAPSHOT.jar:lib/db2triples/dependency/*";
        }
       
        $command = sprintf("java -cp %s "
                . "net.antidot.semantic.rdf.rdb2rdf.main.Db2triples "
                . "-m r2rml "
                . "-u %s "
                . "-pass %s "
                . "-b %s "  // database
                . "-i %s "  // http://www.lavbic.net/onto/sakila/
                . "-r %s "  // r2rml file
                . "-o ./%s "
                . "-t RDFXML "
                . "-l jdbc:mysql://%s/ "
                . "-f", $jar_dependency, $this->db->username, $user_pass, $database, $namespace, $r2rmlFile, $file, $this->db->hostname);
              
        exec($command, $result);

        if (count($result) > 0) {
            echo 'ERROR!!!';
            $return['error'] = true;

            return $return;
        } else {
//            $file = sprintf("%s%s%s%s.rdf", RDF_REPO_PATH, DS, $database, $id);
            $file = sprintf("./%s", $file);
//            $this->loadRDF2TDB($file, $pathTDB, $database);

            return $file;
        }
    }

    public function getPath($subject) {
        // return $path = (DIRECTORY_SEPARATOR === '\\') ? str_replace('\\', '/', $subject) : str_replace('/', '\\', $subject);
        return str_replace('\\', '/', $subject);
    }

    public function runLogMapFRI($file, $lodsource, $output, $fileGSbutton, $combineImputOntologies) {

        $basePath = $this->getPath(realpath(dirname(BASEPATH)));
        $result;

        // $onto1 = $this->getPath("file:" . $basePath . "\\sakila_tmp.rdf");
        // $onto2 = $this->getPath("file:" . $basePath . "/onto/dbpedia/dbpedia_small_over.rdf");
        $onto1 = $this->getPath("file:" . $basePath . "/" . $file);
        $onto2 = $this->getPath("file:" . $basePath . "/" . $lodsource);
        $out_path = $this->getPath("/" . $basePath . "/" . LOGMAPFRI_OUTPUT_PATH . "/");
        $gs_file_path = $this->getPath($basePath . "/" . $fileGSbutton);

        $command = sprintf("java -jar lib/logmap_v2.4/dist/logmap_v2.4.jar "
                . "%s %s %s %s FALSE", $onto1, $onto2, $out_path, $gs_file_path);

        exec($command, $result);

        $result = explode(";", array_pop($result));

        return $result;
    }

    public function loadRDF2TDB($pathRDF, $pathTDB, $database) {

        if (empty($tdbDatabase)) {
            $tdbDatabase = $database;
        }

        $result;
        $pathRDF = $this->getPath($pathRDF);
        $pathTDB = $this->getPath($pathTDB . "/" . $database);

        $basePath = realpath(dirname(BASEPATH));

        // $command = sprintf("lib/apache-jena-2.13.0/bat/tdbloader.bat %s -loc %s/%s_test %s", JENA_HOME, $pathTDB, $tdbDatabase, $pathRDF);
        $command = sprintf("java -Xmx1024M "
                . "-Dlog4j.configuration=\"file:%s/lib/apache-jena-2.13.0/jena-log4j.properties\" "
                . "-cp \"%s/lib/apache-jena-2.13.0/lib/*\" tdb.tdbloader "
                . "-loc \"%s\" "
                . "%s", $basePath, $basePath, $pathTDB, $pathRDF);

        exec($command, $result);

        return true;
    }

    public function saveFinalResult2TDB() {
        $this->load->helper('file');

        $ctm = time();
        $owlMappingsData = $this->input->post('owlMappings');
        $database = $this->input->post('database');
        $mappingsSavePath = $this->getPath(LOGMAPFRI_OUTPUT_PATH . "/" . $database . "" . $ctm . ".owl");    //C:\apache-jena-fuseki-2.0.0\run\databases

        if (!write_file($mappingsSavePath, $owlMappingsData)) {
            die('Unable to write the file');
        } else {

            $result;
            $pathTDB = $this->getPath($this->input->post('JenaTDBDirectoryPath') . "/" . $database);
            $basePath = realpath(dirname(BASEPATH));

            // $command = sprintf("lib/apache-jena-2.13.0/bat/tdbloader.bat %s -loc %s/%s_test %s", JENA_HOME, $pathTDB, $tdbDatabase, $pathRDF);
            $command = sprintf("java -Xmx1024M "
                    . "-Dlog4j.configuration=\"file:%s/lib/apache-jena-2.13.0/jena-log4j.properties\" "
                    . "-cp \"%s/lib/apache-jena-2.13.0/lib/*\" tdb.tdbloader "
                    . "-loc \"%s\" "
                    . "%s", $basePath, $basePath, $pathTDB, $mappingsSavePath);

            exec($command, $result);
        }
        echo true;
    }

    public function getTTLFile() {
        $this->load->helper('download');

        $data = $this->input->post('TTLSource', TRUE);
        $database = $this->input->post('database', TRUE);

        force_download("$database.ttl", $data);
    }

    function getTableChildren($dbName, $tableName) {

        $tablePK = $this->getTablePK($dbName, $tableName);
        $xmlFile = DATABASE_SCHEMA_PATH . "/$dbName.xml";

        if (file_exists($xmlFile)) {

            $xml = simplexml_load_file($xmlFile);

            $result = $xml->xpath("//table[@name='" . $tableName . "']/column[@name='" . $tablePK . "']/child");

            if (empty($result)) {
                return array(); // nima starsev
            }

            $tableChildren = array();
            foreach ($result as $index => $child) {
                $tableChildren[(string) $child['table']] = (string) $child['column'];
            }

            return $tableChildren;
        } else {
            exit("NAPAKA: $xmlFile.");
        }
    }

    function getTableParents($dbName, $tableName) {

        $tablePK = $this->getTablePK($dbName, $tableName);
        $xmlFile = DATABASE_SCHEMA_PATH . "/$dbName.xml";

        if (file_exists($xmlFile)) {

            $xml = simplexml_load_file($xmlFile);

            $result = $xml->xpath("/database/tables/table[@name='" . $tableName . "']//parent");

            if (empty($result)) {
                return array(); // nima starsev
            }

            $tableParents = array();
            foreach ($result as $index => $parent) {
                $tableParents[(string) $parent['table']] = (string) $parent['column'];
            }

            return $tableParents;
        } else {
            exit("NAPAKA: $xmlFile.");
        }
    }

    public function getNeighbours($tableName, $database, $implied) {

        $xmlFile = DATABASE_SCHEMA_PATH . "/" . $database . ".xml";
        $result = array();

        if (file_exists($xmlFile)) {

            $xml = simplexml_load_file($xmlFile);

            $fkTables = $xml->tables->xpath("table[@name='" . $tableName . "']/column/parent");

            foreach ($fkTables as $table) {
                if (strcmp((string) $table["implied"], $implied)) {
                    $result[] = ((string) $table["table"]);
                }
            }

            // check many-many relations: check pk tables for fks and if any table of this kind exists add it to result array
            $pkTables = $xml->tables->xpath("table[@name='" . $tableName . "']/column/child");

            foreach ($pkTables as $pkTable) {
                $currentTable = ((string) $pkTable["table"]);
                $fkTables = $xml->tables->xpath("table[@name='" . $currentTable . "']/column/parent");

                if (!strcmp($pkTable["implied"], $implied)) {
                    continue;
                } elseif (!in_array($currentTable, $result)) {
                    $result[] = $currentTable;
                }

                foreach ($fkTables as $table) {
                    $tmpTableImpl = (string) $table["implied"];
                    $tmpTableName = (string) $table["table"];

                    if (!strcmp($tmpTableImpl, $implied)) {
                        continue;
                    }

                    if (strcmp($tmpTableName, $tableName)) {
                        $result[] = ((string) $table["table"]);
                    }
                }
            }

            $result = array_values(array_unique($result));

            return $result;
        } else {
            exit('The file ' . $xmlFile . ' does not exist');
        }
    }

    public function getDatabaseDirectedGraph($database, $implied, $inverse) {

        if ($inverse) {
            $databaseGraph = $this->getTableDataFromXMLInverseGraph($database, $implied);
        } else {
            $databaseGraph = $this->getTableDataFromXML($database, $implied);
        }

        $graph = array();

        foreach ($databaseGraph as $value) {

            $children = array();
            foreach ($value['children'] as $child) {
                array_push($children, $child['table']);
            }

            $uChildren = array_unique($children);

            $graph[$value['name']] = $uChildren;
        }

        return $graph;
    }

    public function getDatabaseDirectedGraphWithKeys($database, $implied) {

        $databaseGraph = $this->getTableDataFromXML($database, $implied);
        $graph = array();

        foreach ($databaseGraph as $value) {

            $children = array();
            foreach ($value['children'] as $child) {
//                array_push($children, array("table" => $child['table'], "primaryKey" => $child['primaryKey']));
                $children["children"][$child['table']] = array(
                    "table" => $child['table'],
                    "primaryKey" => $child['primaryKey'],
                    "foreignKey" => $child['foreignKey']);
            }

//            $uChildren = array_unique($children);

            $children['primaryKey'] = $value['primaryKey'];
            $graph[$value['name']] = $children;
        }

        return $graph;
    }

    public function getTableDataFromXML($database, $implied) {

        $xmlFile = DATABASE_SCHEMA_PATH . "/" . $database . ".xml";

        if (file_exists($xmlFile)) {

            $xml = simplexml_load_file($xmlFile);
            $tables = $xml->tables->table;
            $result = array();

            foreach ($tables as $table) {

                if (strcmp($table['type'], "TABLE")) {
                    continue;
                }

                $tablePK = $table->xpath("index[@name='PRIMARY']/column");

                $tableFK = $table->xpath("index[contains(@name,'idx_fk_')]");

                if (!empty($tablePK)) {
                    $tablePK = (string) $tablePK[0]['name'];
                    $children = $table->xpath("column[@name='" . $tablePK . "']/child");
                } else {
                    continue;
                }

                $childrenTable = array();
                $prev_index = 0;

                foreach ($xml->tables->table as $index => $tbl) {

                    $tmpppp = $tbl->xpath("index[contains(@name,'idx_fk_')]/column[@name='$tablePK']");

                    if (!empty($tmpppp)) {
                        $childrenTable[$prev_index++] = array(
                            'primaryKey' => $tablePK,
                            'foreignKey' => $tablePK,
                            'implied' => "false",
                            'table' => (string) $tbl['name'],
                        );
                    }
                }

                foreach ($children as $index => $val) {
                    $childrenTable[$index + $prev_index] = array(
                        'primaryKey' => (string) $val['column'],
                        'foreignKey' => (string) $tablePK,
                        'implied' => (string) $val['implied'],
                        'table' => (string) $val['table'],
                    );
                }

                $result[] = array(
                    'name' => (string) $table['name'],
                    'type' => (string) $table['type'],
                    'numRows' => (string) $table['numRows'],
                    'primaryKey' => (string) $tablePK,
                    'children' => $childrenTable
                );
            }

            return $result;
        } else {
            exit('The file ' . $xmlFile . ' does not exist');
        }
    }

    public function getTableDataFromXMLInverseGraph($database, $implied) {

        $xmlFile = DATABASE_SCHEMA_PATH . "/" . $database . ".xml";

        if (file_exists($xmlFile)) {

            $xml = simplexml_load_file($xmlFile);
            $tables = $xml->tables->table;
            $result = array();

            foreach ($tables as $table) {

                if (strcmp($table['type'], "TABLE")) {
                    continue;
                }

                $tablePK = $table->xpath("index[@name='PRIMARY']/column");

                if (!empty($tablePK)) {
                    $tablePK = (string) $tablePK[0]['name'];
//                    $children = $table->xpath("column[@name='" . $tablePK . "']/child");
                    $children = $table->xpath("column/parent");
                }

                $childrenTable = array();

                foreach ($children as $index => $val) {

                    $childrenTable[$index] = array(
                        'primaryKey' => (string) $val['column'],
                        'foreignKey' => (string) $tablePK,
                        'implied' => (string) $val['implied'],
                        'table' => (string) $val['table'],
                    );
                }

                $result[] = array(
                    'name' => (string) $table['name'],
                    'type' => (string) $table['type'],
                    'numRows' => (string) $table['numRows'],
                    'primaryKey' => (string) $tablePK,
                    'children' => $childrenTable
                );
            }

            return $result;
        } else {
            exit('The file ' . $xmlFile . ' does not exist');
        }
    }

    public function isForeignKeyAttribute($database, $table, $attribute) {
        $xmlFile = DATABASE_SCHEMA_PATH . "/" . $database . ".xml";
        if (file_exists($xmlFile)) {
            $xml = simplexml_load_file($xmlFile);
        }
        $fkAttribute = $xml->tables->xpath("table[@name='" . $table . "']/column/parent[@column='" . $attribute . "']");

        return !empty($fkAttribute);
    }

}

class Graph {

    protected $graph;
    protected $visited = array();

    public function __construct($graph) {
        $this->graph = $graph;
    }

    public function breadthFirstSearchPaths($start, $goal) {
        $valid_paths = array();     // mnoĹľica najdenih poti
        $queue = array();       //mnoĹľica najdenih vozliĹˇÄŤ
        array_push($queue, array($start => array($start)));

        while (count($queue) > 0) {     // dokler ne pregledamo vseh vozliĹˇÄŤ
            $temp_vertex = array_pop($queue);   // odstrani vozliĹˇÄŤe v iz zaÄŤetka vrste Q
            $vertex = implode(array_keys($temp_vertex));
            $path = $temp_vertex[$vertex];

            if (!empty($this->graph[$vertex])) {    // ÄŤe vozliĹˇÄŤa nima povezav
                $tmp_graph = array_diff($this->graph[$vertex], $path);

                foreach ($tmp_graph as $next) {

                    if ($next == $goal) {
                        array_push($valid_paths, array_merge($path, array($next)));
                    } else {
                        array_push($queue, array($next => array_merge($path, array($next))));
                    }
                }
            }
        }
        return $valid_paths;
    }

}
