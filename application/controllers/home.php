<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $data = array(
            'menuContent' => $this->load->view('include/cover_menu', array(), TRUE),
            'mainContent' => $this->load->view('cover_about_view', array(), TRUE),
            'footerContent' => $this->load->view('include/cover_footer', array(), TRUE),
        );
        $this->load->view('templates/cover_template', $data);
    }

}
