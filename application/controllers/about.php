<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class About extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('url', 'date', 'file');
        $this->load->dbutil();
        $this->load->database();
    }

    public function index() {

//        $data = array(
//            'menuContent' => $this->load->view('include/cover_menu', array(), TRUE),
//            'mainContent' => $this->load->view('cover_contact_view', array(), TRUE),
//            'footerContent' => $this->load->view('include/cover_footer', array(), TRUE),
//        );
//        $this->load->view('templates/cover_template', $data);


        $dbs = $this->dbutil->list_databases();

        $data = array(
            'headerContent' => $this->load->view('include/main_header', array(), TRUE),
            'mainContent' => $this->load->view('about_view', array('dbs' => $dbs), TRUE),
            'footerContent' => $this->load->view('include/main_footer', array(), TRUE),
        );
        $this->load->view('templates/main_template', $data);
    }

}
