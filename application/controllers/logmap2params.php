<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class LogMap2params extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('url', 'date', 'file');
        $this->load->dbutil();
        $this->load->database();
    }

    public function index() {
//        $dbs = $this->dbutil->list_databases();
        $this->load->helper('file');

        $parameters_file = 'parameters.txt';
        $parameters = null;

        if (is_file($parameters_file)) {
            $parameters = file_get_contents($parameters_file, true);
        } else {
            echo "The file $parameters_file does not exist";
        }



        $data = array();
        foreach (explode("\n", $parameters) as $value) {
            if (strlen($value) > 0) {
                $prop_line = explode("|", $value);
                $data[$prop_line[0]] = $prop_line[1];
            }
        }

        $data = array(
            'headerContent' => $this->load->view('include/main_header', array(), TRUE),
            'mainContent' => $this->load->view('settings_logmap2_param_view', array('parameters' => $data), TRUE),
            'footerContent' => $this->load->view('include/main_footer', array(), TRUE),
        );

        $this->load->view('templates/main_template', $data);
    }

    public function saveProps() {
        $this->load->helper('file');
        if ($this->input->post('sbm') == "save_button") {

            $prop_vars = array();
            $prop_content = "";

            if (!isset($_POST['reverse_labels'])) {
                $prop_content .= "reverse_labels|false\n";
            } else {
                $prop_content .= "reverse_labels|true\n";
            }
            if (!isset($_POST['use_overlapping'])) {
                $prop_content .= "use_overlapping|false\n";
            } else {
                $prop_content .= "use_overlapping|true\n";
            }

            if (!isset($_POST['second_chance_conflicts'])) {
                $prop_content .= "second_chance_conflicts|false\n";
            } else {
                $prop_content .= "second_chance_conflicts|true\n";
            }
            if (!isset($_POST['instance_matching'])) {
                $prop_content .= "instance_matching|false\n";
            } else {
                $prop_content .= "instance_matching|true\n";
            }
            if (!isset($_POST['use_umls_4_individuals'])) {
                $prop_content .= "use_umls_4_individuals|false\n";
            } else {
                $prop_content .= "use_umls_4_individuals|true\n";
            }
            if (!isset($_POST['use_umls_4_context_individuals'])) {
                $prop_content .= "use_umls_4_context_individuals|false\n";
            } else {
                $prop_content .= "use_umls_4_context_individuals|true\n";
            }
            if (!isset($_POST['use_umls_lexicon'])) {
                $prop_content .= "use_umls_lexicon|false\n";
            } else {
                $prop_content .= "use_umls_lexicon|true\n";
            }
            if (!isset($_POST['use_lammagen'])) {
                $prop_content .= "use_lammagen|false\n";
            } else {
                $prop_content .= "use_lammagen|true\n";
            }
            if (!isset($_POST['combine_individuals'])) {
                $prop_content .= "combine_individuals|false\n";
            } else {
                $prop_content .= "combine_individuals|true\n";
            }
            if (!isset($_POST['use_neighbourhood_similarity'])) {
                $prop_content .= "use_neighbourhood_similarity|false\n";
            } else {
                $prop_content .= "use_neighbourhood_similarity|true\n";
            }
            if (!isset($_POST['extract_scope_mapping'])) {
                $prop_content .= "extract_scope_mapping|false\n";
            } else {
                $prop_content .= "extract_scope_mapping|true\n";
            }
            if (!isset($_POST['output_class_mappings'])) {
                $prop_content .= "output_class_mappings|false\n";
            } else {
                $prop_content .= "output_class_mappings|true\n";
            }
            if (!isset($_POST['output_prop_mappings'])) {
                $prop_content .= "output_prop_mappings|false\n";
            } else {
                $prop_content .= "output_prop_mappings|true\n";
            }
            if (!isset($_POST['output_instance_mappings'])) {
                $prop_content .= "output_instance_mappings|false\n";
            } else {
                $prop_content .= "output_instance_mappings|true\n";
            }
            if (!isset($_POST['output_instance_mapping_files'])) {
                $prop_content .= "output_instance_mapping_files|false\n";
            } else {
                $prop_content .= "output_instance_mapping_files|true\n";
            }

            if (!isset($_POST['reason_datatypes'])) {
                $prop_content .= "reason_datatypes|false\n";
            } else {
                $prop_content .= "reason_datatypes|true\n";
            }
            if (!isset($_POST['allow_interactivity'])) {
                $prop_content .= "allow_interactivity|false\n";
            } else {
                $prop_content .= "allow_interactivity|true\n";
            }
            if (!isset($_POST['output_equivalences_only'])) {
                $prop_content .= "output_equivalences_only|false\n";
            } else {
                $prop_content .= "output_equivalences_only|true\n";
            }

            $prop_vars['ratio_second_chance_discarded'] = $this->input->post('ratio_second_chance_discarded', TRUE);
            $prop_vars['good_instance_confidence_map'] = $this->input->post('good_instance_confidence_map', TRUE);
            $prop_vars['sub_levels'] = $this->input->post('sub_levels', TRUE);
            $prop_vars['super_levels'] = $this->input->post('super_levels', TRUE);
            $prop_vars['max_size_subclasses'] = $this->input->post('max_size_subclasses', TRUE);
            $prop_vars['good_confidence'] = $this->input->post('good_confidence', TRUE);
            $prop_vars['good_sim_coocurrence'] = $this->input->post('good_sim_coocurrence', TRUE);
            $prop_vars['min_conf_pro_map'] = $this->input->post('min_conf_pro_map', TRUE);
            $prop_vars['bad_score_scope'] = $this->input->post('bad_score_scope', TRUE);
            $prop_vars['syn_items_size'] = $this->input->post('syn_items_size', TRUE);
            $prop_vars['syn_items_size_for_allowed_addition'] = $this->input->post('syn_items_size_for_allowed_addition', TRUE);
            $prop_vars['syn_items_max_addition_size'] = $this->input->post('syn_items_max_addition_size', TRUE);
            $prop_vars['onto_languages'] = $this->input->post('onto_languages', TRUE);
            $prop_vars['reasoner'] = $this->input->post('reasoner', TRUE);
            $prop_vars['timeout'] = $this->input->post('timeout', TRUE);
            $prop_vars['good_isub_anchors'] = $this->input->post('good_isub_anchors', TRUE);
            $prop_vars['good_isub_candidates'] = $this->input->post('good_isub_candidates', TRUE);
            $prop_vars['max_ambiguity'] = $this->input->post('max_ambiguity', TRUE);
            $prop_vars['good_ambiguity'] = $this->input->post('good_ambiguity', TRUE);
            $prop_vars['min_size_overlapping'] = $this->input->post('min_size_overlapping', TRUE);


            foreach ($prop_vars as $key => $value) {
                $prop_content .= sprintf("%s|%s\n", $key, $value);
            }

            $prop_content .= "debug|false\n" .
                    "print_output|true\n" .
                    "annotation_URI|http://www.w3.org/2000/01/rdf-schema#label\n" .
                    "object_property_relation_URI|http://www.lavbic.net/onto/sakila/relatedTo\n" .
                    "object_property_support_URI|http://www.lavbic.net/onto/sakila/SupportClass\n" .
                    "object_property_support_URI|http://www.lavbic.net/onto/sakila/SupportClass\n";



            if (!write_file('parameters.txt', $prop_content)) {
                echo 'Unable to write the file';
            } else {
                echo 'File written!';
                redirect('mapping/index');
            }
        } else {

            $parameters_file = 'parameters_default.txt';
            $parameters = null;

            if (is_file($parameters_file)) {
                $parameters = file_get_contents($parameters_file, true);
            } else {
                echo "The file $parameters_file does not exist";
            }


            if (!write_file('parameters.txt', $parameters)) {
                echo 'Unable to write the file';
            } else {
                echo 'File written!';
//                redirect('logmap2params/index');
            }
        }

        exit();
    }

    //put your code here
}
