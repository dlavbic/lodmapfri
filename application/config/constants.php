<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


/*
  |--------------------------------------------------------------------------
  | DBAL constants
  |--------------------------------------------------------------------------
  |
 */


define('DS', DIRECTORY_SEPARATOR);

define('DATABASE_SCHEMA_PATH', 'dump');
define('JENA_HOME', 'C:/apache-jena-2.13.0');
define('R2RML_REPO_PATH', 'out/r2rml');
define('RDF_REPO_PATH', 'out/rdf');
define('LOGMAPFRI_OUTPUT_PATH', 'out/logmapfri');

define('GRAPHVIZ_PATH', '');
define('UPLOAD_PATH', 'upload//');
define('TDB_PATH', 'C:/apache-jena-fuseki-2.0.0/run/databases');

/* Lexical utils */
define('LEX_NORM', 'Lex_norm_LRNOM');
define('LEX_SYN', 'wordnet_syn_file.txt');
define('LEX_STOPWORDS', 'stopwords.txt');
define('LEX_PLURALS', 'Lex_plurals_LRAGR');
define('LEX_SPELLING', 'Lex_spelling_LRSPL');