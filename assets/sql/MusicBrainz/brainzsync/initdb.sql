CREATE DATABASE `music_brainz` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE `music_brainz`.`annotation` (
  `id` int(10) unsigned NOT NULL,
  `editor` int(10) NOT NULL,
  `text` TEXT,
  `changelog` VARCHAR(255),
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist` (
  `id` int(10) unsigned NOT NULL,
  `gid` char(36) NOT NULL,
  `name` int(10) NOT NULL,
  `sort_name` int(10) NOT NULL,
  `begin_date_year` smallint,
  `begin_date_month` smallint,
  `begin_date_day` smallint,
  `end_date_year` smallint,
  `end_date_month` smallint,
  `end_date_day` smallint,
  `type` int(10),
  `country` int(10),
  `gender` int(10),
  `comment` VARCHAR(255),
  `ipi_code` VARCHAR(11),
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_alias` (
  `id` int(10) unsigned NOT NULL,
  `artist` int(10) NOT NULL,
  `name` int(10) NOT NULL,
  `locale` TEXT,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_annotation` (
  `artist` int(10) NOT NULL,
  `annotation` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_meta` (
  `id` int(10) NOT NULL,
  `rating` SMALLINT,
  `rating_count` int(10)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_tag` (
  `artist` int(10) NOT NULL,
  `tag` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_rating_raw` (
  `artist` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `rating` SMALLINT NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_tag_raw` (
  `artist` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `tag` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_gid_redirect` (
  `gid` char(36) NOT NULL,
  `new_id` int(10) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_name` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`artist_type` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`cdtoc` (
  `id` int(10) unsigned NOT NULL,
  `discid` CHAR(28) NOT NULL,
  `freedb_id` CHAR(8) NOT NULL,
  `track_count` int(10) NOT NULL,
  `leadout_offset` int(10) NOT NULL,
  `track_offset` text NOT NULL,
  `degraded` BOOLEAN NOT NULL DEFAULT FALSE,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`cdtoc_raw` (
  `id` int(10) unsigned NOT NULL,
  `release` int(10) NOT NULL,
  `discid` CHAR(28) NOT NULL,
  `track_count` int(10) NOT NULL,
  `leadout_offset` int(10) NOT NULL,
  `track_offset` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`clientversion` (
  `id` int(10) unsigned NOT NULL,
  `version` VARCHAR(64) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`country` (
  `id` int(10) unsigned NOT NULL,
  `iso_code` VARCHAR(2) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`gender` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`isrc` (
  `id` int(10) unsigned NOT NULL,
  `recording` int(10) NOT NULL,
  `isrc` CHAR(12) NOT NULL,
  `source` smallint,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_artist_artist` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_artist_label` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_artist_recording` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_artist_release` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_artist_release_group` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_artist_url` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_artist_work` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_label_label` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_label_recording` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_label_release` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_label_release_group` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_label_url` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_label_work` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_recording_recording` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_recording_release` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_recording_release_group` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_recording_url` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_recording_work` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_release_release` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_release_release_group` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_release_url` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_release_work` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_release_group_release_group` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_release_group_url` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_release_group_work` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_url_url` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_url_work` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`l_work_work` (
  `id` int(10) unsigned NOT NULL,
  `link` int(10) NOT NULL,
  `entity0` int(10) NOT NULL,
  `entity1` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label` (
  `id` int(10) unsigned NOT NULL,
  `gid` char(36) NOT NULL,
  `name` int(10) NOT NULL,
  `sort_name` int(10) NOT NULL,
  `begin_date_year` smallint,
  `begin_date_month` smallint,
  `begin_date_day` smallint,
  `end_date_year` smallint,
  `end_date_month` smallint,
  `end_date_day` smallint,
  `label_code` INTEGER,
  `type` int(10),
  `country` int(10),
  `comment` VARCHAR(255),
  `ipi_code` VARCHAR(11),
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_rating_raw` (
  `label` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `rating` SMALLINT NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_tag_raw` (
  `label` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `tag` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_alias` (
  `id` int(10) unsigned NOT NULL,
  `label` int(10) NOT NULL,
  `name` int(10) NOT NULL,
  `locale` TEXT,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_annotation` (
  `label` int(10) NOT NULL,
  `annotation` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_meta` (
  `id` int(10) NOT NULL,
  `rating` SMALLINT,
  `rating_count` int(10)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_gid_redirect` (
  `gid` char(36) NOT NULL,
  `new_id` int(10) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_name` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_tag` (
  `label` int(10) NOT NULL,
  `tag` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`label_type` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`language` (
  `id` int(10) unsigned NOT NULL,
  `iso_code_3t` CHAR(3) NOT NULL,
  `iso_code_3b` CHAR(3) NOT NULL,
  `iso_code_2` CHAR(2),
  `name` VARCHAR(100) NOT NULL,
  `frequency` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`link` (
  `id` int(10) unsigned NOT NULL,
  `link_type` int(10) NOT NULL,
  `begin_date_year` smallint,
  `begin_date_month` smallint,
  `begin_date_day` smallint,
  `end_date_year` smallint,
  `end_date_month` smallint,
  `end_date_day` smallint,
  `attribute_count` int(10) NOT NULL DEFAULT 0,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`link_attribute` (
  `link` int(10) NOT NULL,
  `attribute_type` int(10) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`link_attribute_type` (
  `id` int(10) unsigned NOT NULL,
  `parent` int(10),
  `root` int(10) NOT NULL,
  `child_order` int(10) NOT NULL DEFAULT 0,
  `gid` char(36) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`link_type` (
  `id` int(10) unsigned NOT NULL,
  `parent` int(10),
  `child_order` int(10) NOT NULL DEFAULT 0,
  `gid` char(36) NOT NULL,
  `entity_type0` VARCHAR(50),
  `entity_type1` VARCHAR(50),
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT,
  `link_phrase` VARCHAR(255) NOT NULL,
  `reverse_link_phrase` VARCHAR(255) NOT NULL,
  `short_link_phrase` VARCHAR(255) NOT NULL,
  `priority` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`link_type_attribute_type` (
  `link_type` int(10) NOT NULL,
  `attribute_type` int(10) NOT NULL,
  `min` smallint,
  `max` smallint,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`medium` (
  `id` int(10) unsigned NOT NULL,
  `tracklist` int(10) NOT NULL,
  `release` int(10) NOT NULL,
  `position` int(10) NOT NULL,
  `format` int(10),
  `name` VARCHAR(255),
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`medium_cdtoc` (
  `id` int(10) unsigned NOT NULL,
  `medium` int(10) NOT NULL,
  `cdtoc` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`medium_format` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `parent` int(10),
  `child_order` int(10) NOT NULL DEFAULT 0,
  `year` smallint,
  `has_discids` BOOLEAN NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`puid` (
  `id` int(10) unsigned NOT NULL,
  `puid` CHAR(36) NOT NULL,
  `version` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`replication_control` (
  `id` int(10) unsigned NOT NULL,
  `current_schema_sequence` int(10) NOT NULL,
  `current_replication_sequence` int(10),
  `last_replication_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`recording` (
  `id` int(10) unsigned NOT NULL,
  `gid` char(36) NOT NULL,
  `name` int(10) NOT NULL,
  `artist_credit` int(10) NOT NULL,
  `length` INTEGER,
  `comment` VARCHAR(255),
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`recording_rating_raw` (
  `recording` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `rating` SMALLINT NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`recording_tag_raw` (
  `recording` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `tag` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`recording_annotation` (
  `recording` int(10) NOT NULL,
  `annotation` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`recording_meta` (
  `id` int(10) NOT NULL,
  `rating` SMALLINT,
  `rating_count` int(10)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`recording_gid_redirect` (
  `gid` char(36) NOT NULL,
  `new_id` int(10) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`recording_puid` (
  `id` int(10) unsigned NOT NULL,
  `puid` int(10) NOT NULL,
  `recording` int(10) NOT NULL,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`recording_tag` (
  `recording` int(10) NOT NULL,
  `tag` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release` (
  `id` int(10) unsigned NOT NULL,
  `gid` char(36) NOT NULL,
  `name` int(10) NOT NULL,
  `artist_credit` int(10) NOT NULL,
  `release_group` int(10) NOT NULL,
  `status` int(10),
  `packaging` int(10),
  `country` int(10),
  `language` int(10),
  `script` int(10),
  `date_year` smallint,
  `date_month` smallint,
  `date_day` smallint,
  `barcode` VARCHAR(255),
  `comment` VARCHAR(255),
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `quality` SMALLINT NOT NULL DEFAULT -1,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_raw` (
  `id` int(10) unsigned NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `artist` VARCHAR(255),
  `added` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `last_modified` TIMESTAMP,
  `lookup_count` INTEGER DEFAULT 0,
  `modify_count` INTEGER DEFAULT 0,
  `source` INTEGER DEFAULT 0,
  `barcode` VARCHAR(255),
  `comment` VARCHAR(255),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_tag_raw` (
  `release` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `tag` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_annotation` (
  `release` int(10) NOT NULL,
  `annotation` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_gid_redirect` (
  `gid` char(36) NOT NULL,
  `new_id` int(10) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_meta` (
  `id` int(10) NOT NULL,
  `date_added` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `info_url` VARCHAR(255),
  `amazon_asin` VARCHAR(10),
  `amazon_store` VARCHAR(20)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_coverart` (
  `id` int(10) NOT NULL,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `cover_art_url` VARCHAR(255)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_label` (
  `id` int(10) unsigned NOT NULL,
  `release` int(10) NOT NULL,
  `label` int(10),
  `catalog_number` VARCHAR(255),
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_packaging` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_status` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_tag` (
  `release` int(10) NOT NULL,
  `tag` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_group` (
  `id` int(10) unsigned NOT NULL,
  `gid` char(36) NOT NULL,
  `name` int(10) NOT NULL,
  `artist_credit` int(10) NOT NULL,
  `type` int(10),
  `comment` VARCHAR(255),
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_group_rating_raw` (
  `release_group` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `rating` SMALLINT NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_group_tag_raw` (
  `release_group` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `tag` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_group_annotation` (
  `release_group` int(10) NOT NULL,
  `annotation` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_group_gid_redirect` (
  `gid` char(36) NOT NULL,
  `new_id` int(10) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_group_meta` (
  `id` int(10) NOT NULL,
  `release_count` int(10) NOT NULL DEFAULT 0,
  `first_release_date_year` smallint,
  `first_release_date_month` smallint,
  `first_release_date_day` smallint,
  `rating` SMALLINT,
  `rating_count` int(10)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_group_tag` (
  `release_group` int(10) NOT NULL,
  `tag` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_group_type` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`release_name` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`script` (
  `id` int(10) unsigned NOT NULL,
  `iso_code` CHAR(4) NOT NULL,
  `iso_number` CHAR(3) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `frequency` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`script_language` (
  `id` int(10) unsigned NOT NULL,
  `script` int(10) NOT NULL,
  `language` int(10) NOT NULL,
  `frequency` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`statistic` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `value` int(10) NOT NULL,
  `date_collected` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`tag` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `ref_count` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`tag_relation` (
  `tag1` int(10) NOT NULL,
  `tag2` int(10) NOT NULL,
  `weight` int(10) NOT NULL,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`track` (
  `id` int(10) unsigned NOT NULL,
  `recording` int(10) NOT NULL,
  `tracklist` int(10) NOT NULL,
  `position` int(10) NOT NULL,
  `name` int(10) NOT NULL,
  `artist_credit` int(10) NOT NULL,
  `length` INTEGER,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`track_raw` (
  `id` int(10) unsigned NOT NULL,
  `release` int(10) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `artist` VARCHAR(255),
  `sequence` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`track_name` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`tracklist` (
  `id` int(10) unsigned NOT NULL,
  `track_count` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`tracklist_index` (
  `tracklist` int(10),
  `toc` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`url` (
  `id` int(10) unsigned NOT NULL,
  `gid` char(36) NOT NULL,
  `url` TEXT NOT NULL,
  `description` TEXT,
  `ref_count` int(10) NOT NULL DEFAULT 0,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`url_gid_redirect` (
  `gid` char(36) NOT NULL,
  `new_id` int(10) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`vote` (
  `id` int(10) unsigned NOT NULL,
  `editor` int(10) NOT NULL,
  `edit` int(10) NOT NULL,
  `vote` SMALLINT NOT NULL,
  `vote_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `superseded` BOOLEAN NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work` (
  `id` int(10) unsigned NOT NULL,
  `gid` char(36) NOT NULL,
  `name` int(10) NOT NULL,
  `artist_credit` int(10),
  `type` int(10),
  `iswc` CHAR(15),
  `comment` VARCHAR(255),
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_rating_raw` (
  `work` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `rating` SMALLINT NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_tag_raw` (
  `work` int(10) NOT NULL,
  `editor` int(10) NOT NULL,
  `tag` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_alias` (
  `id` int(10) unsigned NOT NULL,
  `work` int(10) NOT NULL,
  `name` int(10) NOT NULL,
  `locale` TEXT,
  `edits_pending` int(10) NOT NULL DEFAULT 0,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_annotation` (
  `work` int(10) NOT NULL,
  `annotation` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_gid_redirect` (
  `gid` char(36) NOT NULL,
  `new_id` int(10) NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_meta` (
  `id` int(10) NOT NULL,
  `rating` SMALLINT,
  `rating_count` int(10)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_name` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_tag` (
  `work` int(10) NOT NULL,
  `tag` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `last_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
CREATE TABLE `music_brainz`.`work_type` (
  `id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;