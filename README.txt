== Setup instructions == 

LodMapFri can be installed in three steps:

1. Upload the Source folders and files to your server. Normally the index.php file will be at your root.  \\

2. Open the application/config/config.php file with a text editor and set your base URL. If you intend to use encryption or sessions, set your encryption key. \\

$config['base_url'] = 'http://localhost/FRI_MAG_RDFizing/';

3. Open the application/config/database.php file with a text editor and set your database settings.  \\

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'mydbusername';
$db['default']['password'] = 'mypassword123';
$db['default']['database'] = ''; // �e �elimo dati uporabniku na voljo vse podatkovne vire pustimo prazno
$db['default']['dbdriver'] = 'mysql';  
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;
}}}

That's it!

== About LodMapFRI ==

LodMapFRI is a tool which is defined by a process which enables integration of data from a relational database (schema and instances) with existing structured data in RDF format - so called Linked Data (LOD) sources. LodMapFRI is actually a complement and enhancement of ontology matching tool - LogMap. It allows user to work with relational databases, which implicitly contain useful metadata. These metadata can be used to identify the context of the data in the database and thus relieve the user from integrating schemas manually. Being able to do just that and extension of syntactic approaches are in fact key improvement of LogMap and main contribution of our solution.

LodMapFRI is a PHP application which is developed in open source PHP framework called Codeigniter and many other web technologies like Javascript (jQuery), AJAX, CSS. The application guides the user through the entire process of enriching data from relational databases with linked data sources. The process consists of six steps. Within each step, the application calls different tools such as SchemaSpy, DB2Triples and LogMapFRI, which are responsible for each step. 

In the following steps will be presented the process of data enrichment of relational database with LOD. In the first step, the user selects the database connection. For the database connection is necessary to enter authentication information. In the lower part form, the user chooses a database, which wants to include in the enrichment process. User can also choose between two database schema display mode:
* ER diagram,
* as a directed graph.

In the next step, the scheme of the database appears as a directed graph, from which user select the path of tables or nodes that are in next steps considered as data to be enriched.

In the third step, on the left side of the window are displayed all the tables and their representative fields. By using the method of "drag and drop" or by clicking on the button "+", which is located within the field of each table, the user selects the fields that will be part of a new table or. class which will be involved in the process of data enrichment. 

In the fourth step, the selected class from the previous step is transformed into R2RML mapping document. Relationships between classes are marked with the object property "lavbic-owl:relatedTo". To facilitate the detection of context in the process of ontology matching, in addition to all the selected tables from the selected path, R2RML mapping document also includes neighbors (tables) of first and last node of selected path. Context nodes are labeled with the object property "lavbic-owl:supportClass".

For mapping data from relational databases into RDF format, we are using R2RML processor DB2Triples, whose parameters are located on the upper part of the web form shown on the picture below. With DB2Triples, we can export triples directly to a file, in which RDF triples can be written in different formats (N-triples, RDF/XML, OWL/XML, Turtle, or N3). DB2Triples also allows direct mapping of triples in the Jena TDB store. The second part of the form consists of parameters relevant to ontology matching process. 

Before the beginning of matching process, users can edit various dictionaries and properties which affect affect the performance of matching. All configuration forms are available in the web application toolbar.

After the enrichment process (ontology matching process) results are available in various formats: RDF, OWL, and TXT. In addition to the matches which were marked as correct, the user can also review matchings which were marked as incorrect. 

By confirming results of the ontology matching process, triplets can be recorded in the triplestore database named Fuseki.


