/*******************************************************************************
 * Copyright 2012 by the Department of Computer Science (University of Oxford)
 * 
 *    This file is part of LogMap.
 * 
 *    LogMap is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 * 
 *    LogMap is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 * 
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with LogMap.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package uk.ac.ox.krr.logmap2.reasoning.deprecated;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLFunctionalSyntaxOntologyFormat;
import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import org.semanticweb.owlapi.model.*;
import uk.ac.manchester.syntactic_locality.ModuleExtractor;

import java.util.*;
import java.util.concurrent.*;



/**
 * This class will analyze the new consequences over O1 and O2
 * obtained from the integration: O1 union O2 union M
 * @author Ernesto Jimenez-Ruiz<br>
 * Temporal Knowledge Bases Group (Jaume I University of Castellon) <br>
 * Collaboration with Oxford University (Information Systems Group) <br>
 * Supervisors: Bernardo Cuenca-Grau, Rafael Berlanga, Ian Horrocks
 * Created: 03/03/2010 <br>
 * Modified: - <br>
 */
public class ImpactAuditedIntegration {

	
	
	private String rootPath;
	
	private static int FMA2NCI=0;
	private static int FMA2SNOMED=1;
	private static int SNOMED2NCI=2;
	private static int MOUSE2HUMAN=5;
	
	/** Uri OWL merged ontology*/
	private String mergedOntologyUri;
	
	private OWLDataFactory factory;
	private OWLOntologyManager managerOnto1;
	private OWLOntologyManager managerOnto2;
	private OWLOntologyManager managerMappings;
	private OWLOntologyManager managerMerged;
	
	private OWLOntology moduleonto1;
	private OWLOntology moduleonto2;
	private OWLOntology mergedOntology;
	private OWLOntology mappingsOntology;

	private HermiTReasonerAccess reasonerOnto1;
	private HermiTReasonerAccess reasonerOnto2;
	//private HermiTReasonerAccess reasonerMerged;
	
	//Files
	private String mappingsOntologyFile;
	private String moduleonto1file;
	private String moduleonto2file;
	private String mergedOntologyfile;
	
	private String ontolabel1;
	private String ontolabel2;

	
	boolean umls_mapping_assessment;
	
	
	long init, fin;
	
	
	public ImpactAuditedIntegration(){
		
	}
	
	/**
	 * 
	 */
	public ImpactAuditedIntegration(String path, int integration2assess, boolean umls_assessment){
		
		rootPath=path;
		
		umls_mapping_assessment=umls_assessment;
		
		try{
			
			if (integration2assess==FMA2NCI)
				setFMA_NCI_Files();
			else if (integration2assess==FMA2SNOMED)
				setFMA_SNOMED_Files();
			else if (integration2assess==SNOMED2NCI)
				setSNOMED_NCI_Files();
			else
				setAnatomy_Files();
			
			
			init=Calendar.getInstance().getTimeInMillis();
			loadOntologies();
			fin = Calendar.getInstance().getTimeInMillis();
			System.out.println("Time loading ontologies (s): " + (float)((double)fin-(double)init)/1000.0);
			
			init=Calendar.getInstance().getTimeInMillis();
			//setUpReasoners();
			
			fin = Calendar.getInstance().getTimeInMillis();
			System.out.println("Time setting up reasoners for ontologies (s): " + (float)((double)fin-(double)init)/1000.0);
			
			init=Calendar.getInstance().getTimeInMillis();
			createCurrentMergedOntologyAndReason();
			fin = Calendar.getInstance().getTimeInMillis();
			System.out.println("Time creating merged ontology and reasoning (s): " + (float)((double)fin-(double)init)/1000.0);
			
			
			/*
			
			init=Calendar.getInstance().getTimeInMillis();
			extractInferencesL0();
			fin = Calendar.getInstance().getTimeInMillis();
			System.out.println("Time extractib inferences language 0 (s): " + (float)((double)fin-(double)init)/1000.0);
			
			
			System.out.println("Integration between " + ontolabel1 + "-" + ontolabel2);
			
			ImpactDifferences = new OntologyInferredDifferences();
			
			ImpactDifferences.getNewInferenceList(reasonerMerged, reasonerOnto1, 1);
			System.out.println("\tImpact over " + ontolabel1 + ": " + ImpactDifferences.getNumberDifferences());
			
			ImpactDifferences.getNewInferenceList(reasonerMerged, reasonerOnto2, 2);
			System.out.println("\tImpact over " + ontolabel2 + ": " + ImpactDifferences.getNumberDifferences());
			*/
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
	private void setFMA_NCI_Files(){

		ontolabel1="FMA";
		ontolabel2="NCI";
		
		
		//Modules
		//moduleonto1file="file:" + rootPath + currentpath + "FMA_module_mappings_nci.owl";
		//moduleonto2file="file:" + rootPath + currentpath + "NCI_module_mappings_fma.owl";
		
		
		//Whole onto
		//moduleonto1file="file:" + rootPath + currentpath + "FMADL_cleant_2_0.owl";
		//moduleonto2file="file:" + rootPath + currentpath + "NCI_Thesaurus_08.05d_cleant.owl";
		
		
		//mappingsOntologyFile = "file:" + rootPath + "Results/resultsFMA2NCI/OWL_mappings_FMA_NCI_new.owl";
		
		String currentpath="UMLS_Onto_Versions/FMA2NCI/";
		
		if (umls_mapping_assessment){
			moduleonto1file = "file:" + rootPath + currentpath + "FMA_overlappingUMLScleant_nci.owl";
			//moduleonto1file = "file:" + rootPath + "UMLS_Onto_Versions/FMADL_cleant_2_0.owl";
			moduleonto2file = "file:" + rootPath + currentpath + "NCI_overlappingUMLScleant_fma.owl";
			//moduleonto2file = "file:" + rootPath + "UMLS_Onto_Versions/NCI_Thesaurus_08.05d_nolabels.owl";
			mappingsOntologyFile = "file:" + rootPath + currentpath + "FMA2NCI_correspondences_UMLScleant.owl";
			//mappingsOntologyFile = "file:" + rootPath +"UMLS_Onto_Versions/OWL_mappings_FMA_NCI_new_owl11.owl";
			
			mergedOntologyUri="http://krono.act.uji.es/mappings/FMA_NCI_integrationUMLScleant.owl";
			
			mergedOntologyfile="file:" + rootPath + currentpath + "FMA_NCI_integrationUMLScleant.owl";
			
		}
		else{
			moduleonto1file = "file:" + rootPath + "UMLS_Onto_Versions/FMADL_cleant_2_0.owl";
			//moduleonto1file = "file:" + rootPath + currentpath + "FMA_overlapping_nci.owl";
			moduleonto2file = "file:" + rootPath + "UMLS_Onto_Versions/NCI_Thesaurus_08.05d_nolabels.owl";
			//moduleonto2file = "file:" + rootPath + currentpath + "NCI_overlapping_fma.owl";
			mappingsOntologyFile = "file:" + rootPath + currentpath + "FMA2NCI_correspondences.owl";
			mergedOntologyUri="http://krono.act.uji.es/mappings/FMA_NCI_integration.owl";
			
			mergedOntologyfile="file:" + rootPath + currentpath + "FMA_NCI_integration.owl";
		}
		
		//Full Ontos
		/*moduleonto1uri="file:" + rootPath + currentpath + "FMADL_cleant_2_0.owl";
		moduleonto2uri="file:" + rootPath + currentpath + "NCI_Thesaurus_08.05d_cleant.owl";
		*/
		
		
				
	}



	private void setFMA_SNOMED_Files(){

		ontolabel1="FMA";
		ontolabel2="SNOMED";
		
		// Modules
		//moduleonto1file = "file:" + rootPath + currentpath + "FMA_module_mappings_snmd.owl";
		//moduleonto2file = "file:" + rootPath + currentpath + "SNOMED_module_mappings_fma.owl";
	
		//Whole ontos
		//moduleonto1file = "file:" + rootPath + currentpath + "FMADL_cleant_2_0.owl";
		//moduleonto2file = "file:" + rootPath + currentpath + "snomed20090131_cleant.owl";
		
		
		//mappingsOntologyFile = "file:" + rootPath + "Results/resultsFMA2SNOMED/OWL_mappings_FMA_SNOMED_new.owl";
		
		String currentpath="UMLS_Onto_Versions/FMA2SNOMED/";
		
		if (umls_mapping_assessment){
			moduleonto1file = "file:" + rootPath + currentpath + "FMA_overlappingUMLScleant_snmd.owl";
			//moduleonto1file = "file:" + rootPath + "UMLS_Onto_Versions/FMADL_cleant_2_0.owl";
			moduleonto2file = "file:" + rootPath + currentpath + "SNMD_overlappingUMLScleant_fma.owl";
			//moduleonto2file = "file:" + rootPath + "UMLS_Onto_Versions/snomed20090131_replab.owl";
			mappingsOntologyFile = "file:" + rootPath + currentpath + "FMA2SNMD_correspondences_UMLScleant.owl";
			
			mergedOntologyUri="http://krono.act.uji.es/mappings/FMA_SNMD_integrationUMLScleant.owl";
			
			mergedOntologyfile="file:" + rootPath + currentpath + "FMA_SNMD_integrationUMLScleant.owl";
		}
		else{
			moduleonto1file = "file:" + rootPath + currentpath + "FMA_overlapping_snmd.owl";
			moduleonto2file = "file:" + rootPath + currentpath + "SNMD_overlapping_fma.owl";
			mappingsOntologyFile = "file:" + rootPath + currentpath + "FMA2SNMD_correspondences.owl";
			
			mergedOntologyUri="http://krono.act.uji.es/mappings/FMA_SNMD_integration.owl";
			
			mergedOntologyfile="file:" + rootPath + currentpath + "FMA_SNMD_integration.owl";
		}
		
		
		
	
		
		
		//Full Ontos
		//moduleonto1uri="file:" + rootPath + currentpath + "FMADL_cleant_2_0.owl";
		//moduleonto2uri="file:" + rootPath + currentpath + "snomed20090131_cleant.owl";
		
		
		
		
		
		
		
	}


	private void setSNOMED_NCI_Files(){

		ontolabel1="SNOMED";
		ontolabel2="NCI";
		
		
		// Modules
		//moduleonto1file = "file:" + rootPath + currentpath + "SNOMED_module_mappings_nci.owl";
		//moduleonto2file = "file:" + rootPath + currentpath + "NCI_module_mappings_snmd.owl";
		
		//moduleonto1file = "file:" + rootPath + currentpath + "SNOMED_module_mappings_nci_revised.owl";
		//moduleonto2file = "file:" + rootPath + currentpath + "NCI_module_mappings_snmd_revised.owl";
		
		String currentpath="UMLS_Onto_Versions/SNOMED2NCI/";
		
		if (umls_mapping_assessment){
			moduleonto1file = "file:" + rootPath + currentpath + "SNMD_overlappingUMLScleant_nci.owl";
			moduleonto2file = "file:" + rootPath + currentpath + "NCI_overlappingUMLScleant_snmd.owl";
			mappingsOntologyFile = "file:" + rootPath + currentpath + "SNMD2NCI_correspondences_UMLScleant.owl";
			
			mergedOntologyUri="http://krono.act.uji.es/mappings/SNMD_NCI_integrationUMLScleant.owl";
			mergedOntologyfile="file:" + rootPath + currentpath + "SNMD_NCI_integrationUMLScleant.owl";
		}
		else{
			moduleonto1file = "file:" + rootPath + currentpath + "SNMD_overlapping_nci.owl";						
			moduleonto2file = "file:" + rootPath + currentpath + "NCI_overlapping_snmd.owl";						
			mappingsOntologyFile = "file:" + rootPath + currentpath + "SNMD2NCI_correspondences.owl";	
			
			mergedOntologyUri="http://krono.act.uji.es/mappings/SNMD_NCI_integration.owl";
			mergedOntologyfile="file:" + rootPath + currentpath + "SNMD_NCI_integration.owl";
		}
		
		
		
		//mappingsOntologyFile = "file:" + rootPath + "Results/resultsSNOMED2NCI/OWL_mappings_SNOMED_NCI_new.owl";
		//mappingsOntologyFile = "file:" + rootPath + "Results/resultsSNOMED2NCI/OWL_mappings_SNOMED_NCI_new_owl11.owl";
		
		//Full ontos
		//moduleonto1uri = "file:" + rootPath + currentpath + "snomed20090131_cleant.owl";
		//moduleonto2uri = "file:" + rootPath + currentpath + "NCI_Thesaurus_08.05d_cleant.owl";
		
			
			
		
		
		
				
	}

	
	private void setAnatomy_Files(){

		ontolabel1="MO";
		ontolabel2="NCIAnat";
		
		
		String currentpath="UMLS_Onto_Versions/Anatomy/";
	
		moduleonto1file = "file:" + rootPath + currentpath + "MOUSE_overlapping_ncian.owl";						
		moduleonto2file = "file:" + rootPath + currentpath + "NCIAn_overlapping_mouse.owl";						
		mappingsOntologyFile = "file:" + rootPath + currentpath + "MOUSE2NCIAn_correspondences.owl";	
			
		mergedOntologyUri="http://krono.act.uji.es/mappings/Mouse_NCIAnat_integration.owl";
		mergedOntologyfile="file:" + rootPath + currentpath + "Mouse_NCIAnat_integration.owl";
	
				
	}


	private void loadOntologies() throws Exception {		

		managerOnto1 = OWLManager.createOWLOntologyManager();
		managerOnto2 = OWLManager.createOWLOntologyManager();
		managerMappings = OWLManager.createOWLOntologyManager();
		
		factory = managerOnto1.getOWLDataFactory();
	
		moduleonto1 = managerOnto1.loadOntology(IRI.create(moduleonto1file));
		
		moduleonto2 = managerOnto2.loadOntology(IRI.create(moduleonto2file));
		
		mappingsOntology = managerMappings.loadOntology(IRI.create(mappingsOntologyFile));
		//signatureOnto1 = moduleonto1.getReferencedEntities();
		//signatureOnto1.add(factory.getOWLNothing());
		 
		//signatureOnto2 = moduleonto2.getReferencedEntities();
		//signatureOnto2.add(factory.getOWLNothing());
	 
	}


	
	//Initialises Reasoners and classifies ontologies
	private void setUpReasoners() throws Exception{

		//Set<OWLClass> unsatisfiableClasses;
		
		
		reasonerOnto1 = new HermiTReasonerAccess(managerOnto1, moduleonto1);
		reasonerOnto1.craeateInferredEntailmentsLanguage0();
		//unsatisfiableClasses = reasonerOnto1.getUnsatisfiableClasses();
        //System.out.println("\nThere are '" + unsatisfiableClasses.size() + "' unsatisfiable classes in Ontology 1.");
		
        
		reasonerOnto2 = new HermiTReasonerAccess(managerOnto2, moduleonto2);
		
		//unsatisfiableClasses = reasonerOnto2.getUnsatisfiableClasses();
        //System.out.println("\nThere are '" + unsatisfiableClasses.size() + "' unsatisfiable classes in Ontology 2.");

	}
	
	private void extractInferencesL0(){
		reasonerOnto1.craeateInferredEntailmentsLanguage0();
		reasonerOnto2.craeateInferredEntailmentsLanguage0();
		reasonerMergedHermit.craeateInferredEntailmentsLanguage0();
	}
	

	
	/**
	 * Creates current merged ontology with current mappings 
	 * in order to analyze the impact
	 * @throws Exception
	 */
	private void createCurrentMergedOntologyAndReason() throws Exception {
		
		
		managerMerged = OWLManager.createOWLOntologyManager();
		mergedOntology = managerMerged.createOntology(IRI.create(mergedOntologyUri));
		
		
		List<OWLOntologyChange> listChanges = new ArrayList<OWLOntologyChange>();
		
		OWLEquivalentClassesAxiom eq_axiom;
		
		OWLClass ent1;
		OWLClass ent2;

		
	
		//Ontology 1 Axioms
		for (OWLAxiom ax : moduleonto1.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}

		// Ontology 2 Axioms
		for (OWLAxiom ax : moduleonto2.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}

		
		for (OWLAxiom ax : mappingsOntology.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}
		
		
		
		managerMerged.applyChanges(listChanges);
		
		
		System.out.println("Storing merged ontology: ");
		managerMerged.saveOntology(mergedOntology, new OWLFunctionalSyntaxOntologyFormat(), IRI.create(mergedOntologyfile)); //RDFXMLOntologyFormat
		
		//extractModuleUnsat();
		
		//Classify Ontology and getting errors
		reasonerMergedHermit = new HermiTReasonerAccess(managerMerged, mergedOntology);		
        Set<OWLClass> unsatisfiableClasses = reasonerMergedHermit.getUnsatisfiableClasses();
        System.out.println("There are '" + unsatisfiableClasses.size() + "' unsatisfiable classes.");
        
        
	}
	
	
	
	private final int HERMIT = 0;
	private final int PELLET = 1;
	private int reasoner;
	
	private int resultClassEval;	
	private HermiTReasonerAccess reasonerMergedHermit;
	private PelletAccess reasonerMergedPellet;
	
	private final int SAT = 0;
	private final int UNSAT = 1;
	private final int UNKNOWN = 2;
	private OWLClass current_cls;
	
	
	
	
	//maintains a thread for executing the doWork method
    private ExecutorService executor = Executors.newFixedThreadPool(1);

    public void doWork() {
        //perform some long running task here...
    	
    	if (reasoner==HERMIT){
    		if (reasonerMergedHermit.isSatisfiable(current_cls))
    			resultClassEval=SAT;
    		else
    			resultClassEval=UNSAT;
    	}
    	else{
    		if (reasonerMergedPellet.isSatisfiable(current_cls))
    			resultClassEval=SAT;
    		else
    			resultClassEval=UNSAT;
    	}
    }

    
    
    public void doWorkWithTimeout(int timeoutSecs) {

        //set the executor thread working
        final Future<?> future = executor.submit(new Runnable() {
            public void run() {
                try {
                    doWork();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });

        //check the outcome of the executor thread and limit the time allowed for it to complete
        try {
            future.get(timeoutSecs, TimeUnit.MILLISECONDS);
        }
        catch (TimeoutException e) {
        	
        	//ExecutionException: deliverer threw exception
            //TimeoutException: didn't complete within downloadTimeoutSecs
            //InterruptedException: the executor thread was interrupted

        	resultClassEval = UNKNOWN;
        	
            //interrupts the worker thread if necessary
            future.cancel(true);

            
        
    	}
        catch (Exception e) {
        	e.printStackTrace();
    	
        }        
    }
	
	
	
	
	
	public void reasonWithGivenOntologiesHermit(OWLOntology O1, OWLOntology O2, OWLOntology M) throws Exception{
			
		OWLOntologyManager managerMerged = OWLManager.createOWLOntologyManager();
		OWLOntology mergedOntology = managerMerged.createOntology();
			
		reasoner=HERMIT;
			
		List<OWLOntologyChange> listChanges = new ArrayList<OWLOntologyChange>();
			
		
		System.out.println("Axioms ontologies 2 merge: ");
		
		//Ontology 1 Axioms
		for (OWLAxiom ax : O1.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}
		System.out.println("\tOntology 1 classes: " + O1.getClassesInSignature().size());

		// Ontology 2 Axioms
		for (OWLAxiom ax : O2.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}

		System.out.println("\tOntology 2 classes: " + O2.getClassesInSignature().size());
			
		for (OWLAxiom ax : M.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}
		System.out.println("\tMapping axioms: " + M.getAxioms().size());
			
			
			
		managerMerged.applyChanges(listChanges);
			
			
		//Classify Ontology and getting errors
		reasonerMergedHermit = new HermiTReasonerAccess(managerMerged, mergedOntology, false);		
	    //reasonerMerged.getUnsatisfiableClasses();
	    //System.out.println("There are '" + unsatisfiableClasses.size() + "' unsatisfiable classes.");
	    
	    
	    
		System.out.println("Getting unsatistiable classes HERMIT...");
		int unsat = 0;
		int sat = 0;
		int unknown = 0;
		int clsnum = 0;	
		for (OWLClass cls : reasonerMergedHermit.getBaseOntology().getClassesInSignature()){
			
			current_cls = cls;
			
			doWorkWithTimeout(500);
			
			if (resultClassEval==UNSAT){
				unsat++;
				//System.err.println("Class '" + class_name + "' is UNSAT");
			}
			else if (resultClassEval==SAT){
				sat++;
				//System.out.println("Class '" + class_name + "' is SAT");
			}
			else{
				unknown++;
				//System.err.println("No response for class '" + class_name + "'");
			}
			
			clsnum++;
			
			
		}
		System.out.println("There are '" + unknown + "' UNKNOWN classes.");
		System.out.println("There are '" + sat + "' SAT classes.");
		System.out.println("There are '" + unsat + "' UNSAT classes.");
		System.out.println("There are '" + clsnum + "' classes.");
		
        //Set<OWLClass> unsatisfiableClasses = reasonerMerged.getUnsatisfiableClasses();
        //System.out.println("There are '" + unsatisfiableClasses.size() + "' unsatisfiable classes.");
	    
	    
	     
		
	}
	
	
	public void reasonWithGivenOntologiesPellet(OWLOntology O1, OWLOntology O2, OWLOntology M) throws Exception{
		
		OWLOntologyManager managerMerged = OWLManager.createOWLOntologyManager();
		OWLOntology mergedOntology = managerMerged.createOntology();
			
		reasoner=PELLET;
			
		List<OWLOntologyChange> listChanges = new ArrayList<OWLOntologyChange>();
			
				
		//Ontology 1 Axioms
		for (OWLAxiom ax : O1.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}
		System.out.println("Ontology 1 classes: " + O1.getClassesInSignature().size());

		// Ontology 2 Axioms
		for (OWLAxiom ax : O2.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}

		System.out.println("Ontology 2 classes: " + O2.getClassesInSignature().size());
			
		for (OWLAxiom ax : M.getAxioms()) {
			listChanges.add(new AddAxiom(mergedOntology, ax));
		}
		System.out.println("Mappings axioms: " + M.getAxioms().size());
			
			
			
		managerMerged.applyChanges(listChanges);
			
			
		//Classify Ontology and getting errors
		reasonerMergedPellet = new PelletAccess(managerMerged, mergedOntology, false);		
	    //reasonerMerged.getUnsatisfiableClasses();
	    //System.out.println("There are '" + unsatisfiableClasses.size() + "' unsatisfiable classes.");
		
		System.out.println("Getting unsatistiable classes PELLET...");
		int unsat = 0;
		int sat = 0;
		int unknown = 0;
		int clsnum = 0;	
	
		for (OWLClass cls : reasonerMergedPellet.getBaseOntology().getClassesInSignature()){
			
			current_cls = cls;
			
			doWorkWithTimeout(500);
			
			if (resultClassEval==UNSAT){
				unsat++;
				//System.err.println("Class '" + class_name + "' is UNSAT");
			}
			else if (resultClassEval==SAT){
				sat++;
				//System.out.println("Class '" + class_name + "' is SAT");
			}
			else{
				unknown++;
				//System.err.println("No response for class '" + class_name + "'");
			}
			
			clsnum++;
			
			
		}
		System.out.println("There are '" + unknown + "' UNKNOWN classes.");
		System.out.println("There are '" + sat + "' SAT classes.");
		System.out.println("There are '" + unsat + "' UNSAT classes.");
		System.out.println("There are '" + clsnum + "' classes.");
	     
		
	}
	
	
	
	
	private void extractModuleUnsat() throws Exception{
		
		//module for hard cases#
		//are classes in conflocit og below class extracted??
			//IRI entityIRI = IRI.create("http://www.ihtsdo.org/snomed#Arteriectomy_with_anastomosis");
			IRI entityIRI = IRI.create("http://bioontology.org/projects/ontologies/fma/fmaOwlDlComponent_2_0#Adrenocorticotropin");
		
		
			
			ModuleExtractor moduleExtractor = new ModuleExtractor(mergedOntology, false, false, false, true, false);
	
			Set<OWLEntity> signature=new HashSet<OWLEntity>();
			signature.add(factory.getOWLClass(entityIRI));
			
			entityIRI = IRI.create("http://bioontology.org/projects/ontologies/fma/fmaOwlDlComponent_2_0#Smegma");
			signature.add(factory.getOWLClass(entityIRI));
			
			//Set<OWLEntity> moduleEnt = moduleExtractor.extractModuleEntitiesForGroupSignature(signature);
			OWLOntology module = moduleExtractor.getLocalityModuleForSignatureGroup(signature, mergedOntologyUri);
			
			//Store module
			if (umls_mapping_assessment)
				OWLManager.createOWLOntologyManager().saveOntology(module, new RDFXMLOntologyFormat(), IRI.create(mergedOntologyfile + "_module_unsatUMLS.owl"));
			else
				OWLManager.createOWLOntologyManager().saveOntology(module, new RDFXMLOntologyFormat(), IRI.create(mergedOntologyfile + "_module_unsat.owl"));
			
			
			
	}
	
	
	
	public static void main(String[] args) {
	
		if (args.length==3)
			new ImpactAuditedIntegration(args[0], Integer.valueOf(args[1]), Boolean.valueOf(args[2]));
		else{
			//new ImpactAuditedIntegration("/C:/Users/ernesto/EclipseWS/DataUMLS/", 2);
			
			//new ImpactAuditedIntegration("/usr/local/data/DataUMLS/", 0, true);
			//new ImpactAuditedIntegration("/usr/local/data/DataUMLS/", 1, true);
			//new ImpactAuditedIntegration("/usr/local/data/DataUMLS/", 2, true);
			
			
			new ImpactAuditedIntegration("/usr/local/data/DataUMLS/", 0, false);
			//new ImpactAuditedIntegration("/usr/local/data/DataUMLS/", 1, false);
			//new ImpactAuditedIntegration("/usr/local/data/DataUMLS/", 2, false);
			//new ImpactAuditedIntegration("/usr/local/data/DataUMLS/", MOUSE2HUMAN, false);
			
		}
			
		
	}
	
	
	
}
