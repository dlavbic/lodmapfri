/*******************************************************************************
 * Copyright 2012 by the Department of Computer Science (University of Oxford)
 * 
 *    This file is part of LogMap.
 * 
 *    LogMap is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 * 
 *    LogMap is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 * 
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with LogMap.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package uk.ac.ox.krr.logmap2.reasoning.deprecated;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.*;
import org.semanticweb.owlapi.util.*;
import uk.ac.manchester.syntactic_locality.ModuleExtractorManager;
import uk.ac.ox.krr.logmap2.io.LogOutput;

import java.util.*;
import java.util.concurrent.*;

//Using OWL APi factory
//import org.semanticweb.owlapi.reasoner.impl.OWLReasonerBase;
//HermiT_adapted




/**
 * This class will serve us as intermediate class to access HermiT Reasoner (version 1.3).
 * 
 * @author root
 *
 */
public class HermiTReasonerAccess {
	
	private OWLOntology ontoBase;
	private OWLOntologyManager ontoManager;
	
	private IRI iri;
	
	private Set<OWLAxiom> inferredAxioms=new HashSet<OWLAxiom>();
	
	public static final String DEFAULT_REASONER_ID = "NoOpReasoner";
	
	private TreeSet<OWLAxiom> axiomsInferredOnt;
	
	private OWLDataFactory datafactory;
	
	//private Reasoner hermit;
	private HermiT_adapted hermit;
	private OWLReasoner hermitWithFactory;

	
	//For L1 extraction
	private ModuleExtractorManager moduleExtractorManager;
	private OWLOntology currentModule;
	private HermiTReasonerAccess currentModuleReasoner;
	private boolean L1disj=false;
	private boolean L1some=true; //defaults
	private boolean L1all=false;
	
	
	private boolean isClassified = false;
	
	private long init, fin;

	
	public HermiTReasonerAccess(OWLOntology onto, boolean classify) throws Exception {
		this(OWLManager.createOWLOntologyManager(), onto, classify);
	}
	
	
	
	/**
	 * 
	 * @param ontoManage
	 * @param onto
	 * @throws Exception
	 */
	public HermiTReasonerAccess(OWLOntologyManager ontoManager, OWLOntology onto) throws Exception {
		
		ontoBase=onto;
		
		iri=onto.getOntologyID().getOntologyIRI();
		
		this.ontoManager=ontoManager;
		
		datafactory=ontoManager.getOWLDataFactory();
		
		//This will avoid obtaining already known disjoint
		//removeExplicitDisjointness();
				
		LogOutput.print("Set up reasoner... ");
		setUpReasoner();
	
		classifyOntology();
		
		craeateInferredDisjointAxioms();
		
		getUnsatisfiableClasses();
		
	}
	
	
	public HermiTReasonerAccess(OWLOntologyManager ontoManager, OWLOntology onto, boolean classify) throws Exception {
		
		ontoBase=onto;
	
		
		
		iri=onto.getOntologyID().getOntologyIRI();
		
		
		this.ontoManager=ontoManager;
		
		datafactory=ontoManager.getOWLDataFactory();
		
		LogOutput.print("Set up reasoner... ");
		setUpReasoner();
		
		if (classify){
			classifyOntology();
			//getUnsatisfiableClasses();
		}
		
		
		
	}
	
	
	
	private void setUpReasonerWithFactory(){
		
		ConsoleProgressMonitor progressMonitor = new ConsoleProgressMonitor();
		OWLReasonerConfiguration config = new SimpleConfiguration(progressMonitor); 
		 
		OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory(); 
		hermitWithFactory = reasonerFactory.createReasoner(ontoBase, config); 
		//OWLReasoner reasoner = reasonerFactory.createReasoner(ontoBase); 
		
		Set<OWLOntology> importsClosure = ontoManager.getImportsClosure(ontoBase);
      
        DLExpressivityChecker checker = new DLExpressivityChecker(importsClosure);
        System.err.println("Expressivity Ontology: " + checker.getDescriptionLogicName());
        
	}
	
	
	private void setUpReasoner() throws Exception{
	
		hermit=new HermiT_adapted(ontoBase);
		
		Set<OWLOntology> importsClosure;		
        try {
		importsClosure = ontoManager.getImportsClosure(ontoBase);
        //hermit.loadOntologies(importsClosure);
		//hermit.loadOntology(ontoManager, ontoBase, null);
        }
        catch (Exception e){
        	//System.err.println("Error getting importsClosure");
        	importsClosure = new HashSet<OWLOntology>();
        	importsClosure.add(ontoBase);
        	
        }
	
        DLExpressivityChecker checker = new DLExpressivityChecker(importsClosure);
        LogOutput.print("Expressivity Ontology: " + checker.getDescriptionLogicName());
        
	}
	
	
    
    
	public boolean isOntologyClassified(){
		return isClassified;
	}
	
	

    
    public void ClassOntology_withTimeout(int timeoutSecs) {

    	ExecutorService executor = Executors.newFixedThreadPool(1);
    	
        //set the executor thread working
        final Future<?> future = executor.submit(new Runnable() {
            public void run() {
                try {
                	classifyOntologyNoProperties(); //If timeout never class properties
                	//classifyOntology();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });

        //check the outcome of the executor thread and limit the time allowed for it to complete
        try {
            future.get(timeoutSecs, TimeUnit.SECONDS);
            future.cancel(true);
            executor.shutdown();
        }
        catch (TimeoutException e) {
        	
        	//ExecutionException: deliverer threw exception
            //TimeoutException: didn't complete within downloadTimeoutSecs
            //InterruptedException: the executor thread was interrupted

        	LogOutput.print("Time out classifying with HermiT. Using 'structural' reasoner instead.");
        	
        	isClassified = false;
        	
        	hermit.interrupt();
        	hermit.dispose();
        	
            //interrupts the worker thread if necessary
            future.cancel(true);
            executor.shutdown();

            
        
    	}
        catch (Exception e) {
        	LogOutput.print("Error classifying with HermiT");
    	
        }        
    }
	
    
    public void classifyOntologyNoProperties(){
    	classifyOntology(false);
    }
	
    public void classifyOntology(){
    	classifyOntology(true);
    }
	
	public void classifyOntology(boolean classproperties){
		isClassified = false;
				
		init=Calendar.getInstance().getTimeInMillis();
		LogOutput.print("Classifying Ontology with HermiT... ");
        hermit.precomputeInferences(InferenceType.CLASS_HIERARCHY);
        if (classproperties){
        	hermit.precomputeInferences(InferenceType.DATA_PROPERTY_HIERARCHY);
        	hermit.precomputeInferences(InferenceType.OBJECT_PROPERTY_HIERARCHY);
        }
        hermit.precomputeInferences(InferenceType.DISJOINT_CLASSES);//Only explicit, we have extended method!
        
        //hermit.realise();
		fin = Calendar.getInstance().getTimeInMillis();
		LogOutput.print("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
		isClassified = true;
	}
	

	public void classifyOntologyWithFactory(){
		init=Calendar.getInstance().getTimeInMillis();
        System.err.print("Classifying Ontology with HermiT... ");
		hermitWithFactory.precomputeInferences(InferenceType.CLASS_HIERARCHY);
		
        //hermit.realise();
		fin = Calendar.getInstance().getTimeInMillis();
		System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	}

	
	
	public OWLOntology getBaseOntology(){
		return ontoBase;
	}
	
	
	
	public OWLOntologyManager getBaseOntologyManager(){
		return ontoManager;
	}

	
	
	public Set<OWLAxiom> getInferredAxioms(){
		
		return inferredAxioms;
		
	}

	public void reinitInferredAxioms(){
		inferredAxioms.clear();
	}

	
	public Reasoner getReasoner(){
    		return hermit;
    }
	
	
//	public void clearOntologies(){
//		hermit.clearOntologies();
//}
	
	
	
	public Set<OWLClass> getUnsatisfiableClasses() throws Exception{
		
		
		Set<OWLClass> set;
	
		//Now the reasoner return a node structure. A Node contains the
		//set of entities which are equivalent
		Node<OWLClass> node = hermit.getUnsatisfiableClasses();
	
		//set = node.getEntities();
		set = node.getEntitiesMinusBottom();
		
		//set.remove(datafactory.getOWLNothing());
		
		if (!set.isEmpty()) {
			System.out.println("There are '" + set.size() + "' UNSATisfiable classes.");
			//System.out.println("The following classes are unsatisfiable: ");
			//for(OWLClass cls : set) {
			//	System.out.println(" " + cls);
			//}
		}
		else{
			System.out.println("There are '0' unsatisfiable classes.");
		}
		
		return set;
		
	}
	
	
	
	public boolean isSatisfiable(OWLClass cls){
		return hermit.isSatisfiable(cls);
	}
	
	
	public boolean isEntailed(OWLAxiom ax){
		return hermit.isEntailed(ax);
	}
	
	
	
	public boolean isSubClassOf(OWLClass cls1, OWLClass cls2) {//throws Exception{
		//return hermit.isSubClassOf(cls1, cls2);
		return hermit.isEntailed(datafactory.getOWLSubClassOfAxiom(cls1, cls2));
	}
	
	
	/**
	 * @deprecated
	 * 
	 */
	public boolean isSubClassOf3(OWLClass cls1, OWLClass cls2) {//throws Exception{
		//System.err.println(hermit.getAncestorClasses(cls1).size());
		//System.err.println(hermit.getAncestorClasses(cls1));
		//return hermit.getAncestorClasses(cls1).containsEntity(cls2);
		return false;
	}
	
	/**
	 * @deprecated
	 * 
	 */	
	public boolean areDisjointClasses3(OWLClass cls1, OWLClass cls2) { // throws Exception{
		
		
		return hermit.isEntailed(datafactory.getOWLSubClassOfAxiom(
						cls1,
						datafactory.getOWLObjectComplementOf(cls2)
						));
	}
	
	
	public boolean areDisjointClasses(OWLClass cls1, OWLClass cls2) { // throws Exception{
		
		
		return !hermit.isSatisfiable(
				datafactory.getOWLObjectIntersectionOf(cls1, cls2));
		
	}
	
	
	public NodeSet<OWLClass> getIntesectionOfClasses(OWLClass cls1, OWLClass cls2) { // throws Exception{
		
		return hermit.getSuperClasses(datafactory.getOWLObjectIntersectionOf(cls1, cls2), true);
		
	}
	
	

	public boolean areEquivalentClasses(OWLClass cls1, OWLClass cls2) {//throws Exception{
		//return hermit.isEquivalentClass(cls1, cls2);
		return hermit.isEntailed(datafactory.getOWLEquivalentClassesAxiom(cls1, cls2));
	}

	
	
	
	public void removeExplicitDisjointness() {
		
		List<OWLOntologyChange> lisdisjointaxioms = new ArrayList<OWLOntologyChange>();
		
        for (OWLClass cls : ontoBase.getClassesInSignature()) {
        	for (OWLDisjointClassesAxiom ax : ontoBase.getDisjointClassesAxioms(cls.asOWLClass())) {
        		lisdisjointaxioms.add(new RemoveAxiom(ontoBase,	ax));
        	}
        }
        
        ontoManager.applyChanges(lisdisjointaxioms);
        
    }
	
	
	
	/**
	 * This method populates the structure of inferred entailments L0
	 *
	 */
	public void craeateInferredDisjointAxioms(){
		
		System.out.println("IRI: "+ iri);
		
		try {    	   
			
			axiomsInferredOnt = new TreeSet<OWLAxiom>();
	   		
			   
	           OWLOntologyManager classifiedOntoMan = OWLManager.createOWLOntologyManager();
	           OWLOntology inferredOnt = classifiedOntoMan.createOntology(iri);
	           InferredOntologyGenerator ontGen = new InferredOntologyGenerator(
	        		   hermit, new ArrayList<InferredAxiomGenerator<? extends OWLAxiom>>());
	           //InferredOntologyGenerator ontGen = new InferredOntologyGenerator(reasoner);
	           
	           
	           init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Extracting class inferences... ");
	           
	           
	           ontGen.addGenerator(new InferredDisjointClassesAxiomGenerator());
	           
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	           

	   		   init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Filling ontology... ");
	           ontGen.fillOntology(classifiedOntoMan, inferredOnt);	           
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	           
	           init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Building Treeset... ");
	           axiomsInferredOnt = new TreeSet<OWLAxiom>(inferredOnt.getAxioms());	    
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	   		   
	           
	           System.out.println("Disjoint axioms: " + axiomsInferredOnt.size());
	           
	           
	       }
	       catch (Exception e) {
	           e.printStackTrace();
	           //return new ArrayList<OWLAxiom>();
	       }		
		   
	}
	
	
	
	
	
	
	
	/**
	 * This method populates the structure of inferred entailments L0
	 *
	 */
	public void craeateInferredEntailmentsLanguage0(){
		
		System.out.println("IRI: "+ iri);
		
		try {    	   
			
			axiomsInferredOnt = new TreeSet<OWLAxiom>();
	   		
			for (OWLClass cls : hermit.getUnsatisfiableClasses()) {
	        
				if (!cls.isOWLNothing()) {
	                   OWLAxiom unsatAx = ontoManager.getOWLDataFactory().getOWLSubClassOfAxiom(cls,
	                		   ontoManager.getOWLDataFactory().getOWLNothing());
	                   
	                   //Consider unsat axiom!
	                   inferredAxioms.add(unsatAx);
	                   
	               }
	           }
	           
	           //axiomsInferredOnt.addAll(inferredAxioms);
	           
	           OWLOntologyManager classifiedOntoMan = OWLManager.createOWLOntologyManager();
	           OWLOntology inferredOnt = classifiedOntoMan.createOntology(iri);
	           InferredOntologyGenerator ontGen = new InferredOntologyGenerator(
	        		   hermit, new ArrayList<InferredAxiomGenerator<? extends OWLAxiom>>());
	           //InferredOntologyGenerator ontGen = new InferredOntologyGenerator(reasoner);
	           
	           
	           init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Extracting class inferences... ");
	           
	           ontGen.addGenerator(new InferredEquivalentClassAxiomGenerator());
	           ontGen.addGenerator(new InferredSubClassAxiomGenerator());
	           //ontGen.addGenerator(new InferredDisjointClassesAxiomGenerator());
	           
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	           
	           
	   		  /* init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Extracting assertion inferences... ");
	   		   
	           ontGen.addGenerator(new InferredClassAssertionAxiomGenerator());
	   		   ontGen.addGenerator(new InferredPropertyAssertionGenerator());
	   		   
	   		   fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	           
	           //The computacional cost is really high!
	           ontGen.addGenerator(new InferredDisjointClassesAxiomGenerator());
	           
	   		   init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Extracting dProp inferences... ");
	           
	           ontGen.addGenerator(new InferredDataPropertyCharacteristicAxiomGenerator());	           
	           ontGen.addGenerator(new InferredEquivalentDataPropertiesAxiomGenerator());
	           ontGen.addGenerator(new InferredSubDataPropertyAxiomGenerator());
	           
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	   		   
	   		   
	           init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Extracting oProp inferences... ");
	           
	           ontGen.addGenerator(new InferredEquivalentObjectPropertyAxiomGenerator());
	           ontGen.addGenerator(new InferredInverseObjectPropertiesAxiomGenerator());
	           ontGen.addGenerator(new InferredObjectPropertyCharacteristicAxiomGenerator());
	           ontGen.addGenerator(new InferredSubObjectPropertyAxiomGenerator());
	           
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	   		   
	           //System.out.println(classifiedOntoMan.toString());
	           //System.out.println(inferredOnt);
	           //System.out.println(ontGen);
	           */
	   		   init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Filling ontology... ");
	           ontGen.fillOntology(classifiedOntoMan, inferredOnt);	           
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	           
	           init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Building Treeset... ");
	           axiomsInferredOnt = new TreeSet<OWLAxiom>(inferredOnt.getAxioms());	    
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	   		   
	           
	           init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Spliting equivalences... ");
	           //splitMultipleEquivalenceAxioms();
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	           
	           init=Calendar.getInstance().getTimeInMillis();
	           System.err.print("Filtering axioms... ");
	           extractFilteredInferredAxioms();
	           fin = Calendar.getInstance().getTimeInMillis();
	   		   System.err.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	           
	           
	           
	       }
	       catch (Exception e) {
	           e.printStackTrace();
	           //return new ArrayList<OWLAxiom>();
	       }		
		   
	}
	
	
	
	/**
	 * Split equivalence axioms
	 */
	private void splitMultipleEquivalenceAxioms(){
		
		Set<OWLAxiom> multipleEqAxioms=new HashSet<OWLAxiom>();
		Set<OWLAxiom> splitAxioms=new HashSet<OWLAxiom>();
        
		OWLEquivalentClassesAxiom eqAxiom;
		List<OWLClassExpression> descs;
		
        for (OWLAxiom ax : axiomsInferredOnt) {
     	   
     	   if (ax instanceof OWLEquivalentClassesAxiom){
     		   eqAxiom = (OWLEquivalentClassesAxiom)ax;
     		   
     		   if (eqAxiom.getClassExpressions().size()>2){
     			   
     			   multipleEqAxioms.add(eqAxiom);     			  
     			   
     			   descs = new ArrayList<OWLClassExpression>(eqAxiom.getClassExpressions());
     			   
     			   if (ax.getSignature().contains(datafactory.getOWLNothing())){
     				  for (int i=0; i<descs.size(); i++){     					  
     					  if (!descs.get(i).equals(datafactory.getOWLNothing())){
     						  //We add subclass of axiom (that way we avoid duplicates (subclass and equivalent))
     						  OWLAxiom axSubNothing = datafactory.getOWLSubClassOfAxiom(descs.get(i), datafactory.getOWLNothing());
     						  splitAxioms.add(axSubNothing);
     						 //System.out.println(axiomsInferredOnt.size());
     						 //System.out.println(axiomsInferredOnt);
     						
     						  //if (!axiomsInferredOnt.contains(axSubNothing)){
     							  
     							  //splitAxioms.add(datafactory.getOWLEquivalentClassesAxiom(descs.get(i), Utilities.getOWLNothing()));
     							  //System.out.println("Adding Equivalenece to nothing.");
     						  //}
     					  }
     				  }
     			   }
     			   else {     			   
	     			   for (int i=0; i<descs.size()-1; i++){
	     				  for (int j=i+1; j<descs.size(); j++){     					  
	     					splitAxioms.add(datafactory.getOWLEquivalentClassesAxiom(descs.get(i), descs.get(j)));
	     					
	     				  }
	     				   
	     			   }
     			   }
     			   
     		   }//end if >2 descs
     		   
     	   }//end if equiv axioms
     	   
        }//end axioms
        
        
        //System.out.println(multipleEqAxioms.size() + "  " + splitAxioms.size());
        axiomsInferredOnt.removeAll(multipleEqAxioms);
        axiomsInferredOnt.addAll(splitAxioms);
        
        
	}
	
	


	/**
	 * We want to have only new axioms.
	 */
	private void extractFilteredInferredAxioms(){
		
		OWLSubClassOfAxiom subClsAx;

        //System.out.println("InferredAxioms: " + axiomsInferredOnt.size());
        
		
		for (OWLAxiom ax : axiomsInferredOnt) {
			//System.out.println(ax);
            boolean add = true;
            //for (OWLOntology actOnt : ontoManager.getOntologies()) {
            
            
            if (ontoBase.containsAxiom(ax)) {
             	   add=false;
            
            }
            //}
                
            ///We want new axioms and also the ones A subClass B, being A and B atomics (also prop classifications)
            //They are also classification axioms
            if (add) {                
            	//inferredAxioms.add(ax);
            	
            	//Inferences with a class as a subclassof THING are not interesting 
            	//and also they put noise within the results
            		if (ax instanceof OWLSubClassOfAxiom) {
            			
            			subClsAx = (OWLSubClassOfAxiom) ax;
                                
            			if (!subClsAx.getSuperClass().isOWLThing()) {
                        //ConsiderAxiom
                 	   inferredAxioms.add(subClsAx);
                    
            			}
                
            		}

            	else {
                   //Consider Axiom
             	   inferredAxioms.add(ax);
                }
            }
        }

		//System.out.println("InferredAxiomsFiltered: " + inferredAxioms.size());
		
		//return inferredAxioms;
	}


	
	public static void main(String[] args){
		
		try {
			IRI iriOnto= IRI.create("file:/C://xampp/htdocs/FRI_MAG_RDFizing/sakila_paris2.rdf");
			
			System.err.print("Loading ontology... ");
			OWLOntologyManager ontoMan = OWLManager.createOWLOntologyManager();
			OWLOntology onto = ontoMan.loadOntology(iriOnto);
			
			new HermiTReasonerAccess(ontoMan, onto);
			
			
			
			
			
			
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	
    /**
     * This method intends to enrich the reasoner output (only subsumptions) with
     * existencial and universal restriction (one level) and disjointness <br>
     * Uses given concepts to extract modules
     * 
     * @param conceptsMapps
     * @throws Exception
     */
    public void craeateInferredEntailmentsLanguage1(Set<OWLClass> conceptsMapps) throws Exception{
    	
    	//Set Up Module Extractor
    	//moduleExtractor = new ModuleExtractor(ontoBase, false, false, false, false);
    	
    	moduleExtractorManager = new ModuleExtractorManager(ontoBase, "LUM", false, false, false);
    	
    	//System.out.println("Number of inferred axioms (L0): " + inferredAxioms.size());
    	extractL1Inferences(conceptsMapps);
    	
    	 //
    	
    }
    
    
    private boolean getConsiderSomeValuesAxioms(){
    	return L1some;
    }
    
    private boolean getConsiderAllValuesAxioms(){
    	return L1all;
    }

	private boolean getConsiderDisjointnessAxioms(){
		return L1disj;
	}
    
	
	public boolean considerSomeValuesAxioms(boolean consider){
    	return L1some;
    }
    
    public boolean considerAllValuesAxioms(boolean consider){
    	return L1all;
    }

	public boolean considerDisjointnessAxioms(boolean consider){
		return L1disj;
	}
	
	
	private void extractL1Inferences(Set<OWLClass> conceptsMapps) throws Exception{
	
	
		HashSet<OWLEntity> setClass = new HashSet<OWLEntity>();
		
		Set<OWLClass> moduleClasses;
		Set<OWLObjectProperty> moduleObjectProperties;
		
		//Set<OWLClass> conceptsMapps = KnowledgeManager.getMappingConcepts();
		
		Set<OWLOntology> setOntos=new HashSet<OWLOntology>();
		
		long init, fin;
		
		//For entity in local difference
		for (OWLClass ent : conceptsMapps){
			
			setOntos.clear();
			
			init=Calendar.getInstance().getTimeInMillis();			
			
			setClass.add(ent);			
			//currentModule = moduleExtractor.getLocalityModuleForSignatureGroup(setClass);
			currentModule = moduleExtractorManager.extractModule(setClass);
			setOntos.add(currentModule);
			OWLOntologyManager moduleManager = OWLManager.createOWLOntologyManager();
			moduleManager.createOntology(currentModule.getOntologyID().getOntologyIRI(), setOntos);
			
			fin = Calendar.getInstance().getTimeInMillis();
			//System.out.println("\tModule extraction for " + ent.toString() + ": " + (double)((double)fin-(double)init)/1000.0);
									
			
			init=Calendar.getInstance().getTimeInMillis();
			//createModuleReasoner(moduleExtractor.getModuleOntologyManager(), currentModule);
			createModuleReasoner(moduleManager, currentModule);
			fin = Calendar.getInstance().getTimeInMillis();
			//System.out.println("\tModule Reasoning " + ent.toString() + ": " + (double)((double)fin-(double)init)/1000.0);
			
			
			moduleClasses = new HashSet<OWLClass>(currentModule.getClassesInSignature());			
			moduleObjectProperties = new HashSet<OWLObjectProperty>(currentModule.getObjectPropertiesInSignature());
			
			//System.out.println("\tModule size for: " + 
			//		intersectionWithDiffCls.size() + "(" +currentModule.getReferencedClasses().size() + ") - " 
			//		+ intersectionWithDiffProp.size()+ "(" + currentModule.getReferencedObjectProperties().size()+")");
			
			
			init=Calendar.getInstance().getTimeInMillis();
			
			
			if (getConsiderDisjointnessAxioms()){
				
				if (getConsiderAllValuesAxioms()){
					
					if (getConsiderSomeValuesAxioms()){
						
						//CASE I: ALL (todos)
						for (OWLClass cls : moduleClasses){
							if (cls.equals(ent))
								continue;
							
							for (OWLObjectProperty oProp : moduleObjectProperties){								
								AddExistencialRestrictionAxiomIfSatisfied(ent, oProp, cls);
								AddUniversalRestrictionAxiomIfSatisfied(ent, oProp, cls);
							}//En for properties module
									
							AddComplementAxiomIfSatisfied(ent, cls);							
						}//End for classes modules
					}
					else{ 
						
						//CASE II: No Existencial
						for (OWLClass cls : moduleClasses){
							if (cls.equals(ent))
								continue;
							
							for (OWLObjectProperty oProp : moduleObjectProperties){								
								AddUniversalRestrictionAxiomIfSatisfied(ent, oProp, cls);
							}//En for properties module
									
							AddComplementAxiomIfSatisfied(ent, cls);							
						}//End for classes modules
						
					}
				}
				else { 
					//CASE III: No All Values
					if (getConsiderSomeValuesAxioms()){
						
						for (OWLClass cls : moduleClasses){
							if (cls.equals(ent))
								continue;
							
							for (OWLObjectProperty oProp : moduleObjectProperties){								
								AddExistencialRestrictionAxiomIfSatisfied(ent, oProp, cls);
								
							}//En for properties module
									
							AddComplementAxiomIfSatisfied(ent, cls);							
						}//End for classes modules
						
					}
					else{ 
						//CASE IV: Neither All nor some					
						for (OWLClass cls : moduleClasses){
							if (cls.equals(ent))
								continue;
							
							AddComplementAxiomIfSatisfied(ent, cls);						
						}
					}
				}
			}//En Disjoint
			
			else{
				
				if (getConsiderAllValuesAxioms()){
					
					if (getConsiderSomeValuesAxioms()){
						
						//CASE V: NO DISJOINT
						for (OWLClass cls : moduleClasses){
							if (cls.equals(ent))
								continue;
							
							for (OWLObjectProperty oProp : moduleObjectProperties){								
								AddExistencialRestrictionAxiomIfSatisfied(ent, oProp, cls);
								AddUniversalRestrictionAxiomIfSatisfied(ent, oProp, cls);
							}//En for properties module									
														
						}//End for classes modules
					}
					else{ 
						
						//CASE VI: No Existencial nor Disjoint
						for (OWLClass cls : moduleClasses){
							if (cls.equals(ent))
								continue;
							
							for (OWLObjectProperty oProp : moduleObjectProperties){								
								AddUniversalRestrictionAxiomIfSatisfied(ent, oProp, cls);
							}//En for properties module
														
						}//End for classes modules
						
					}
				}
				else { 
					//CASE VII: No All Values nor Disjoint
					if (getConsiderSomeValuesAxioms()){
						
						for (OWLClass cls : moduleClasses){
							if (cls.equals(ent))
								continue;
							
							for (OWLObjectProperty oProp : moduleObjectProperties){								
								AddExistencialRestrictionAxiomIfSatisfied(ent, oProp, cls);
								
							}//En for properties module
																					
						}//End for classes modules
						
					}
					//else{//CASE VIII: Nothing
				}
			}//En Disjoint
				
						
			
			fin = Calendar.getInstance().getTimeInMillis();
			System.out.println("\tTime inferences L1 for " + ent.toString() + ": " + (double)((double)fin-(double)init)/1000.0);
			
			setClass.clear();
			
					
			
		}//End entities mappings
		
		
		
		//Check redundance
		//
		
		
		//System.out.println("Number of Total inferred axioms (L0+L1): " + inferredAxioms.size());
		
		
	}

    
    
    
    
	private void createModuleReasoner(OWLOntologyManager ontomanager, OWLOntology onto) throws Exception{
			currentModuleReasoner = new HermiTReasonerAccess(ontomanager, onto);
	}
	
	
	
	/**
	 * 
	 * @param Structure Structe where infernces should be added
	 * @param reasoner Reasoner to use	
	 * @param ent
	 * @param oProp
	 * @param rangeCls
	 * @throws Exception
	 */	 
	private void AddExistencialRestrictionAxiomIfSatisfied(  
				OWLEntity ent, OWLObjectProperty oProp, OWLClass rangeCls) throws Exception{
	
		OWLClassExpression desc1 = ent.asOWLClass();
		OWLClassExpression desc2 = datafactory.getOWLObjectSomeValuesFrom(oProp, rangeCls);
		
		OWLAxiom ax = datafactory.getOWLSubClassOfAxiom(desc1,desc2);
		
		if (!ontoBase.getAxioms().contains(ax) && !isEntailmentRedundant_Exists(ent, oProp, rangeCls)) { 		
			if (currentModuleReasoner.isEntailed(ax)){			
				inferredAxioms.add(ax);
				//inferredAxioms_L1_Some.add(ax);
			}
		}
		
	}
	
	
	/**
	 * 
	 * @param Structure Structe where infernces should be added
	 * @param reasoner Reasoner to use
	 * @param ent
	 * @param oProp
	 * @param rangeCls
	 * @throws Exception
	 */
	private void AddUniversalRestrictionAxiomIfSatisfied(  
				OWLEntity ent, OWLObjectProperty oProp, OWLClass rangeCls) throws Exception{
	
		OWLClassExpression desc1 = ent.asOWLClass();
		OWLClassExpression desc2 = datafactory.getOWLObjectAllValuesFrom(oProp, rangeCls);
		
		OWLAxiom ax = datafactory.getOWLSubClassOfAxiom(desc1,desc2);
		
		if (!ontoBase.getAxioms().contains(ax) && !isEntailmentRedundant_Only(ent, oProp, rangeCls)) { 
			
			if (currentModuleReasoner.isEntailed(ax)){			
				inferredAxioms.add(ax);
				//inferredAxioms_L1_All.add(ax);
			}
		}
	}
	
	
	/**
	 * 
	 * @param Structure Structe where infernces should be added
	 * @param reasoner Reasoner to use
	 * @param ent
	 * @param cls
	 * @throws Exception
	 */
	private void AddComplementAxiomIfSatisfied( 
				OWLEntity ent, OWLClass cls) throws Exception{
	
		OWLClassExpression desc = datafactory.getOWLObjectComplementOf(cls);
		
		OWLAxiom ax = datafactory.getOWLSubClassOfAxiom(ent.asOWLClass(), desc);
		
		if (!ontoBase.getAxioms().contains(ax) && !isEntailmentRedundant_Neg(ent, cls)) {		
			if (currentModuleReasoner.isEntailed(ax)){			
				inferredAxioms.add(ax);
				//inferredAxioms_L1_Neg.add(ax);
			}
		}
	}
    
	
	
	
	
	
	
	
	
	
	/**
	 * If B sub C is inferred and A sub r only B is explicit then A sub r only C could be considered redundant 
	 * @param ent
	 * @param oProp
	 * @param rangeCls
	 * @return
	 */
	public boolean isEntailmentRedundant_Only(OWLEntity ent, OWLObjectProperty oProp, OWLClass rangeCls){
		
		//if (true)
		//	return false;
		
		for (OWLAxiom inf : inferredAxioms){
			
			if (inf instanceof OWLSubClassOfAxiom){
				
				if (((OWLSubClassOfAxiom)inf).getSuperClass().equals(rangeCls)){
										
					OWLClassExpression desc1 = ent.asOWLClass();
					OWLClassExpression desc2 = datafactory.getOWLObjectAllValuesFrom(oProp, ((OWLSubClassOfAxiom)inf).getSubClass());
					
					OWLAxiom ax = datafactory.getOWLSubClassOfAxiom(desc1,desc2);
					
					if (ontoBase.containsAxiom(ax) || inferredAxioms.contains(ax)){
						return true;
					}					
				}
			}
		}	
		
		
		for (OWLAxiom axiom : ontoBase.getAxioms()){
			
			if (axiom instanceof OWLSubClassOfAxiom){
				
				if (((OWLSubClassOfAxiom)axiom).getSuperClass().equals(rangeCls)){
										
					OWLClassExpression desc1 = ent.asOWLClass();
					OWLClassExpression desc2 = datafactory.getOWLObjectAllValuesFrom(oProp, ((OWLSubClassOfAxiom)axiom).getSubClass());
					
					OWLAxiom ax = datafactory.getOWLSubClassOfAxiom(desc1,desc2);
					
					if (inferredAxioms.contains(ax)){
						return true;
					}					
				}
			}
		}		
		
		
		
		
				
		return false;
		
	}
	
	
	/**
	 * If B sub C is inferred and A sub r some B is explicit then A sub r some C could be considered redundant 
	 * @param ent
	 * @param oProp
	 * @param rangeCls
	 * @return
	 */
	public boolean isEntailmentRedundant_Exists(OWLEntity ent, OWLObjectProperty oProp, OWLClass rangeCls){
		
	//	if (true)
		//	return false;
		
		for (OWLAxiom inf : inferredAxioms){
			
			if (inf instanceof OWLSubClassOfAxiom){
				
				//if (((OWLSubClassAxiom)inf).getSuperClass().equals(Utilities.getOWLNothing()))
				//		return true;
						
				
				//
				if (((OWLSubClassOfAxiom)inf).getSuperClass().equals(rangeCls)){
										
					OWLClassExpression desc1 = ent.asOWLClass();
					OWLClassExpression desc2 = datafactory.getOWLObjectSomeValuesFrom(oProp, ((OWLSubClassOfAxiom)inf).getSubClass());
					
					OWLAxiom ax = datafactory.getOWLSubClassOfAxiom(desc1,desc2);
					
					if (ontoBase.containsAxiom(ax) || inferredAxioms.contains(ax)){
						return true;
					}					
				}
			}
		}		
		
		
		
		for (OWLAxiom axiom : ontoBase.getAxioms()){
			
			if (axiom instanceof OWLSubClassOfAxiom){
				
				if (((OWLSubClassOfAxiom)axiom).getSuperClass().equals(rangeCls)){
										
					OWLClassExpression desc1 = ent.asOWLClass();
					OWLClassExpression desc2 = datafactory.getOWLObjectSomeValuesFrom(oProp, ((OWLSubClassOfAxiom)axiom).getSubClass());
					
					OWLAxiom ax = datafactory.getOWLSubClassOfAxiom(desc1,desc2);
					
					if (inferredAxioms.contains(ax)){
						return true;
					}					
				}
			}
		}		
		
		
		
				
		return false;
		
	}
    
	
	
	
	/**
	 * If B sub C is inferred and A sub not B is explicit then A not C could be considered redundant 
	 * @param ent
	 * @param oProp
	 * @param rangeCls
	 * @return
	 */
	public boolean isEntailmentRedundant_Neg(OWLEntity ent, OWLClass cls){
		
	//	if (true)
	//		return false;
					
		for (OWLAxiom inf : inferredAxioms){
			
			if (inf instanceof OWLSubClassOfAxiom){
				
				if (((OWLSubClassOfAxiom)inf).getSuperClass().equals(cls)){
										
					
					OWLClassExpression desc1 = ent.asOWLClass();
					OWLClassExpression desc2 = datafactory.getOWLObjectComplementOf(cls);
					
					OWLAxiom ax_neg = datafactory.getOWLSubClassOfAxiom(desc1,desc2);
					OWLAxiom ax_disj = datafactory.getOWLDisjointClassesAxiom(desc1, desc2);
					
					if (ontoBase.containsAxiom(ax_neg) || inferredAxioms.contains(ax_neg) || 
							ontoBase.containsAxiom(ax_disj) || inferredAxioms.contains(ax_disj)){
						return true;
					}					
				}
			}
		}
		
		
		for (OWLAxiom axiom : ontoBase.getAxioms()){
			
			if (axiom instanceof OWLSubClassOfAxiom){
				
				if (((OWLSubClassOfAxiom)axiom).getSuperClass().equals(cls)){
										
					
					OWLClassExpression desc1 = ent.asOWLClass();
					OWLClassExpression desc2 = datafactory.getOWLObjectComplementOf(((OWLSubClassOfAxiom)axiom).getSubClass());
					
					OWLAxiom ax_neg = datafactory.getOWLSubClassOfAxiom(desc1,desc2);
					OWLAxiom ax_disj = datafactory.getOWLDisjointClassesAxiom(desc1, desc2);
					
					if (inferredAxioms.contains(ax_neg) || 
							ontoBase.containsAxiom(ax_disj) || inferredAxioms.contains(ax_disj)){
						return true;
					}					
				}
			}
		}		
		
				
		return false;
		
	}

  

	
	

}
