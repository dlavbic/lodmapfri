/*******************************************************************************
 * Copyright 2012 by the Department of Computer Science (University of Oxford)
 * 
 *    This file is part of LogMap.
 * 
 *    LogMap is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 * 
 *    LogMap is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 * 
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with LogMap.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package uk.ac.ox.krr.logmap2.reasoning.deprecated;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import uk.ac.ox.krr.logmap2.utilities.Utilities;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

public class SatisfiabilityIntegration {

	private String rootPath = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/LogMap2_Mappings/";
	private String sufix = "_logmap2_Output/";
	private String module1str = "module1.owl";
	private String module2str = "module2.owl";
	private String mappingstr = "mappings.owl";
	
	private OWLDataFactory factory;
	private OWLOntologyManager managerOnto1;
	private OWLOntologyManager managerOnto2;
	private OWLOntologyManager managerMappings;
	private OWLOntologyManager managerMerged;

	private OWLOntology moduleonto1;
	private OWLOntology moduleonto2;
	private OWLOntology mergedOntology;
	private OWLOntology mappingsOntology;
	
	private String mappingsOntologyFile;
	private String moduleonto1file;
	private String moduleonto2file;

	private String pair_str;
	
	long init, fin;

	HermiTReasonerAccess reasonerMergedHermit;
	PelletAccess reasonerMergedPellet;
	
	int resultClassEval;
	
	final int SAT = 0;
	final int UNSAT = 1;
	final int UNKNOWN = 2;
	final int KNOWN = 3;
	
	OWLClass current_cls;
	
	
	private final int NOREASONER = -1;
	private final int HERMIT = 0;
	private final int PELLET = 1;
	private final int PELLET_HERMIT = 2;
	private int reasoner = HERMIT;
	//private int reasonerType = HERMIT;
	
	private int unsat = 0;
	
	
	public SatisfiabilityIntegration(OWLOntology O1, OWLOntology O2, String uri_M) throws Exception{
				
		managerMappings = OWLManager.createOWLOntologyManager();
		OWLOntology M = managerMappings.loadOntology(IRI.create(uri_M));
			
		System.out.println("Mappings?? : " + M.getLogicalAxiomCount());
		
		
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		axioms.addAll(O1.getAxioms());
		axioms.addAll(O2.getAxioms());
		axioms.addAll(M.getAxioms());
				
		managerMerged = OWLManager.createOWLOntologyManager();
		mergedOntology = managerMerged.createOntology(axioms, IRI.create("http://krono.act.uji.es/mappings/Integration.owl"));
		
		//Classify Ontology and getting errors
		reasonerMergedHermit = new HermiTReasonerAccess(managerMerged, mergedOntology, true);		
		Set<OWLClass> unsatisfiableClasses = reasonerMergedHermit.getUnsatisfiableClasses();
		System.out.println("There are '" + unsatisfiableClasses.size() + "' unsatisfiable classes.");

	}
	
	
	
	
	public SatisfiabilityIntegration(OWLOntology O1, OWLOntology O2, OWLOntology M) throws Exception{
		
		try{
			
			unsat = 0;
			
			init=Calendar.getInstance().getTimeInMillis();
			
			//Create merged onto
			createMergedOntology(O1, O2, M);
			
			//Reasoners to use
			reasoner = PELLET_HERMIT;
			
			//Sat
			evaluateSatisfiability();
			
			
			fin=Calendar.getInstance().getTimeInMillis();
			System.out.println("Time extracting unsat classes (s): " + (float)((double)fin-(double)init)/1000.0);
		}
		catch (Exception e){
			System.err.println("An error ocurred when reasoning with the intergated ontology. So satisfiability coul not be checked.");
		}
		
		
	}
	
	
	public boolean hasUnsatClasses(){
		return (unsat>0);
	}
	
	public int getNumUnsatClasses(){
		return unsat;
	}
	
	
	private void createMergedOntology(OWLOntology O1, OWLOntology O2, OWLOntology M) throws Exception{
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		axioms.addAll(O1.getAxioms());
		axioms.addAll(O2.getAxioms());
		axioms.addAll(M.getAxioms());
				
		managerMerged = OWLManager.createOWLOntologyManager();
		mergedOntology = managerMerged.createOntology(axioms, IRI.create("http://krono.act.uji.es/mappings/Integration.owl"));
		
		//System.out.println("Storing merged ontology: ");
		//managerMerged.saveOntology(mergedOntology, new RDFXMLOntologyFormat(), IRI.create("file:/usr/local/data/ConfOntosOAEI/cmt_cocus.owl")); //RDFXMLOntologyFormat
		
		
		
		System.out.println("Number of classes integration: " + mergedOntology.getClassesInSignature().size());
	}
	
	
	
	private void evaluateSatisfiability() throws Exception{
		
		//Classify Ontology and getting errors	
		try{
			reasonerMergedHermit = new HermiTReasonerAccess(managerMerged, mergedOntology, false);
		}
		catch (Exception e){
			reasoner=PELLET; //only pellet
		}
		
		
		try{
			reasonerMergedPellet = new PelletAccess(managerMerged, mergedOntology, false);
		}
		catch (Exception e){
			if (reasoner==PELLET){
				reasoner=NOREASONER;
			}
			else{
				reasoner=HERMIT;//only hermit then
			}
		}
		
		if (reasoner==NOREASONER){
			throw new Exception();//System.err.println("An error ocurred when reasoning with the intergated ontology. So satisfiability coul not be checked.");
			//return;
		}
		
				
		if (reasoner == PELLET_HERMIT){
				
			reasoner=HERMIT;
			ClassOntology_withTimeout(100);
			
			//System.out.println(resultClassEval);
			
			if (resultClassEval==UNKNOWN){ //Then check with Hermit
			
				//System.out.println("Class hermit: " + resultClassEval);
				
				reasoner=PELLET;
				ClassOntology_withTimeout(100);//seconds
			}
		}
		else {
			ClassOntology_withTimeout(100);
		}
		
		
				
		
		if (resultClassEval == UNKNOWN){  //We check class per class
			
			System.out.println("Getting unsatistiable classes...");
			unsat = 0;
			int sat = 0;
			int unknown = 0;
			int clsnum = 0;	
			String class_name;
			for (OWLClass cls : mergedOntology.getClassesInSignature()){
				
				current_cls = cls;
				class_name = Utilities.getEntityLabelFromURI(cls.getIRI().toString());
				
				if (reasoner == PELLET_HERMIT){
					reasoner=PELLET;
					checkSATCls_withTimeout(1);
					if (resultClassEval==UNKNOWN){ //Then check with Hermit
						reasoner = HERMIT;
						checkSATCls_withTimeout(2); //Give more time
					}
					reasoner = PELLET_HERMIT;
				}
				else {
					//Only one reasoner, either Hermit or Pellet
					checkSATCls_withTimeout(1);
				}
				
				
				
				if (resultClassEval==UNSAT){
					unsat++;
					//System.err.println("Class '" + class_name + "' is UNSAT");
				}
				else if (resultClassEval==SAT){
					sat++;
					//System.out.println("Class '" + class_name + "' is SAT");
				}
				else{
					unknown++;
					//System.err.println("No response for class '" + class_name + "'");
				}
				
				clsnum++;
				
				
			}
			System.out.println("There are '" + clsnum + "' classes.");
			System.out.println("\tThere are '" + unknown + "' UNKNOWN classes.");
			System.out.println("\tThere are '" + sat + "' SAT classes.");
			System.out.println("\tThere are '" + unsat + "' UNSAT classes.");
			
			
	        //Set<OWLClass> unsatisfiableClasses = reasonerMerged.getUnsatisfiableClasses();
	        //System.out.println("There are '" + unsatisfiableClasses.size() + "' unsatisfiable classes.");

		}
		
	}
	
	
	
	public SatisfiabilityIntegration(String onto_iri_str) throws Exception{
	
		System.out.println("Loading ontology...");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	
		OWLOntology onto = manager.loadOntology(IRI.create(onto_iri_str));
		
		
		//Classify Ontology and getting errors
		System.out.println("Getting unsatistiable classes...");
		HermiTReasonerAccess reasonerMerged = new HermiTReasonerAccess(manager, onto, false);
		//PelletAccess reasonerMerged = new PelletAccess(managerMerged, mergedOntology, false);
		
		unsat = 0;
		int clsnum = 0;
		int mappings = 0;
		for (OWLClass cls : reasonerMerged.getBaseOntology().getClassesInSignature()){
			
			if (reasonerMerged.getReasoner().getEquivalentClasses(cls).getEntitiesMinus(cls).size() > 1){
				mappings += reasonerMerged.getReasoner().getEquivalentClasses(cls).getEntitiesMinus(cls).size();
			}
			
			
			if (!reasonerMerged.isSatisfiable(cls)){
				unsat++;
				System.err.println("Class " + clsnum + " is UNSAT");
			}
			else{
				//System.out.println("Class " + clsnum + " is satisfiable");
			}
			
			clsnum++;
			
			
		}
		
		System.out.println("There are '" + unsat + "' unsatisfiable classes.");
		System.out.println("There are '" + clsnum + "' classes.");
		System.out.println("There are '" + mappings + "' mappings.");
		
		
		
	}
	
	
	public SatisfiabilityIntegration(String onto1, String onto2, String mappings) throws Exception{
		
		
		mappingsOntologyFile = mappings;
		moduleonto1file = onto1;
		moduleonto2file = onto2;
		
		init=Calendar.getInstance().getTimeInMillis();
		loadOntologies();
		fin=Calendar.getInstance().getTimeInMillis();
		System.out.println("Time loading ontologies (s): " + (float)((double)fin-(double)init)/1000.0);
		
		init=Calendar.getInstance().getTimeInMillis();
		createCurrentMergedOntologyAndGetUNSAT();
		fin=Calendar.getInstance().getTimeInMillis();
		System.out.println("Time extracting unsat classes (s): " + (float)((double)fin-(double)init)/1000.0);
		
		
		
	}
	
	
	//maintains a thread for executing the doWork method

    public void doWork_SatCls() {
        //perform some long running task here...
    	if (reasoner == HERMIT){    		
    		if (reasonerMergedHermit.isSatisfiable(current_cls))
    			resultClassEval=SAT;
    		else
    			resultClassEval=UNSAT;
    	}
    	else{
    		if (reasonerMergedPellet.isSatisfiable(current_cls))
    			resultClassEval=SAT;
    		else
    			resultClassEval=UNSAT;
    	}
    }
    

    public void checkSATCls_withTimeout(int timeoutSecs) {

    	ExecutorService executor = Executors.newFixedThreadPool(1);
    	
        //set the executor thread working
        final Future<?> future = executor.submit(new Runnable() {
            public void run() {
                try {
                	doWork_SatCls();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });

        //check the outcome of the executor thread and limit the time allowed for it to complete
        try {
            future.get(timeoutSecs, TimeUnit.SECONDS);
            future.cancel(true);
            executor.shutdown();
        }
        catch (TimeoutException e) {
        	
        	//ExecutionException: deliverer threw exception
            //TimeoutException: didn't complete within downloadTimeoutSecs
            //InterruptedException: the executor thread was interrupted

        	resultClassEval = UNKNOWN;
        	
            //interrupts the worker thread if necessary
        	future.cancel(true);
            executor.shutdown();

            
        
    	}
        catch (Exception e) {
        	e.printStackTrace();
    	
        }        
    }
    
    
    
    public void doWork_ClassOntology() throws Exception {
    	
    	unsat = 0;
    	
        //perform some long running task here...
    	if (reasoner == HERMIT){    		
    		//reasonerMergedHermit.classifyOntology();
    		reasonerMergedHermit.classifyOntologyNoProperties();
    		unsat = reasonerMergedHermit.getUnsatisfiableClasses().size();
    	}
    	else{
    		reasonerMergedPellet.classifyOntologyNoProperties();
    		unsat = reasonerMergedPellet.getUnsatisfiableClasses().size();
    	}
    	
    	resultClassEval = KNOWN;
    }
    

    public void ClassOntology_withTimeout(int timeoutSecs) {

    	ExecutorService executor = Executors.newFixedThreadPool(1);
    	
        //set the executor thread working
        final Future<?> future = executor.submit(new Runnable() {
            public void run() {
                try {
                	doWork_ClassOntology();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });

        //check the outcome of the executor thread and limit the time allowed for it to complete
        try {
            future.get(timeoutSecs, TimeUnit.SECONDS);
            future.cancel(true);
            executor.shutdown();
        }
        catch (TimeoutException e) {
        	
        	//ExecutionException: deliverer threw exception
            //TimeoutException: didn't complete within downloadTimeoutSecs
            //InterruptedException: the executor thread was interrupted

        	resultClassEval = UNKNOWN;
        	
        	if (reasoner == HERMIT){    		
            	reasonerMergedHermit.getReasoner().interrupt();
            }
            else{
            	reasonerMergedPellet.getReasoner().interrupt();
            }
        	
            //interrupts the worker thread if necessary         
            future.cancel(true);
            executor.shutdown();
            
            

            
        
    	}
        catch (Exception e) {
        	e.printStackTrace();
    	
        }        
    }

	
	
	
	
	public SatisfiabilityIntegration() throws Exception{
		
		
		//setOntologies();
		setNonFixedOntologies();
		
		init=Calendar.getInstance().getTimeInMillis();
		loadOntologies();
		fin=Calendar.getInstance().getTimeInMillis();
		System.out.println("Time loading ontologies (s): " + (float)((double)fin-(double)init)/1000.0);
		
		init=Calendar.getInstance().getTimeInMillis();
		createCurrentMergedOntologyAndGetUNSAT();
		fin=Calendar.getInstance().getTimeInMillis();
		System.out.println("Time extracting unsat classes (s): " + (float)((double)fin-(double)init)/1000.0);
		
		
		
	}
	
	private void setNonFixedOntologies(){
		
		//setFMA2NCI();
		setFMA2SNOMED();
		//setSNOMED2NCI();
		mappingsOntologyFile = rootPath + pair_str + sufix + mappingstr;
		
		moduleonto1file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/fma2nci_dataset/oaei2012_FMA_big_overlapping_nci.owl";
		moduleonto2file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/fma2nci_dataset/oaei2012_NCI_big_overlapping_fma.owl";
		
		moduleonto1file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/fma2snmd_dataset/oaei2012_FMA_big_overlapping_snomed.owl";
		moduleonto2file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/fma2snmd_dataset/oaei2012_SNOMED_big_overlapping_fma.owl";
		
		
		//mappingsOntologyFile = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/Aroma_results/Aroma_FMA2NCI_wholeOntologies.owl";
		//moduleonto1file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/fma2nci_dataset/oaei2012_FMA_whole_ontology.owl";
		//moduleonto2file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/fma2nci_dataset/oaei2012_NCI_whole_ontology.owl";
		//moduleonto1file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/snomed20090131_replab.owl";
		//moduleonto2file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/fma2nci_dataset/oaei2012_NCI_whole_ontology.owl";
		
		//moduleonto1file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/snmd2nci_dataset/oaei2012_SNOMED_small_overlapping_nci.owl";
		//moduleonto2file = "file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/OAEI_datasets/snmd2nci_dataset/oaei2012_NCI_small_overlapping_snomed.owl";
		
		
	}
	
	private void setOntologies(){
		setFMA2NCI();
		//setFMA2SNOMED();
		//setSNOMED2NCI();
		//setNCI2LUCADA();
		//setSNOMED2LUCADA();
		
		
		mappingsOntologyFile = rootPath + pair_str + sufix + mappingstr;
		moduleonto1file = rootPath + pair_str + sufix + module1str;
		moduleonto2file = rootPath + pair_str + sufix + module2str;
	}
	
	
	
	
	
	private void loadOntologies() throws Exception {		

		managerOnto1 = OWLManager.createOWLOntologyManager();
		managerOnto2 = OWLManager.createOWLOntologyManager();
		managerMappings = OWLManager.createOWLOntologyManager();
		
		factory = managerOnto1.getOWLDataFactory();
	
		moduleonto1 = managerOnto1.loadOntology(IRI.create(moduleonto1file));
		System.out.println("Getting unsatistiable classes...");
		moduleonto2 = managerOnto2.loadOntology(IRI.create(moduleonto2file));
		
		mappingsOntology = managerMappings.loadOntology(IRI.create(mappingsOntologyFile));
	 
	}
	
	
	
	/**
	 * Creates current merged ontology with current mappings 
	 * in order to analyze the impact 
	 * @throws Exception
	 */
	private void createCurrentMergedOntologyAndGetUNSAT() throws Exception {
		
		
		//Create merged
		createMergedOntology(moduleonto1, moduleonto2, mappingsOntology);
		
		evaluateSatisfiability();
			
		
		
	}
	
	
	
	
	
	
	
	
	private void setFMA2NCI(){
		pair_str ="FMA2NCI";		
	}
		
	private void setFMA2SNOMED(){
		pair_str ="FMA2SNOMED";
	}
	
	private void setSNOMED2NCI(){
		pair_str ="SNOMED2NCI";
	}

	private void setNCI2LUCADA(){
		pair_str ="NCI2LUCADA";
	}

	private void setSNOMED2LUCADA(){
		pair_str ="SNOMED2LUCADA";
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try{
			
			if (true){	
				//new SatisfiabilityIntegration("file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/LogMap2_Mappings/SNOMED2NCI_logmap2_Output/integration.owl");
				new SatisfiabilityIntegration("file:/usr/local/data/DataUMLS/UMLS_Onto_Versions/LogMap2_Mappings/SNOMED2NCI_logmap2_Output/OWL2ELmodule_In_Monster_SNOMED_NCI.owl");				
			}
			else 
			if (args.length==3){
				new SatisfiabilityIntegration(args[0], args[1], args[2]);
			}
			else{
				new SatisfiabilityIntegration();
			}
		
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

}
