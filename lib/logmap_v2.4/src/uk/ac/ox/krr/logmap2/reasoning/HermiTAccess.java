/**
 * *****************************************************************************
 * Copyright 2012 by the Department of Computer Science (University of Oxford)
 *
 * This file is part of LogMap.
 *
 * LogMap is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LogMap is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LogMap. If not, see <http://www.gnu.org/licenses/>.
 * ****************************************************************************
 */
package uk.ac.ox.krr.logmap2.reasoning;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.datatypes.UnsupportedDatatypeException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;

/**
 * @author Ernesto Jimenez Ruiz Nov 8, 2011
 *
 */
public class HermiTAccess extends ReasonerAccessImpl {

    public HermiTAccess(OWLOntologyManager ontoManager, OWLOntology onto, boolean useFactory) throws Exception {
        super(ontoManager, onto, useFactory);
    }

//	protected void setUpReasoner(boolean useFactory) throws Exception{
//			
//		
//		new SimpleConfiguration(); //BufferingMode.NON_BUFFERING
//		
//		if (useFactory){	 
//			reasonerFactory = new Reasoner.ReasonerFactory();
//			reasoner = reasonerFactory.createReasoner(ontoBase);
//		}
//		else{
//			//This may avoid the exception due to datatypes outside OWL 2 specification
//			Configuration conf = new Configuration();
//			conf.ignoreUnsupportedDatatypes=true;
//			reasoner=new HermiT_adapted(ontoBase, conf);
//		}
//		
//		reasonerName = "HermiT";//reasoner.getReasonerName();
//
//		
//	}
    protected void setUpReasoner(boolean useFactory) throws Exception {

        //, new SimpleConfiguration()); //BufferingMode.NON_BUFFERING
        if (useFactory) {
            reasonerFactory = new Reasoner.ReasonerFactory();
            reasoner = reasonerFactory.createReasoner(ontoBase);
        } else {
            try {
                reasoner = new uk.ac.ox.krr.logmap2.reasoning.deprecated.HermiT_adapted(ontoBase);
            } catch (UnsupportedDatatypeException e) {
                reasoner = new Reasoner.ReasonerFactory().createNonBufferingReasoner(ontoBase);
            }
        }

        reasonerName = "HermiT";//reasoner.getReasonerName();

    }

}
